<?php
class ControllerCatalogQuestion extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('catalog/question');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/question');
		
		$this->getList();
	} 

	public function insert() {
		$this->language->load('catalog/question');

		$this->document->setTitle($this->language->get('heading_title_question'));
		
		$this->load->model('catalog/question');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_question->addQuestion($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['filter_question_id'])) {
				$url .= '&filter_question_id=' . $this->request->get['filter_question_id'];
			}
			
			if (isset($this->request->get['filter_questioner'])) {
				$url .= '&filter_questioner=' . urlencode(html_entity_decode($this->request->get['filter_questioner'], ENT_QUOTES, 'UTF-8'));
			}
			
			if (isset($this->request->get['filter_question_category'])) {
				$url .= '&filter_question_category=' . $this->request->get['filter_question_category'];
			}
			
			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}
			
			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
						
			$this->redirect($this->url->link('catalog/question', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/question');

		$this->document->setTitle($this->language->get('heading_title_question'));
		
		$this->load->model('catalog/question');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_question->editQuestion($this->request->get['question_id'], $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['filter_question_id'])) {
				$url .= '&filter_question_id=' . $this->request->get['filter_question_id'];
			}
			
			if (isset($this->request->get['filter_questioner'])) {
				$url .= '&filter_questioner=' . urlencode(html_entity_decode($this->request->get['filter_questioner'], ENT_QUOTES, 'UTF-8'));
			}
			
			if (isset($this->request->get['filter_question_category'])) {
				$url .= '&filter_question_category=' . $this->request->get['filter_question_category'];
			}
			
			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}
			
			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
						
			$this->redirect($this->url->link('catalog/question', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() { 
		$this->language->load('catalog/question');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/question');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $question_id) {
				$this->model_catalog_question->deleteQuestion($question_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['filter_question_id'])) {
				$url .= '&filter_question_id=' . $this->request->get['filter_question_id'];
			}
			
			if (isset($this->request->get['filter_questioner'])) {
				$url .= '&filter_questioner=' . urlencode(html_entity_decode($this->request->get['filter_questioner'], ENT_QUOTES, 'UTF-8'));
			}
			
			if (isset($this->request->get['filter_question_category'])) {
				$url .= '&filter_question_category=' . $this->request->get['filter_question_category'];
			}
			
			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}
			
			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
						
			$this->redirect($this->url->link('catalog/question', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_question_id'])) {
			$filter_question_id = $this->request->get['filter_question_id'];
		} else {
			$filter_question_id = null;
		}

		if (isset($this->request->get['filter_questioner'])) {
			$filter_questioner = $this->request->get['filter_questioner'];
		} else {
			$filter_questioner = null;
		}
		
		if (isset($this->request->get['filter_question_category'])) {
			$filter_question_category = $this->request->get['filter_question_category'];
		} else {
			$filter_question_category = null;
		}
		
		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}
		
		if (isset($this->request->get['filter_date_added'])) {
			$filter_date_added = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = null;
		}
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'date_added';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
				
		$url = '';
		
		if (isset($this->request->get['filter_question_id'])) {
			$url .= '&filter_question_id=' . $this->request->get['filter_question_id'];
		}
		
		if (isset($this->request->get['filter_questioner'])) {
			$url .= '&filter_questioner=' . urlencode(html_entity_decode($this->request->get['filter_questioner'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_question_category'])) {
			$url .= '&filter_question_category=' . $this->request->get['filter_question_category'];
		}
		
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		
		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/question', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
							
		$this->data['insert'] = $this->url->link('catalog/question/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/question/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['questions'] = array();

		$data = array(
			'filter_question_id' 		=> $filter_question_id,
			'filter_questioner' 		=> $filter_questioner,
			'filter_question_category'	=> $filter_question_category,
			'filter_status'   			=> $filter_status,
			'filter_date_added'     	=> $filter_date_added,
			'sort'  					=> $sort,
			'order' 					=> $order,
			'start' 					=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' 					=> $this->config->get('config_admin_limit')
		);
		
		$question_total = $this->model_catalog_question->getTotalQuestions();

		$results = $this->model_catalog_question->getQuestions($data);
 
    	foreach ($results as $result) {
			$action = array();
			
			if ($this->config->get('config_buttons_apply') == 0) {
				$action[] = array(
					'text' => $this->language->get('text_edit'),
					'href' => $this->url->link('catalog/question/update', 'token=' . $this->session->data['token'] . '&question_id=' . $result['question_id'] . $url, 'SSL')
				);
			} else {
				$action[] = array(
					'text' => $this->language->get('button_edites'),
					'href' => $this->url->link('catalog/question/update', 'token=' . $this->session->data['token'] . '&question_id=' . $result['question_id'] . $url, 'SSL')
				);
			}
						
			$this->data['questions'][] = array(
				'question_id'  		=> $result['question_id'],
				'questioner'    	=> $result['questioner'],
				'question_category' => ($result['question_category'] ? $this->language->get('text_question_product') : $this->language->get('text_question_no_product')),
				'status'     		=> ($result['status'] ? '<span style="color:green;">'.$this->language->get('text_answered').'</span>' : '<span style="color:red;">'.$this->language->get('text_unanswered').'</span>'),
				'date_added' 		=> date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'time_added' 		=> date($this->language->get('time_format'), strtotime($result['date_added'])),
				'selected'   		=> isset($this->request->post['selected']) && in_array($result['question_id'], $this->request->post['selected']),
				'action'     		=> $action
			);
		}	
	
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_question_product'] = $this->language->get('text_question_product');
		$this->data['text_question_no_product'] = $this->language->get('text_question_no_product');
		$this->data['text_answered'] = $this->language->get('text_answered');
		$this->data['text_unanswered'] = $this->language->get('text_unanswered');

		$this->data['column_question_id'] = $this->language->get('column_question_id');
		$this->data['column_questioner'] = $this->language->get('column_questioner');
		$this->data['column_question_category'] = $this->language->get('column_question_category');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_date_added'] = $this->language->get('column_date_added');
		$this->data['column_action'] = $this->language->get('column_action');		
		
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		
		$this->data['token'] = $this->session->data['token'];
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';
		
		if (isset($this->request->get['filter_question_id'])) {
			$url .= '&filter_question_id=' . $this->request->get['filter_question_id'];
		}
		
		if (isset($this->request->get['filter_questioner'])) {
			$url .= '&filter_questioner=' . urlencode(html_entity_decode($this->request->get['filter_questioner'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_question_category'])) {
			$url .= '&filter_question_category=' . $this->request->get['filter_question_category'];
		}
		
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		
		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_question_id'] = $this->url->link('catalog/question', 'token=' . $this->session->data['token'] . '&sort=question_id' . $url, 'SSL');
		$this->data['sort_question_category'] = $this->url->link('catalog/question', 'token=' . $this->session->data['token'] . '&sort=question_category' . $url, 'SSL');
		$this->data['sort_questioner'] = $this->url->link('catalog/question', 'token=' . $this->session->data['token'] . '&sort=questioner' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('catalog/question', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
		$this->data['sort_date_added'] = $this->url->link('catalog/question', 'token=' . $this->session->data['token'] . '&sort=date_added' . $url, 'SSL');
		
		$url = '';
		
		if (isset($this->request->get['filter_question_id'])) {
			$url .= '&filter_question_id=' . $this->request->get['filter_question_id'];
		}
		
		if (isset($this->request->get['filter_questioner'])) {
			$url .= '&filter_questioner=' . urlencode(html_entity_decode($this->request->get['filter_questioner'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_question_category'])) {
			$url .= '&filter_question_category=' . $this->request->get['filter_question_category'];
		}
		
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		
		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $question_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/question', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
			
		$this->data['pagination'] = $pagination->render();
		
		$this->data['filter_question_id'] = $filter_question_id;
		$this->data['filter_questioner'] = $filter_questioner;
		$this->data['filter_question_category'] = $filter_question_category;
		$this->data['filter_status'] = $filter_status;
		$this->data['filter_date_added'] = $filter_date_added;
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'catalog/question_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title_question');
		
		$this->load->model('setting/setting');

		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_answered'] = $this->language->get('text_answered');
		$this->data['text_unanswered'] = $this->language->get('text_unanswered');
		$this->data['text_question_product'] = $this->language->get('text_question_product');
		$this->data['text_question_no_product'] = $this->language->get('text_question_no_product');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_select'] = $this->language->get('text_select');

		$this->data['entry_category'] = $this->language->get('entry_category');
		$this->data['entry_product'] = $this->language->get('entry_product');
		$this->data['entry_subject'] = $this->language->get('entry_subject');
		$this->data['entry_questioner'] = $this->language->get('entry_questioner');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_text'] = $this->language->get('entry_text');
		$this->data['entry_answer'] = $this->language->get('entry_answer');
		$this->data['entry_mail_quistioner'] = $this->language->get('entry_mail_quistioner');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
 		
		if (isset($this->error['product'])) {
			$this->data['error_product'] = $this->error['product'];
		} else {
			$this->data['error_product'] = '';
		}
		
		if (isset($this->error['subject'])) {
			$this->data['error_subject'] = $this->error['subject'];
		} else {
			$this->data['error_subject'] = '';
		}
		
 		if (isset($this->error['questioner'])) {
			$this->data['error_questioner'] = $this->error['questioner'];
		} else {
			$this->data['error_questioner'] = '';
		}
		
 		if (isset($this->error['text_question'])) {
			$this->data['error_text_question'] = $this->error['text_question'];
		} else {
			$this->data['error_text_question'] = '';
		}

		$url = '';
		
		if (isset($this->request->get['filter_question_id'])) {
			$url .= '&filter_question_id=' . $this->request->get['filter_question_id'];
		}
		
		if (isset($this->request->get['filter_questioner'])) {
			$url .= '&filter_questioner=' . urlencode(html_entity_decode($this->request->get['filter_questioner'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_question_category'])) {
			$url .= '&filter_question_category=' . $this->request->get['filter_question_category'];
		}
		
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		
		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
				
   		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/question', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
										
		if (!isset($this->request->get['question_id'])) { 
			$this->data['action'] = $this->url->link('catalog/question/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/question/update', 'token=' . $this->session->data['token'] . '&question_id=' . $this->request->get['question_id'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('catalog/question', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['question_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$question_info = $this->model_catalog_question->getQuestion($this->request->get['question_id']);
		}
		
		$this->data['token'] = $this->session->data['token'];
			
		$this->load->model('catalog/product');
		
		if (isset($this->request->post['product_id'])) {
			$this->data['product_id'] = $this->request->post['product_id'];
		} elseif (!empty($question_info)) {
			$this->data['product_id'] = $question_info['product_id'];
		} else {
			$this->data['product_id'] = '';
		}

		if (isset($this->request->post['product'])) {
			$this->data['product'] = $this->request->post['product'];
		} elseif (!empty($question_info)) {
			$this->data['product'] = $question_info['product'];
		} else {
			$this->data['product'] = '';
		}
		
		if (isset($this->request->post['subject'])) {
			$this->data['subject'] = $this->request->post['subject'];
		} elseif (!empty($question_info)) {
			$this->data['subject'] = $question_info['subject'];
		} else {
			$this->data['subject'] = '';
		}
				
		if (isset($this->request->post['questioner'])) {
			$this->data['questioner'] = $this->request->post['questioner'];
		} elseif (!empty($question_info)) {
			$this->data['questioner'] = $question_info['questioner'];
		} else {
			$this->data['questioner'] = '';
		}
		
		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} elseif (!empty($question_info)) {
			$this->data['email'] = $question_info['email'];
		} else {
			$this->data['email'] = '';
		}

		if (isset($this->request->post['text_question'])) {
			$this->data['text_question'] = $this->request->post['text_question'];
		} elseif (!empty($question_info)) {
			$this->data['text_question'] = $question_info['text_question'];
		} else {
			$this->data['text_question'] = '';
		}
		
		if (isset($this->request->post['text_answer'])) {
			$this->data['text_answer'] = $this->request->post['text_answer'];
		} elseif (!empty($question_info)) {
			$this->data['text_answer'] = $question_info['text_answer'];
		} else {
			$this->data['text_answer'] = '';
		}
		
		if (isset($this->request->post['question_category'])) {
			$this->data['question_category'] = $this->request->post['question_category'];
		} elseif (!empty($question_info)) {
			$this->data['question_category'] = $question_info['question_category'];
		} else {
			$this->data['question_category'] = 1;
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($question_info)) {
			$this->data['status'] = $question_info['status'];
		} else {
			$this->data['status'] = '';
		}
		
		if (isset($this->request->post['notify'])) {
			$this->data['notify'] = $this->request->post['notify'];
		} elseif (!empty($question_info)) {
			$this->data['notify'] = $question_info['notify'];
		} else {
			$this->data['notify'] = '';
		}
		
		if (isset($this->request->post['customer_id'])) {
			$this->data['customer_id'] = $this->request->post['customer_id'];
		} elseif (!empty($question_info)) {
			$this->data['customer_id'] = $question_info['customer_id'];
		} else {
			$this->data['customer_id'] = '';
		}

		$this->template = 'catalog/question_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/question')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ($this->request->post['question_category']) {
			if (!$this->request->post['product_id']) {
				$this->error['product'] = $this->language->get('error_product');
			}		
		}
		
		if (!$this->request->post['question_category']) {
			if ((utf8_strlen($this->request->post['subject']) < 3) || (utf8_strlen($this->request->post['subject']) > 64)) {
				$this->error['subject'] = $this->language->get('error_subject');
			}
		}
		
		if ((utf8_strlen($this->request->post['questioner']) < 3) || (utf8_strlen($this->request->post['questioner']) > 64)) {
			$this->error['questioner'] = $this->language->get('error_questioner');
		}

		if (utf8_strlen($this->request->post['text_question']) < 1) {
			$this->error['text_question'] = $this->language->get('error_text_question');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/question')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function autocomplete() {
		$json = array();
		
		if (isset($this->request->get['filter_questioner'])) {
			$this->load->model('catalog/question');
			
			if (isset($this->request->get['filter_questioner'])) {
				$filter_questioner = $this->request->get['filter_questioner'];
			} else {
				$filter_questioner = '';
			}			
			
			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];	
			} else {
				$limit = 20;	
			}			
						
			$data = array(
				'filter_questioner' => $filter_questioner,
				'start'          	=> 0,
				'limit'          	=> $limit
			);
			
			$results = $this->model_catalog_question->getQuestions($data);
			
			foreach ($results as $result) {
					
				$json[] = array(
					'question_id' => $result['question_id'],
					'questioner'  => strip_tags(html_entity_decode($result['questioner'], ENT_QUOTES, 'UTF-8'))	
				);	
			}
		}

		$this->response->setOutput(json_encode($json));
	}
}
?>