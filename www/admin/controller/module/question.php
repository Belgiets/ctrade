<?php
class ControllerModuleQuestion extends Controller {
	private $error = array();
	private $install_question = array('question_installed' => 1);
	
	public function index() {   
		$this->language->load('module/question');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('question', $this->request->post);		
			
			$this->cache->delete('product');
			
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_all'] = $this->language->get('text_all');
		$this->data['text_customer'] = $this->language->get('text_customer');
		$this->data['text_guest'] = $this->language->get('text_guest');
		$this->data['text_no_display'] = $this->language->get('text_no_display');		
		$this->data['text_no_delete'] = $this->language->get('text_no_delete');
		$this->data['text_product_delete'] = $this->language->get('text_product_delete');
		$this->data['text_no_product_delete'] = $this->language->get('text_no_product_delete');
		$this->data['text_all_delete'] = $this->language->get('text_all_delete');		
		$this->data['text_all_allowed'] = $this->language->get('text_all_allowed');
		$this->data['text_no_allowed'] = $this->language->get('text_no_allowed');
		$this->data['text_customer_allowed'] = $this->language->get('text_customer_allowed');
		$this->data['text_guest_allowed'] = $this->language->get('text_guest_allowed');
		
		$this->data['entry_question_product'] = $this->language->get('entry_question_product');
		$this->data['entry_guest_question_product'] = $this->language->get('entry_guest_question_product');
		$this->data['entry_mail_admin'] = $this->language->get('entry_mail_admin');
		$this->data['entry_captcha_product'] = $this->language->get('entry_captcha_product');	
		$this->data['entry_limit_question_product'] = $this->language->get('entry_limit_question_product');		
		$this->data['entry_question_account'] = $this->language->get('entry_question_account');
		$this->data['entry_mail_admin_account'] = $this->language->get('entry_mail_admin_account');
		$this->data['entry_question_customer_delete'] = $this->language->get('entry_question_customer_delete');
		$this->data['entry_mail_admin_delete'] = $this->language->get('entry_mail_admin_delete');
		$this->data['entry_captcha_account'] = $this->language->get('entry_captcha_account');
		$this->data['entry_limit_question_account'] = $this->language->get('entry_limit_question_account');		
		$this->data['entry_question_faq'] = $this->language->get('entry_question_faq');
		$this->data['entry_allowed_faq'] = $this->language->get('entry_allowed_faq');
		$this->data['entry_mail_admin_faq'] = $this->language->get('entry_mail_admin_faq');
		$this->data['entry_captcha_faq'] = $this->language->get('entry_captcha_faq');
		$this->data['entry_limit_question_faq'] = $this->language->get('entry_limit_question_faq');
		
		$this->data['tab_product'] = $this->language->get('tab_product');
		$this->data['tab_account'] = $this->language->get('tab_account');
		$this->data['tab_faq'] = $this->language->get('tab_faq');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/question', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/question', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['config_question_product'])) {
			$this->data['config_question_product'] = $this->request->post['config_question_product'];
		} else {
			$this->data['config_question_product'] = $this->config->get('config_question_product');
		}
		
		if (isset($this->request->post['config_guest_question_product'])) {
			$this->data['config_guest_question_product'] = $this->request->post['config_guest_question_product'];
		} else {
			$this->data['config_guest_question_product'] = $this->config->get('config_guest_question_product');
		}
		
		if (isset($this->request->post['config_question_mail_admin'])) {
			$this->data['config_question_mail_admin'] = $this->request->post['config_question_mail_admin'];
		} else {
			$this->data['config_question_mail_admin'] = $this->config->get('config_question_mail_admin');
		}
		
		if (isset($this->request->post['config_captcha_product_display'])) {
			$this->data['config_captcha_product_display'] = $this->request->post['config_captcha_product_display'];
		} else {
			$this->data['config_captcha_product_display'] = $this->config->get('config_captcha_product_display');
		}
		
		if (isset($this->request->post['config_limit_question_product'])) {
			$this->data['config_limit_question_product'] = $this->request->post['config_limit_question_product'];
		} else {
			$this->data['config_limit_question_product'] = $this->config->get('config_limit_question_product');
		}
		
		if (isset($this->request->post['config_question_account'])) {
			$this->data['config_question_account'] = $this->request->post['config_question_account'];
		} else {
			$this->data['config_question_account'] = $this->config->get('config_question_account');
		}
		
		if (isset($this->request->post['config_question_mail_admin_account'])) {
			$this->data['config_question_mail_admin_account'] = $this->request->post['config_question_mail_admin_account'];
		} else {
			$this->data['config_question_mail_admin_account'] = $this->config->get('config_question_mail_admin_account');
		}
		
		if (isset($this->request->post['config_question_customer_delete'])) {
			$this->data['config_question_customer_delete'] = $this->request->post['config_question_customer_delete'];
		} else {
			$this->data['config_question_customer_delete'] = $this->config->get('config_question_customer_delete');
		}
		
		if (isset($this->request->post['config_question_mail_admin_delete'])) {
			$this->data['config_question_mail_admin_delete'] = $this->request->post['config_question_mail_admin_delete'];
		} else {
			$this->data['config_question_mail_admin_delete'] = $this->config->get('config_question_mail_admin_delete');
		}
		
		if (isset($this->request->post['config_captcha_account_display'])) {
			$this->data['config_captcha_account_display'] = $this->request->post['config_captcha_account_display'];
		} else {
			$this->data['config_captcha_account_display'] = $this->config->get('config_captcha_account_display');
		}
		
		if (isset($this->request->post['config_limit_question_account'])) {
			$this->data['config_limit_question_account'] = $this->request->post['config_limit_question_account'];
		} else {
			$this->data['config_limit_question_account'] = $this->config->get('config_limit_question_account');
		}
		
		if (isset($this->request->post['config_question_faq'])) {
			$this->data['config_question_faq'] = $this->request->post['config_question_faq'];
		} else {
			$this->data['config_question_faq'] = $this->config->get('config_question_faq');
		}
		
		if (isset($this->request->post['config_allowed_faq'])) {
			$this->data['config_allowed_faq'] = $this->request->post['config_allowed_faq'];
		} else {
			$this->data['config_allowed_faq'] = $this->config->get('config_allowed_faq');
		}
		
		if (isset($this->request->post['config_question_mail_admin_faq'])) {
			$this->data['config_question_mail_admin_faq'] = $this->request->post['config_question_mail_admin_faq'];
		} else {
			$this->data['config_question_mail_admin_faq'] = $this->config->get('config_question_mail_admin_faq');
		}
		
		if (isset($this->request->post['config_captcha_faq_display'])) {
			$this->data['config_captcha_faq_display'] = $this->request->post['config_captcha_faq_display'];
		} else {
			$this->data['config_captcha_faq_display'] = $this->config->get('config_captcha_faq_display');
		}
		
		if (isset($this->request->post['config_limit_question_faq'])) {
			$this->data['config_limit_question_faq'] = $this->request->post['config_limit_question_faq'];
		} else {
			$this->data['config_limit_question_faq'] = $this->config->get('config_limit_question_faq');
		}

		$this->template = 'module/question.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/question')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}	
						
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
	public function install() {
		$this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "question (question_id int(11) NOT NULL AUTO_INCREMENT, product_id int(11) NOT NULL, product varchar(128) NOT NULL, customer_id int(11) NOT NULL, questioner varchar(64) NOT NULL, email varchar(96) NOT NULL, subject varchar(64) NOT NULL, text_question text NOT NULL, text_answer text NOT NULL, question_category tinyint(1) NOT NULL DEFAULT '0', status tinyint(1) NOT NULL DEFAULT '0', notify tinyint(1) NOT NULL DEFAULT '0', date_added datetime NOT NULL DEFAULT '0000-00-00 00:00:00', date_modified datetime NOT NULL DEFAULT '0000-00-00 00:00:00', PRIMARY KEY (question_id), KEY product_id (product_id)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");

		$this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('question_installed', $this->install_question);
	}
	
	public function uninstall() {
		$this->db->query("DROP TABLE IF EXISTS " . DB_PREFIX . "question");
		
		$this->load->model('setting/setting');
        $this->model_setting_setting->deleteSetting('question_installed');
	}
}
?>