<?php
class ControllerModulebest extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->language->load('module/best');

		$this->document->setTitle($this->language->get('heading_title_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {			
			$this->model_setting_setting->editSetting('best', $this->request->post);		
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['heading_title_title'] = $this->language->get('heading_title_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] = $this->language->get('text_column_left');
		$this->data['text_column_right'] = $this->language->get('text_column_right');
		
		$this->data['entry_product_day'] = $this->language->get('entry_product_day');
		$this->data['entry_product_uspey'] = $this->language->get('entry_product_uspey');
		$this->data['entry_product'] = $this->language->get('entry_product');
		$this->data['entry_limit'] = $this->language->get('entry_limit');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_position'] = $this->language->get('entry_position');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$this->data['entry_addit_image'] = $this->language->get('entry_addit_image');
		$this->data['off_product_day'] = $this->language->get('off_product_day');
		$this->data['off_addit'] = $this->language->get('off_addit');
		$this->data['schetchik'] = $this->language->get('schetchik');
		$this->data['rating_and_prodano'] = $this->language->get('rating_and_prodano');
		$this->data['responsive_setting'] = $this->language->get('responsive_setting');
		$this->data['responsive'] = $this->language->get('responsive');
		$this->data['kolvo_responsive_setting'] = $this->language->get('kolvo_responsive_setting');
		$this->data['on'] = $this->language->get('on');
		$this->data['off'] = $this->language->get('off');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['image'])) {
			$this->data['error_image'] = $this->error['image'];
		} else {
			$this->data['error_image'] = array();
		}
				
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/best', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/best', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->post['best_product_day'])) {
			$this->data['best_product_day'] = $this->request->post['best_product_day'];
		} else {
			$this->data['best_product_day'] = $this->config->get('best_product_day');
		}
		
		if (isset($this->request->post['best_product_uspey'])) {
			$this->data['best_product_uspey'] = $this->request->post['best_product_uspey'];
		} else {
			$this->data['best_product_uspey'] = $this->config->get('best_product_uspey');
		}	
				
		$this->load->model('catalog/product');
				
		if (isset($this->request->post['best_product'])) {
			$products = explode(',', $this->request->post['best_product']);
		} else {		
			$products = explode(',', $this->config->get('best_product'));
		}
		
		if (isset($this->request->post['best_product_day'])) {
			$products_day = explode(',', $this->request->post['best_product_day']);
		} else {		
			$products_day = explode(',', $this->config->get('best_product_day'));
		}
		
		$this->data['products_day'] = array();
		
		foreach ($products_day as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);
			
			if ($product_info) {
				$this->data['products_day'][] = array(
					'product_id' => $product_info['product_id'],
					'name'       => $product_info['name']
				);
			}
		}	
		
		if (isset($this->request->post['best_product_uspey'])) {
			$products_uspey = explode(',', $this->request->post['best_product_uspey']);
		} else {		
			$products_uspey = explode(',', $this->config->get('best_product_uspey'));
		}
		
		$this->data['products_uspey'] = array();
		
		foreach ($products_uspey as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);
			
			if ($product_info) {
				$this->data['products_uspey'][] = array(
					'product_id' => $product_info['product_id'],
					'name'       => $product_info['name']
				);
			}
		}
			
		$this->data['modules'] = array();
		
		if (isset($this->request->post['best_module'])) {
			$this->data['modules'] = $this->request->post['best_module'];
		} elseif ($this->config->get('best_module')) { 
			$this->data['modules'] = $this->config->get('best_module');
		}
		
		$this->data['modules_setting'] = array();
		
		if (isset($this->request->post['best_module_setting'])) {
			$this->data['modules_setting'] = $this->request->post['best_module_setting'];
		} elseif ($this->config->get('best_module_setting')) { 
			$this->data['modules_setting'] = $this->config->get('best_module_setting');
		}
		
		$this->data['modules_setting_get'] = "0";
		if (isset($this->request->post['best_module_setting'])) {
			$this->data['modules_setting_get'] = "0";
		} elseif ($this->config->get('best_module_setting')) { 
			$this->data['modules_setting_get'] = "1";
		}
				
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'module/best.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/best')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (isset($this->config->get['best_module'])) {
			foreach ($this->config->get['best_module'] as $key => $value) {
				if (!$value['image_width'] || !$value['image_height']) {
					$this->error['image'][$key] = $this->language->get('error_image');
				}
			}
		}
		
		if (isset($this->config->get['best_module'])) {
			foreach ($this->config->get['best_module'] as $key => $value) {
				if (!$value['image_addit_width'] || !$value['image_addit_height']) {
					$this->error['image'][$key] = $this->language->get('error_image');
				}
			}
		}
				
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>