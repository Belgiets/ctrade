<?php
class ModelCatalogQuestion extends Model {
	public function addQuestion($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "question SET questioner = '" . $this->db->escape($data['questioner']) . "', subject = '" . $this->db->escape($data['subject']) . "', product_id = '" . $this->db->escape($data['product_id']) . "', product = '" . $this->db->escape($data['product']) . "', text_question = '" . $this->db->escape($data['text_question']) . "', text_answer = '" . $this->db->escape($data['text_answer']) . "', question_category = '" . (int)$data['question_category'] . "', status = '" . (int)$data['status'] . "', date_added = NOW()");
	
		$this->cache->delete('product');
	}
	
	public function editQuestion($question_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "question SET questioner = '" . $this->db->escape($data['questioner']) . "', email = '" . $this->db->escape($data['email']) . "', subject = '" . $this->db->escape($data['subject']) . "', product_id = '" . $this->db->escape($data['product_id']) . "', text_question = '" . $this->db->escape($data['text_question']) . "', text_answer = '" . $this->db->escape($data['text_answer']) . "', question_category = '" . (int)$data['question_category'] . "', status = '" . (int)$data['status'] . "', notify = '" . (int)$data['notify'] . "',  date_modified = NOW() WHERE question_id = '" . (int)$question_id . "'");
	
		$this->load->model('catalog/question');		
		$question_info = $this->model_catalog_question->getQuestion($question_id);
			
		$this->load->model('catalog/product');		
		$product_info = $this->model_catalog_product->getProduct($question_info['product_id']);
			
		$this->load->model('sale/customer');
		$customer_info = $this->model_sale_customer->getCustomer($question_info['customer_id']);
		
		if ($this->db->escape($data['email'])) {
			$to_email = $this->db->escape($data['email']);
			$notify_email = true;
		} else {
			$to_email = '';
			$notify_email = false;
		}
		
		if ($this->db->escape($data['notify']) && $notify_email) {
			$this->language->load('mail/question');
			
			if ($this->db->escape($data['question_category']) == 1) {
				$question_category = $this->language->get('text_question_product');
			} else {
				$question_category = $this->language->get('text_question_no_product');
			}
			
			if ($this->db->escape($data['status']) == 1) {
				$question_status = $this->language->get('text_answered');
			} else {
				$question_status = $this->language->get('text_unanswered');
			}
			
			if ($notify_email) {
				$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));

				$message  = sprintf($this->language->get('text_waiting'), $this->config->get('config_name')) . "\n";
				if ($this->db->escape($data['question_category']) == 1) {
					$message .= sprintf($this->language->get('text_category'), $question_category) . "\n";
					$message .= sprintf($this->language->get('text_product'), $this->db->escape(strip_tags($product_info['name']))) . "\n";
				} else {
					$message .= sprintf($this->language->get('text_category'), $question_category) . "\n";
					$message .= sprintf($this->language->get('text_question_subject'), $this->db->escape(strip_tags($data['subject']))) . "\n";
				}
				
				$message .= sprintf($this->language->get('text_status'), $question_status) . "\n";
				$message .= $this->language->get('text_question') . "\n";
				$message .= html_entity_decode($data['text_question'], ENT_QUOTES, 'UTF-8');
				$message .= $this->language->get('text_answer_admin') . "\n";
				$message .= html_entity_decode($data['text_answer'], ENT_QUOTES, 'UTF-8');
				$message .= $this->language->get('text_link') . "\n";
				if ($question_info['customer_id']) {
					$message .=  html_entity_decode(HTTP_CATALOG . 'index.php?route=account/question/info&question_id=' . $question_id, ENT_QUOTES, 'UTF-8') . "\n\n";
				} else {
					$message .=  html_entity_decode(HTTP_CATALOG . 'index.php?route=information/question', ENT_QUOTES, 'UTF-8') . "\n\n";
				}
				


				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');	
				$mail->setTo($to_email);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($this->config->get('config_name'));
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				$mail->setHtml($message);
				$mail->send();

				$emails = explode(',', $this->config->get('config_alert_emails'));
				
				foreach ($emails as $email) {
					if ($email && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
						$mail->setTo($email);
						$mail->send();
					}
				}
			}
		}

		$this->cache->delete('product');
	}
	
	public function deleteQuestion($question_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "question WHERE question_id = '" . (int)$question_id . "'");
		
		$this->cache->delete('product');
	}
	
	public function getQuestion($question_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT pd.name FROM " . DB_PREFIX . "product_description pd WHERE pd.product_id = q.product_id AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS product FROM " . DB_PREFIX . "question q WHERE q.question_id = '" . (int)$question_id . "'");
		
		return $query->row;
	}

	public function getQuestions($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "question` WHERE question_id";


		if (!empty($data['filter_question_id'])) {
			$sql .= " AND question_id = '" . (int)$data['filter_question_id'] . "'";
		}

		if (!empty($data['filter_questioner'])) {
			$sql .= " AND questioner LIKE '%" . $this->db->escape($data['filter_questioner']) . "%'";
		}
		
		if (isset($data['filter_question_category']) && $data['filter_question_category'] !== null) {
			$sql .= " AND question_category = '" . (int)$data['filter_question_category'] . "'";
		}
		
		if (isset($data['filter_status']) && $data['filter_status'] !== null) {
			$sql .= " AND status = '" . (int)$data['filter_status'] . "'";
		}
		
		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}
		
		$sql .= " GROUP BY question_id";
		
		$sort_data = array(
			'question_id',
			'questioner',
			'question_category',
			'status',
			'date_added'
		);	
			
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY date_added";	
		}
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}																																							  
																																							  
		$query = $this->db->query($sql);																																				
		
		return $query->rows;	
	}
	
	public function getTotalQuestions($data = array()) {
		$sql = "SELECT COUNT(DISTINCT question_id) AS total FROM " . DB_PREFIX . "question";
		
		$sql .= " ";
		
		if (!empty($data['filter_question_id'])) {
			$sql .= " AND question_id = '" . (int)$data['filter_question_id'] . "'";
		}

		if (!empty($data['filter_questioner'])) {
			$sql .= " AND questioner LIKE '%" . $this->db->escape($data['filter_questioner']) . "%'";
		}
		
		if (isset($data['filter_question_category']) && $data['filter_question_category'] !== null) {
			$sql .= " AND question_category = '" . (int)$data['filter_question_category'] . "'";
		}
		
		if (isset($data['filter_status']) && $data['filter_status'] !== null) {
			$sql .= " AND status = '" . (int)$data['filter_status'] . "'";
		}
		
		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
}
?>