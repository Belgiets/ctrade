<?php
// Heading
$_['heading_title']       			= html_entity_decode('<span style="color: #2E85BC; font-weight: bold; font-size: 14px;">Product of the day and buy Uspey</span>', ENT_QUOTES, 'UTF-8');
$_['heading_title_title']           = 'Product of the day and buy Uspey';
$_['ostalos']        	  			= 'Specify the initial number of days <br /> goods sold to calculate the percentage of the balance<br /><span style="font-size: 9px;">(��� ����� �� ������ ���� ������ ���������� ������ � �������)</span>';
$_['style_best']          			= 'Style settings module';
$_['color_best_day']      			= 'Color block Product of the day';
$_['header_color_best_day']         = 'Header text color block Product of the day';
$_['header_color_best_uspey']       = 'Header text color block Uspey buy';
$_['style_best']          			= 'Style settings module';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module best!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

// Entry
$_['entry_product']       = 'Products:<br /><span class="help">(Autocomplete)</span>';
$_['off_product_day']     = 'Mapping unit product of the day';
$_['off_addit']   		  = 'Additional product image';
$_['schetchik']   		  = 'Counter stocks end';
$_['rating_and_prodano']  = 'Rating and balance indicator';
$_['responsive_setting']  = 'Setting Responsive';
$_['kolvo_responsive_setting']  = 'The number of visible items in a block buy Uspey';
$_['responsive']  		  = 'Adaptability module<br><span style="font-size: 11px;">(for adaptive templates)</span>';
$_['entry_limit']         = 'Limit:';
$_['entry_image']         = 'Image (W x H) and Resize Type:';
$_['entry_image']         = 'Additional image (W x H) and Resize Type:';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';
$_['on']   				  = 'On';
$_['off']   			  = 'Off';

// Error 
$_['error_permission']    = 'Warning: You do not have permission to modify module best!';
$_['error_image']         = 'Image width &amp; height dimensions required!';
?>