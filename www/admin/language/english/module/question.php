<?php
// Heading
$_['heading_title']       				= 'The Question - Answer System';

// Text
$_['text_module']         				= 'Modules';
$_['text_success']        				= 'System setup questions and answers updated successfully!';
$_['text_all']        					= 'For all';
$_['text_customer']        				= 'Only for buyers';
$_['text_guest']        				= 'Only for guests';
$_['text_no_display']        			= 'No show';
$_['text_all_delete']        			= 'Customers are able to delete their questions';
$_['text_no_delete']        			= 'Customers are not allowed to delete their questions';
$_['text_product_delete']        		= 'Allowed to remove questions relating to products';
$_['text_no_product_delete']        	= 'Allowed to remove questions not related to the products';
$_['text_all_allowed']        			= 'Are allowed to ask questions';
$_['text_no_allowed']        			= 'Not allowed to ask questions';
$_['text_customer_allowed']        		= 'Customers are allowed to ask questions';
$_['text_guest_allowed']        		= 'Customers are not allowed to ask questions';

// Entry
$_['entry_question_product']         	= 'Questions on the product page:<br /><span class="help">Allow to ask questions for product on his page.</span>';
$_['entry_guest_question_product']   	= 'Allow guests to ask questions:<br /><span class="help">Allow guests to ask questions for product on his page. If not, then to ask a question, the buyer must log in.</span>';
$_['entry_mail_admin']   				= 'Notify administrator:<br /><span class="help">Send E-Mail notification to the administrator of a new question on product.</span>';
$_['entry_captcha_product']   			= 'Captcha:<br /><span class="help">Display the security code when you add a question on product for protection from spam bots.</span>';
$_['entry_limit_question_product']   	= 'Limit questions on page:';
$_['entry_question_account']         	= 'Questions in Account:<br /><span class="help">Show the buyer list of the questions, and allow him to ask questions from his personal account.</span>';
$_['entry_mail_admin_account']   		= 'Notify administrator:<br /><span class="help">Send E-Mail notification to the administrator of a new question from the customer.</span>';
$_['entry_question_customer_delete']    = 'Deleting questions:<br /><span class="help">Allow the buyer to delete their questions.</span>';
$_['entry_mail_admin_delete']         	= 'Notify about deleting:<br /><span class="help">Send E-Mail notification to the administrator about removing the buyer of their question.</span>';
$_['entry_captcha_account']   			= 'Captcha:<br /><span class="help">Show the buyer security code when adding a question of personal account.</span>';
$_['entry_limit_question_account']   	= 'Limit questions on page:';

$_['entry_question_faq']   				= 'Frequently Asked Questions (FAQ):<br /><span class="help">Enable system frequently asked questions (FAQ) in the store.</span>';
$_['entry_allowed_faq']   				= 'Permission to ask questions:<br /><span class="help">Allowed to ask questions the page frequently asked questions (FAQ).</span>';
$_['entry_mail_admin_faq']   			= 'Notify administrator:<br /><span class="help">Send E-Mail notification to the administrator on a new question.</span>';
$_['entry_captcha_faq']   				= 'Captcha:<br /><span class="help">Display the security code when you add a question for protection from spam bots.</span>';
$_['entry_limit_question_faq']   		= 'Limit questions on page (FAQ):';

// Tab
$_['tab_product']         				= 'Product';
$_['tab_account']         				= 'Account';
$_['tab_faq']         					= 'FAQ';

// Error
$_['error_permission']    				= 'Note: You do not Have enough rights to edit the settings of a system of questions and answers!';
?>