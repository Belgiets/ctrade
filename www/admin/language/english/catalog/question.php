<?php
// Heading
$_['heading_title']     		= 'List Questions';
$_['heading_title_question']	= 'Question';

// Text
$_['text_success'] 				= 'Questions updated successfully!';
$_['text_review']       		= 'Edit';
$_['text_answered']     		= 'Closed';
$_['text_unanswered']    		= 'Opened';
$_['text_question_product']     = 'Question associated with the products';
$_['text_question_no_product']  = 'Question is not associated with the products';
$_['text_ticket']               = 'Questions / Answers';
$_['text_question']             = 'List Questions';
$_['text_question_setting']     = 'Settings';

// Column
$_['column_question_id'] 		= 'Question ID';
$_['column_questioner'] 		= 'Author';
$_['column_question_category']  = 'Category Questions';
$_['column_status']     		= 'Status';
$_['column_date_added'] 		= 'Date added';
$_['column_action']     		= 'Action';

// Entry
$_['entry_category']      		= 'Category Questions:';
$_['entry_subject']      		= 'Theme:';
$_['entry_product']     		= 'Product:<br/><span class="help">(Autocomplete %)</span>';
$_['entry_questioner']  		= 'Author:';
$_['entry_text']        		= 'Question Text:';
$_['entry_answer']        		= 'Answer Text:';
$_['entry_status']      		= 'Status:';
$_['entry_mail_quistioner']     = 'Notify Author:<br/><span class="help">Notify the author of the answer to his question.</span>';

// Error
$_['error_permission']  		= 'Note: You Have no rights to change the questions!';
$_['error_product']     		= 'Should choose the product!';
$_['error_subject']      		= 'Theme question should be from 3 up to 64 characters!';
$_['error_questioner']      	= 'The name of the author must be from 3 up to 64 characters!';
$_['error_text_question']   	= 'Question text must be at least 1 character!';
?>
