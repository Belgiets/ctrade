<?php
// Text
$_['text_subject']				= '%s - The answer to your question';
$_['text_waiting']				= 'You receive the answer to your question you have asked the administration of the "<b>%s</b>"<br />';
$_['text_category']				= 'Category: <b>%s</b>';
$_['text_product']				= 'Product: <b>%s</b><br />';
$_['text_question_subject']		= 'Subject: <b>%s</b><br />';
$_['text_question']				= '<br /><b>The text of your question</b>:<br />';
$_['text_question_product']		= 'Question associated with the products<br />';
$_['text_question_no_product'] 	= 'Question is not associated with the products<br />';
$_['text_answer_admin'] 		= '<b>Answer administrator</b>:<br />';
$_['text_status']     			= 'Status: <b>%s</b>';
$_['text_answered']     		= 'Closed<br />';
$_['text_unanswered']    		= 'Opened<br />';
$_['text_link']    				= 'For more details see the answer to your question, please follow the link:<br />';
?>