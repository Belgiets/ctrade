<?php
// Heading
$_['heading_title']    = 'Volgend / Vorig product';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Succes: Instellingen gewijzigd!';
$_['text_content_top']    = 'Content bovenaan';
$_['text_content_bottom'] = 'Content onderaan';
$_['text_column_left']    = 'Column links';
$_['text_column_right']   = 'Column rechts';
$_['text_horizontal']     = 'Horizontaal';
$_['text_vertical']       = 'Verticaal';

// Entry
$_['entry_layout']     = 'Layout:';
$_['entry_position']   = 'Positie:';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sorteervolgorde:';

// Error
$_['error_permission'] = 'Waarschuwing: U heeft geen rechten deze instellingen te wijzigen!';
$_['error_dimension']  = 'Waarschuwing: Afmeting (B x H) verplicht!';
$_['error_image']      = 'Waarschuwing: Afbeelding (B x H) verplicht!';
?>