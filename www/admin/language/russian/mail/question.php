<?php
// Text
$_['text_subject']				= '%s - Ответ на ваш вопрос';
$_['text_waiting']				= 'Вам поступил ответ на ваш вопрос, который вы задали администрации "<b>%s</b>"<br />';
$_['text_category']				= 'Категория вопросов: <b>%s</b>';
$_['text_product']				= 'Товар: <b>%s</b><br />';
$_['text_question_subject']		= 'Тема вопроса: <b>%s</b><br />';
$_['text_question']				= '<br /><b>Текст вашего вопроса</b>:<br />';
$_['text_question_product']		= 'Вопрос связан с товарами<br />';
$_['text_question_no_product'] 	= 'Вопрос не связан с товарами<br />';
$_['text_answer_admin'] 		= '<b>Ответ администрации</b>:<br />';
$_['text_status']     			= 'Статус вопроса: <b>%s</b>';
$_['text_answered']     		= 'Закрыт<br />';
$_['text_unanswered']    		= 'Открыт<br />';
$_['text_link']    				= 'Более подробно ознакомиться с ответом на ваш вопрос можно по ссылке:<br />';
?>