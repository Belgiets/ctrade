<?php
// Heading
$_['heading_title']       			= html_entity_decode('<span style="color: #2E85BC; font-weight: bold; font-size: 14px;">Товар дня и Успей купить</span>', ENT_QUOTES, 'UTF-8');
$_['heading_title_title']           = 'Товар дня и Успей купить';
$_['ostalos']        	  			= 'Укажите  первоначальное количество<br />Товара дня для расчета процента проданного от остатка<br /><span style="font-size: 9px;">(это число не должно быть меньше количества товара в наличии)</span>';
$_['style_best']          			= '<b>Настройки стилей модуля</b>';
$_['color_best_day']      			= 'Цвет блока Товар дня';
$_['header_color_best_day']         = 'Цвет текста заголовка блока Товар дня';
$_['header_color_best_uspey']       = 'Цвет текста заголовка блока Успей купить';
$_['style_best']          			= '<b>Настройки стилей модуля</b>';


// Text
$_['text_module']         			= 'Модули';
$_['text_success']        			= 'Настройки модуля обновлены!';
$_['text_content_top']    			= 'Верх страницы';
$_['text_content_bottom'] 			= 'Низ страницы';
$_['text_column_left']    			= 'Левая колонка';
$_['text_column_right']   			= 'Правая колонка';

// Entry
$_['entry_product_day']   			= '<b>Укажите Товар дня:<br /><span style="font-size: 11px;">(возможен только 1 товар)</span></b>';
$_['off_product_day']   			= 'Отображение блока Товар дня';
$_['off_addit']   					= 'Дополнительные изображения товара';
$_['schetchik']   					= 'Счетчик конца акции';
$_['rating_and_prodano']   			= 'Рейтинг и индикатор остатка';
$_['responsive_setting']   			= '<b>Настройки адаптивности</b>';
$_['responsive']   					= 'Адаптивность модуля<br><span style="font-size: 11px;">(для адаптивных шаблонов)</span>';
$_['kolvo_responsive_setting']  	= 'Количество видимых товаров в блоке Успей купить';
$_['entry_product_uspey'] 			= '<b>Укажите товары для отображения в блоке "Успей купить"</b>';
$_['entry_limit']         			= 'Лимит блока "Успей купить":';
$_['entry_image']         			= 'Изображение (Ш x В):';
$_['entry_addit_image']         	= 'Дополнит. изображения (Ш x В):';
$_['entry_layout']        			= 'Макет:';
$_['entry_position']      			= 'Расположение:';
$_['entry_status']        			= 'Статус:';
$_['entry_sort_order']    			= 'Порядок сортировки:';
$_['on']   							= 'Вкл';
$_['off']   						= 'Откл';

// Error
$_['error_permission']    			= 'У Вас нет прав для управления этим модулем!';
$_['error_image']         			= 'Необходимо указать размеры изображения!';
?>