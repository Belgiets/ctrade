<?php
// Heading
$_['heading_title']     		= 'Список вопросов';
$_['heading_title_question']	= 'Вопрос';

// Text
$_['text_success'] 				= 'Вопросы успешно обновлены!';
$_['text_review']       		= 'Редактировать';
$_['text_answered']     		= 'Закрыт';
$_['text_unanswered']    		= 'Открыт';
$_['text_question_product']     = 'Вопрос связан с товарами';
$_['text_question_no_product']  = 'Вопрос не связан с товарами';
$_['text_ticket']               = 'Вопросы / Ответы';
$_['text_question']             = 'Список вопросов';
$_['text_question_setting']     = 'Настройки';

// Column
$_['column_question_id'] 		= '№ вопроса';
$_['column_questioner'] 		= 'Автор вопроса';
$_['column_question_category']  = 'Категория вопроса';
$_['column_status']     		= 'Статус';
$_['column_date_added'] 		= 'Дата добавления';
$_['column_action']     		= 'Действие';

// Entry
$_['entry_category']      		= 'Категория вопроса:';
$_['entry_subject']      		= 'Тема:';
$_['entry_product']     		= 'Товар:<br/><span class="help">(Для автозаполнения введите знак %)</span>';
$_['entry_questioner']  		= 'Автор вопроса:';
$_['entry_text']        		= 'Текст вопроса:';
$_['entry_answer']        		= 'Текст ответа:';
$_['entry_status']      		= 'Статус вопроса:';
$_['entry_mail_quistioner']     = 'Уведомить автора:<br/><span class="help">Уведомить автора об ответе на его вопрос.</span>';

// Error
$_['error_permission']  		= 'Внимание: У Вас нет прав для изменения вопросов!';
$_['error_product']     		= 'Необходимо выбрать товар!';
$_['error_subject']      		= 'Тема вопроса должна быть от 3 до 64 символов!';
$_['error_questioner']      	= 'Имя автора должно быть от 3 до 64 символов!';
$_['error_text_question']   	= 'Текст вопроса должен быть не менее 1 символа!';
?>
