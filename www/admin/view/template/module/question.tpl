<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <div id="tabs" class="htabs"><a href="#tab-product"><?php echo $tab_product; ?></a><a href="#tab-account"><?php echo $tab_account; ?></a><a href="#tab-faq"><?php echo $tab_faq; ?></a></div>
	  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-product">
          <table class="form">
            <tr>
              <td><?php echo $entry_question_product; ?></td>
              <td><?php if ($config_question_product) { ?>
                <input type="radio" name="config_question_product" value="1" checked="checked" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_question_product" value="0" />
                <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="config_question_product" value="1" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_question_product" value="0" checked="checked" />
                <?php echo $text_no; ?>
                <?php } ?></td>
            </tr>
			<tr>
              <td><?php echo $entry_guest_question_product; ?></td>
              <td><?php if ($config_guest_question_product) { ?>
                <input type="radio" name="config_guest_question_product" value="1" checked="checked" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_guest_question_product" value="0" />
                <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="config_guest_question_product" value="1" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_guest_question_product" value="0" checked="checked" />
                <?php echo $text_no; ?>
                <?php } ?></td>
            </tr>
			<tr>
              <td><?php echo $entry_mail_admin; ?></td>
              <td><?php if ($config_question_mail_admin) { ?>
                <input type="radio" name="config_question_mail_admin" value="1" checked="checked" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_question_mail_admin" value="0" />
                <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="config_question_mail_admin" value="1" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_question_mail_admin" value="0" checked="checked" />
                <?php echo $text_no; ?>
                <?php } ?></td>
            </tr>
			<tr>
              <td><?php echo $entry_captcha_product; ?></td>
			  <td><select name="config_captcha_product_display">
				<?php if ($config_captcha_product_display == 'all') { ?>
                <option value="all" selected="selected"><?php echo $text_all; ?></option>
                <?php } else { ?>
                <option value="all"><?php echo $text_all; ?></option>
                <?php } ?>
				<?php if ($config_captcha_product_display == 'guest') { ?>
                <option value="guest" selected="selected"><?php echo $text_guest; ?></option>
                <?php } else { ?>
                <option value="guest"><?php echo $text_guest; ?></option>
				<?php } ?>
				<?php if ($config_captcha_product_display == 'customer') { ?>
                <option value="customer" selected="selected"><?php echo $text_customer; ?></option>
                <?php } else { ?>
                <option value="customer"><?php echo $text_customer; ?></option>
				<?php } ?>
				<?php if ($config_captcha_product_display == 'no') { ?>
                <option value="no" selected="selected"><?php echo $text_no_display; ?></option>
                <?php } else { ?>
                <option value="no"><?php echo $text_no_display; ?></option>
                <?php } ?>
              </select></td>
            </tr>
			<tr>
              <td><?php echo $entry_limit_question_product; ?></td>
              <td><input type="text" name="config_limit_question_product" value="<?php echo $config_limit_question_product; ?>" size="2" /></td>
            </tr>
          </table>
		</div>
		<div id="tab-account">
          <table class="form">
            <tr>
              <td><?php echo $entry_question_account; ?></td>
              <td><?php if ($config_question_account) { ?>
                <input type="radio" name="config_question_account" value="1" checked="checked" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_question_account" value="0" />
                <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="config_question_account" value="1" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_question_account" value="0" checked="checked" />
                <?php echo $text_no; ?>
                <?php } ?></td>
            </tr>
			<tr>
              <td><?php echo $entry_mail_admin_account; ?></td>
              <td><?php if ($config_question_mail_admin_account) { ?>
                <input type="radio" name="config_question_mail_admin_account" value="1" checked="checked" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_question_mail_admin_account" value="0" />
                <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="config_question_mail_admin_account" value="1" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_question_mail_admin_account" value="0" checked="checked" />
                <?php echo $text_no; ?>
                <?php } ?></td>
            </tr>
			<tr>
              <td><?php echo $entry_question_customer_delete; ?></td>
			  <td><select name="config_question_customer_delete">
				<?php if ($config_question_customer_delete == 'all') { ?>
                <option value="all" selected="selected"><?php echo $text_all_delete; ?></option>
                <?php } else { ?>
                <option value="all"><?php echo $text_all_delete; ?></option>
                <?php } ?>
				<?php if ($config_question_customer_delete == 'no') { ?>
                <option value="no" selected="selected"><?php echo $text_no_delete; ?></option>
                <?php } else { ?>
                <option value="no"><?php echo $text_no_delete; ?></option>
                <?php } ?>
				<?php if ($config_question_customer_delete == 'product') { ?>
                <option value="product" selected="selected"><?php echo $text_product_delete; ?></option>
                <?php } else { ?>
                <option value="product"><?php echo $text_product_delete; ?></option>
				<?php } ?>
				<?php if ($config_question_customer_delete == 'no_product') { ?>
                <option value="no_product" selected="selected"><?php echo $text_no_product_delete; ?></option>
                <?php } else { ?>
                <option value="no_product"><?php echo $text_no_product_delete; ?></option>
				<?php } ?>
              </select></td>
            </tr>
			<tr>
              <td><?php echo $entry_mail_admin_delete; ?></td>
              <td><?php if ($config_question_mail_admin_delete) { ?>
                <input type="radio" name="config_question_mail_admin_delete" value="1" checked="checked" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_question_mail_admin_delete" value="0" />
                <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="config_question_mail_admin_delete" value="1" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_question_mail_admin_delete" value="0" checked="checked" />
                <?php echo $text_no; ?>
                <?php } ?></td>
            </tr>
			<tr>
              <td><?php echo $entry_captcha_account; ?></td>
              <td><?php if ($config_captcha_account_display) { ?>
                <input type="radio" name="config_captcha_account_display" value="1" checked="checked" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_captcha_account_display" value="0" />
                <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="config_captcha_account_display" value="1" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_captcha_account_display" value="0" checked="checked" />
                <?php echo $text_no; ?>
                <?php } ?></td>
            </tr>
			<tr>
              <td><?php echo $entry_limit_question_account; ?></td>
              <td><input type="text" name="config_limit_question_account" value="<?php echo $config_limit_question_account; ?>" size="2" /></td>
            </tr>
          </table>
		</div>
		<div id="tab-faq">
          <table class="form">
            <tr>
              <td><?php echo $entry_question_faq; ?></td>
              <td><?php if ($config_question_faq) { ?>
                <input type="radio" name="config_question_faq" value="1" checked="checked" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_question_faq" value="0" />
                <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="config_question_faq" value="1" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_question_faq" value="0" checked="checked" />
                <?php echo $text_no; ?>
                <?php } ?></td>
            </tr>
			<tr>
              <td><?php echo $entry_allowed_faq; ?></td>
			  <td><select name="config_allowed_faq">
				<?php if ($config_allowed_faq == 'all') { ?>
                <option value="all" selected="selected"><?php echo $text_all_allowed; ?></option>
                <?php } else { ?>
                <option value="all"><?php echo $text_all_allowed; ?></option>
                <?php } ?>
				<?php if ($config_allowed_faq == 'no') { ?>
                <option value="no" selected="selected"><?php echo $text_no_allowed; ?></option>
                <?php } else { ?>
                <option value="no"><?php echo $text_no_allowed; ?></option>
                <?php } ?>
				<?php if ($config_allowed_faq == 'guest') { ?>
                <option value="guest" selected="selected"><?php echo $text_guest_allowed; ?></option>
                <?php } else { ?>
                <option value="guest"><?php echo $text_guest_allowed; ?></option>
				<?php } ?>
				<?php if ($config_allowed_faq == 'customer') { ?>
                <option value="customer" selected="selected"><?php echo $text_customer_allowed; ?></option>
                <?php } else { ?>
                <option value="customer"><?php echo $text_customer_allowed; ?></option>
				<?php } ?>
			  </select></td>
            </tr>
			<tr>
              <td><?php echo $entry_mail_admin_faq; ?></td>
              <td><?php if ($config_question_mail_admin_faq) { ?>
                <input type="radio" name="config_question_mail_admin_faq" value="1" checked="checked" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_question_mail_admin_faq" value="0" />
                <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="config_question_mail_admin_faq" value="1" />
                <?php echo $text_yes; ?>
                <input type="radio" name="config_question_mail_admin_faq" value="0" checked="checked" />
                <?php echo $text_no; ?>
                <?php } ?></td>
            </tr>
			<tr>
              <td><?php echo $entry_captcha_faq; ?></td>
			  <td><select name="config_captcha_faq_display">
				<?php if ($config_captcha_faq_display == 'all') { ?>
                <option value="all" selected="selected"><?php echo $text_all; ?></option>
                <?php } else { ?>
                <option value="all"><?php echo $text_all; ?></option>
                <?php } ?>
				<?php if ($config_captcha_faq_display == 'guest') { ?>
                <option value="guest" selected="selected"><?php echo $text_guest; ?></option>
                <?php } else { ?>
                <option value="guest"><?php echo $text_guest; ?></option>
				<?php } ?>
				<?php if ($config_captcha_faq_display == 'customer') { ?>
                <option value="customer" selected="selected"><?php echo $text_customer; ?></option>
                <?php } else { ?>
                <option value="customer"><?php echo $text_customer; ?></option>
				<?php } ?>
				<?php if ($config_captcha_faq_display == 'no') { ?>
                <option value="no" selected="selected"><?php echo $text_no_display; ?></option>
                <?php } else { ?>
                <option value="no"><?php echo $text_no_display; ?></option>
                <?php } ?>
              </select></td>
            </tr>
			<tr>
              <td><?php echo $entry_limit_question_faq; ?></td>
              <td><input type="text" name="config_limit_question_faq" value="<?php echo $config_limit_question_faq; ?>" size="2" /></td>
            </tr>
          </table>
		</div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
//--></script> 
<?php echo $footer; ?>