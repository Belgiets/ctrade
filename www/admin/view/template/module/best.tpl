<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td rowspan=2><?php echo $entry_product_day; ?></td>
			<?php $name = ""; ?>
			<?php foreach ($products_day as $product_day) {
				if ($product_day['name']) {$name = $product_day['name'];}
			} ?>
            <td><input type="text" name="product_day" value="<?php echo $name; ?>" id="inputday" style="width: 80%;" />
			<div id="best-product-day" class="scrollbox-day"></div>
              <input type="hidden" name="best_product_day" value="<?php echo $best_product_day; ?>" />
			</td>
			<td style="width: 30%;"><?php echo $this->language->get('ostalos'); ?></td>
			<td>
			<?php $module_row = 0; ?>
			<?php $pervon_kol_vo = 2000; ?>
			<?php foreach ($modules_setting as $module_setting) { ?>
			<?php if (isset($module_setting['pervon_kol_vo'])) { $pervon_kol_vo = $module_setting['pervon_kol_vo']; } ?>
			<?php } ?>
			<input type="text" name="best_module_setting[<?php echo $module_row; ?>][pervon_kol_vo]" value="<?php echo $pervon_kol_vo; ?>" size="5" />
			</td>
          </tr>
		  <tr>
			<td>
			<?php echo $off_product_day; ?><br /><br />
			<?php if (!empty($modules_setting_get)) { ?>
				<?php foreach ($modules_setting as $module_setting) { ?>
					<?php if ($module_setting['off_product_day'] == '1') { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][off_product_day]" value="1" checked="checked">
					<?php } else { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][off_product_day]" value="1">
					<?php } ?>
					<?php echo $on; ?>
					<?php if ($module_setting['off_product_day'] == '0') { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][off_product_day]" value="0" checked="checked">
					<?php } else { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][off_product_day]" value="0">
					<?php } ?>
					<?php echo $off; ?>
				<?php } ?>
			<?php } ?>
			<?php if (empty($modules_setting_get)) { ?>
				<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][off_product_day]" value="1" checked="checked"><?php echo $on; ?>
				<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][off_product_day]" value="0"><?php echo $off; ?>
			<?php } ?>
			</td>
			<td>
			<?php echo $off_addit; ?><br /><br />
			<?php if (!empty($modules_setting_get)) { ?>
				<?php foreach ($modules_setting as $module_setting) { ?>
					<?php if ($module_setting['off_addit'] == '1') { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][off_addit]" value="1" checked="checked">
					<?php } else { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][off_addit]" value="1">
					<?php } ?>
					<?php echo $on; ?>
					<?php if ($module_setting['off_addit'] == '0') { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][off_addit]" value="0" checked="checked">
					<?php } else { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][off_addit]" value="0">
					<?php } ?>
					<?php echo $off; ?>
				<?php } ?>
			<?php } ?>
			<?php if (empty($modules_setting_get)) { ?>
				<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][off_addit]" value="1" checked="checked"><?php echo $on; ?>
				<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][off_addit]" value="0"><?php echo $off; ?>
			<?php } ?>
			</td>
			<td>
			<?php echo $rating_and_prodano; ?><br /><br />
			<?php if (!empty($modules_setting_get)) { ?>
				<?php foreach ($modules_setting as $module_setting) { ?>
					<?php if ($module_setting['rating_and_prodano'] == '1') { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][rating_and_prodano]" value="1" checked="checked">
					<?php } else { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][rating_and_prodano]" value="1">
					<?php } ?>
					<?php echo $on; ?>
					<?php if ($module_setting['rating_and_prodano'] == '0') { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][rating_and_prodano]" value="0" checked="checked">
					<?php } else { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][rating_and_prodano]" value="0">
					<?php } ?>
					<?php echo $off; ?>
				<?php } ?>
			<?php } ?>
			<?php if (empty($modules_setting_get)) { ?>
				<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][rating_and_prodano]" value="1" checked="checked"><?php echo $on; ?>
				<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][rating_and_prodano]" value="0"><?php echo $off; ?>
			<?php } ?>
			</td>
		  </tr>
		  <tr>
		  <td><?php echo $this->language->get('style_best'); ?></td>
		  <td><?php echo $this->language->get('color_best_day'); ?><br /><br /><input id="sc_input_color_best_day" type="text" name="best_module_setting[<?php echo $module_row; ?>][color_best_day]" value="<?php IF (isset($module_setting['color_best_day'])) {echo $module_setting['color_best_day'];} else {echo '#2E85BC';} ?>" size="8" /></td>
		  <td><?php echo $this->language->get('header_color_best_day'); ?><br /><br /><input id="sc_input_hea_color_best_day" type="text" name="best_module_setting[<?php echo $module_row; ?>][hea_color_best_day]" value="<?php IF (isset($module_setting['hea_color_best_day'])) {echo $module_setting['hea_color_best_day'];} else {echo '#FFFFFF';} ?>" size="8" /></td>
		  <td><?php echo $this->language->get('header_color_best_uspey'); ?><br /><br /><input id="sc_input_hea_color_best_uspey" type="text" name="best_module_setting[<?php echo $module_row; ?>][hea_color_best_uspey]" value="<?php IF (isset($module_setting['hea_color_best_uspey'])) {echo $module_setting['hea_color_best_uspey'];} else {echo '#333333';} ?>" size="8" /></td>
		  </tr>
          <tr>
		  <td><?php echo $entry_product_uspey; ?></td>
            <td><input type="text" name="product_uspey" value="" style="width: 80%;" /></td>
			<td><div id="best-product-uspey" class="scrollbox">
                <?php $class = 'odd'; ?>
                <?php foreach ($products_uspey as $product_uspey) { ?>
                <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                <div id="best-product-uspey<?php echo $product_uspey['product_id']; ?>" class="<?php echo $class; ?>"><?php echo $product_uspey['name']; ?> <img src="view/image/delete.png" alt="" />
                  <input type="hidden" value="<?php echo $product_uspey['product_id']; ?>" />
                </div>
                <?php } ?>
              </div>
              <input type="hidden" name="best_product_uspey" value="<?php echo $best_product_uspey; ?>" />
			</td>
			<td>
			<?php echo $schetchik; ?><br /><br />
			<?php if (!empty($modules_setting_get)) { ?>
				<?php foreach ($modules_setting as $module_setting) { ?>
					<?php if ($module_setting['schetchik'] == '1') { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][schetchik]" value="1" checked="checked">
					<?php } else { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][schetchik]" value="1">
					<?php } ?>
					<?php echo $on; ?>
					<?php if ($module_setting['schetchik'] == '0') { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][schetchik]" value="0" checked="checked">
					<?php } else { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][schetchik]" value="0">
					<?php } ?>
					<?php echo $off; ?>
				<?php } ?>
			<?php } ?>
			<?php if (empty($modules_setting_get)) { ?>
				<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][schetchik]" value="1" checked="checked"><?php echo $on; ?>
				<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][schetchik]" value="0"><?php echo $off; ?>
			<?php } ?>
			</td>
          </tr>
		  <tr>
			<td><?php echo $responsive_setting; ?></td>
			<td>
			<?php echo $responsive; ?><br /><br />
			<?php if (!empty($modules_setting_get)) { ?>
				<?php foreach ($modules_setting as $module_setting) { ?>
					<?php if ($module_setting['responsive_setting'] == '1') { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][responsive_setting]" value="1" checked="checked">
					<?php } else { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][responsive_setting]" value="1">
					<?php } ?>
					<?php echo $on; ?>
					<?php if ($module_setting['responsive_setting'] == '0') { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][responsive_setting]" value="0" checked="checked">
					<?php } else { ?>
						<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][responsive_setting]" value="0">
					<?php } ?>
					<?php echo $off; ?>
				<?php } ?>
			<?php } ?>
			<?php if (empty($modules_setting_get)) { ?>
				<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][responsive_setting]" value="1"><?php echo $on; ?>
				<input type="radio" name="best_module_setting[<?php echo $module_row; ?>][responsive_setting]" value="0" checked="checked"><?php echo $off; ?>
			<?php } ?>
			</td>
			<td>
			<?php echo $kolvo_responsive_setting; ?><br /><br />
			<?php $kol_vo_responsive_setting = 3; ?>
			<?php foreach ($modules_setting as $module_setting) { ?>
			<?php if (isset($module_setting['kol_vo_responsive_setting'])) { $kol_vo_responsive_setting = $module_setting['kol_vo_responsive_setting']; } ?>
			<?php } ?>
			<input type="text" name="best_module_setting[<?php echo $module_row; ?>][kol_vo_responsive_setting]" value="<?php echo $kol_vo_responsive_setting; ?>" size="5" />
			</td>
			</tr>
        </table>
        <table id="module" class="list">
          <thead>
            <tr>
              <td class="left"><?php echo $entry_limit; ?></td>
              <td class="left"><?php echo $entry_image; ?></td>
			  <td class="left"><?php echo $entry_addit_image; ?></td>
              <td class="left"><?php echo $entry_layout; ?></td>
              <td class="left"><?php echo $entry_position; ?></td>
              <td class="left"><?php echo $entry_status; ?></td>
              <td class="right"><?php echo $entry_sort_order; ?></td>
              <td></td>
            </tr>
          </thead>
          <?php foreach ($modules as $module) { ?>
		  <?php if (isset($module['limit'])) { ?>
          <tbody id="module-row<?php echo $module_row; ?>">
            <tr>
              <td class="left"><input type="text" name="best_module[<?php echo $module_row; ?>][limit]" value="<?php if (isset($module['limit'])) { echo $module['limit']; } else {echo "5";} ?>" size="1" /></td>
              <td class="left"><input type="text" name="best_module[<?php echo $module_row; ?>][image_width]" value="<?php if (isset($module['image_width'])) { echo $module['image_width']; } else {echo "100";} ?>" size="3" />
                <input type="text" name="best_module[<?php echo $module_row; ?>][image_height]" value="<?php if (isset($module['image_height'])) { echo $module['image_height']; } else {echo "100";} ?>" size="3" />
                <?php if (isset($error_image[$module_row])) { ?>
                <span class="error"><?php echo $error_image[$module_row]; ?></span>
                <?php } ?></td>
			  <td class="left"><input type="text" name="best_module[<?php echo $module_row; ?>][image_addit_width]" value="<?php if (isset($module['image_addit_width'])) { echo $module['image_addit_width']; } else {echo "25";} ?>" size="3" />
                <input type="text" name="best_module[<?php echo $module_row; ?>][image_addit_height]" value="<?php if (isset($module['image_addit_height'])) { echo $module['image_addit_height']; } else {echo "25";} ?>" size="3" />
                <?php if (isset($error_image[$module_row])) { ?>
                <span class="error"><?php echo $error_image[$module_row]; ?></span>
                <?php } ?></td>
              <td class="left"><select name="best_module[<?php echo $module_row; ?>][layout_id]">
                  <?php foreach ($layouts as $layout) { ?>
                  <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                  <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
              <td class="left"><select name="best_module[<?php echo $module_row; ?>][position]">
                  <?php if ($module['position'] == 'content_top') { ?>
                  <option value="content_top" selected="selected"><?php echo $text_content_top; ?></option>
                  <?php } else { ?>
                  <option value="content_top"><?php echo $text_content_top; ?></option>
                  <?php } ?>
                  <?php if ($module['position'] == 'content_bottom') { ?>
                  <option value="content_bottom" selected="selected"><?php echo $text_content_bottom; ?></option>
                  <?php } else { ?>
                  <option value="content_bottom"><?php echo $text_content_bottom; ?></option>
                  <?php } ?>
                </select></td>
              <td class="left"><select name="best_module[<?php echo $module_row; ?>][status]">
                  <?php if ($module['status']) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
              <td class="right"><input type="text" name="best_module[<?php echo $module_row; ?>][sort_order]" value="<?php if (isset($module['sort_order'])) {echo $module['sort_order'];} else {echo "1";} ?>" size="3" /></td>
              <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
            </tr>
          </tbody>
		  <?php } ?>
          <?php $module_row++; ?>
          <?php } ?>
          <tfoot>
            <tr>
              <td colspan="7"></td>
              <td class="left"><a onclick="addModule();" class="button"><?php echo $button_add_module; ?></a></td>
            </tr>
          </tfoot>
        </table>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('input[name=\'product_day\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id
					}
				}));
			}
		});
	}, 
	
	select: function(event, ui) {
		$('#best-product-day' + ui.item.value).remove();
		
		$('#inputday').val(ui.item.label);
		
		$('#best-product-day').append('<div id="best-product-day' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" alt="" /><input type="hidden" value="' + ui.item.value + '" /></div>');

		$('#best-product-day div:odd').attr('class', 'odd');
		$('#best-product-day div:even').attr('class', 'even');
		
		data = $.map($('#best-product-day input'), function(element){
			return $(element).attr('value');
		});
						
		$('input[name=\'best_product_day\']').attr('value', data.join());
					
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});

$('input[name=\'product_uspey\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('#best-product-uspey' + ui.item.value).remove();
		
		$('#best-product-uspey').append('<div id="best-product-uspey' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" alt="" /><input type="hidden" value="' + ui.item.value + '" /></div>');

		$('#best-product-uspey div:odd').attr('class', 'odd');
		$('#best-product-uspey div:even').attr('class', 'even');
		
		data = $.map($('#best-product-uspey input'), function(element){
			return $(element).attr('value');
		});
						
		$('input[name=\'best_product_uspey\']').attr('value', data.join());
					
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});

$('#best-product-day div img').live('click', function() {
	$(this).parent().remove();
	
	$('#best-product-day div:odd').attr('class', 'odd');
	$('#best-product-day div:even').attr('class', 'even');

	data = $.map($('#best-product-day input'), function(element){
		return $(element).attr('value');
	});
					
	$('input[name=\'best_product_day\']').attr('value', data.join());	
});

$('#best-product-uspey div img').live('click', function() {
	$(this).parent().remove();
	
	$('#best-product-uspey div:odd').attr('class', 'odd');
	$('#best-product-uspey div:even').attr('class', 'even');

	data = $.map($('#best-product-uspey input'), function(element){
		return $(element).attr('value');
	});
					
	$('input[name=\'best_product_uspey\']').attr('value', data.join());	
});
//--></script> 
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {	
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><input type="text" name="best_module[' + module_row + '][limit]" value="5" size="1" /></td>';
	html += '    <td class="left"><input type="text" name="best_module[' + module_row + '][image_width]" value="100" size="3" /> <input type="text" name="best_module[' + module_row + '][image_height]" value="100" size="3" /></td>';	
	html += '    <td class="left"><input type="text" name="best_module[' + module_row + '][image_addit_width]" value="25" size="3" /> <input type="text" name="best_module[' + module_row + '][image_addit_height]" value="25" size="3" /></td>';	
	html += '    <td class="left"><select name="best_module[' + module_row + '][layout_id]">';
	<?php foreach ($layouts as $layout) { ?>
	html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="best_module[' + module_row + '][position]">';
	html += '      <option value="content_top"><?php echo $text_content_top; ?></option>';
	html += '      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>';
	html += '    </select></td>';
	html += '    <td class="left"><select name="best_module[' + module_row + '][status]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="right"><input type="text" name="best_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
	html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#module tfoot').before(html);
	
	module_row++;
}
//--></script> 
<script type="text/javascript" src="view/javascript/modcoder/jquery.modcoder.excolor.js"></script>
<script type="text/javascript"><!--
	$('document').ready(function() {
	    $('#sc_input_color_best_day').modcoder_excolor();
        $('#sc_input_hea_color_best_day').modcoder_excolor();
        $('#sc_input_hea_color_best_uspey').modcoder_excolor();
	});
//--></script>
<style type="text/css">
	.scrollbox-day {
		
	}
	table.form > tbody > tr {
    border-bottom: 1px dotted #CCCCCC;
	}
	#best-product-day {
	display: none;
	}
	.scrollbox {
    height: 156px;
	}
</style>
<?php echo $footer; ?>