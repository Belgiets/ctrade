<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/review.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table id="question" class="form">
          <tr>
            <td><span class="required">*</span> <?php echo $entry_questioner; ?></td>
            <td><input type="text" name="questioner" value="<?php echo $questioner; ?>" size="50" />
              <?php if ($error_questioner) { ?>
              <span class="error"><?php echo $error_questioner; ?></span>
              <?php } ?></td>
          </tr>
		  <tr style="display:none;">
            <td><input type="text" name="email" value="<?php echo $email; ?>" /></td>
          </tr>
		  <tr>
            <td><?php echo $entry_category; ?></td>
            <td><select name="question_category">
                <?php if ($question_category) { ?>
                <option value="1" selected="selected"><?php echo $text_question_product; ?></option>
                <option value="0"><?php echo $text_question_no_product; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_question_product; ?></option>
                <option value="0" selected="selected"><?php echo $text_question_no_product; ?></option>
                <?php } ?>
              </select></td>
          </tr>
		  <tbody id="question-1" class="question">
		    <tr>
              <td><span class="required">*</span> <?php echo $entry_product; ?></td>
              <td><input type="text" name="product" value="<?php echo $product; ?>" size="50" />
                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                <?php if ($error_product) { ?>
                <span class="error"><?php echo $error_product; ?></span>
                <?php } ?></td>
			</tr>
          </tbody>
		  <tbody id="question-0" class="question">
		    <tr>
              <td><span class="required">*</span> <?php echo $entry_subject; ?></td>
              <td><input type="text" name="subject" value="<?php echo $subject; ?>" size="50" />
                <?php if ($error_subject) { ?>
                <span class="error"><?php echo $error_subject; ?></span>
                <?php } ?></td>
			</tr>
          </tbody>
          <tr>
            <td><span class="required">*</span> <?php echo $entry_text; ?></td>
            <td><textarea name="text_question" id="text-question"><?php echo $text_question; ?></textarea>
              <?php if ($error_text_question) { ?>
              <span class="error"><?php echo $error_text_question; ?></span>
              <?php } ?></td>
          </tr>
		  <tr>
            <td><?php echo $entry_answer; ?></td>
            <td><textarea name="text_answer" id="text-answer"><?php echo $text_answer; ?></textarea></td>
          </tr>
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="status">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_answered; ?></option>
                <option value="0"><?php echo $text_unanswered; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_answered; ?></option>
                <option value="0" selected="selected"><?php echo $text_unanswered; ?></option>
                <?php } ?>
              </select></td>
          </tr>
		  <?php if ($email) { ?>
		    <tr>
              <td><?php echo $entry_mail_quistioner; ?></td>
              <td><select name="notify">
                <?php if ($notify) { ?>
                <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                <option value="0"><?php echo $text_no; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_yes; ?></option>
                <option value="0" selected="selected"><?php echo $text_no; ?></option>
                <?php } ?>
              </select></td>
            </tr>
		  <?php } else { ?>
			<tr style="display:none;">
              <td><?php echo $entry_mail_quistioner; ?></td>
              <td><select name="notify">
                <?php if ($notify) { ?>
                <option value="0" selected="selected"><?php echo $text_yes; ?></option>
                <option value="1"><?php echo $text_no; ?></option>
                <?php } else { ?>
                <option value="0"><?php echo $text_yes; ?></option>
                <option value="1" selected="selected"><?php echo $text_no; ?></option>
                <?php } ?>
              </select></td>
            </tr>
		  <?php } ?>
        </table>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 
<script type="text/javascript"><!--
CKEDITOR.replace('text-question');
//--></script> 
<script type="text/javascript"><!--
CKEDITOR.replace('text-answer');
//--></script> 
<script type="text/javascript"><!--	
$('select[name=\'question_category\']').bind('change', function() {
	$('#question .question').hide();
	
	$('#question #question-' + $(this).attr('value').replace('_', '-')).show();
});

$('select[name=\'question_category\']').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('input[name=\'product\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id
					}
				}));
			}
		});
	},
	select: function(event, ui) {
		$('input[name=\'product\']').val(ui.item.label);
		$('input[name=\'product_id\']').val(ui.item.value);
		
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});
//--></script> 
<?php echo $footer; ?>