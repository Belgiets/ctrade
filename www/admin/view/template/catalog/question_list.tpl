<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/question.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a href="<?php echo $insert; ?>" class="button"><?php echo $button_insert; ?></a><a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
			  <td class="right"><?php if ($sort == 'question_id') { ?>
                <a href="<?php echo $sort_question_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_question_id; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_question_id; ?>"><?php echo $column_question_id; ?></a>
                <?php } ?></td>
			  <td class="left"><?php if ($sort == 'questioner') { ?>
                <a href="<?php echo $sort_questioner; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_questioner; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_questioner; ?>"><?php echo $column_questioner; ?></a>
                <?php } ?></td>
			  <td class="left"><?php if ($sort == 'question_category') { ?>
                <a href="<?php echo $sort_question_category; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_question_category; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_question_category; ?>"><?php echo $column_question_category; ?></a>
                <?php } ?></td>
              <td class="left"><?php if ($sort == 'status') { ?>
                <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                <?php } ?></td>
              <td class="left"><?php if ($sort == 'date_added') { ?>
                <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                <?php } ?></td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td></td>
              <td align="right"><input type="text" name="filter_question_id" value="<?php echo $filter_question_id; ?>" size="4" style="text-align: right;" /></td>
              <td><input type="text" name="filter_questioner" value="<?php echo $filter_questioner; ?>" /></td>
              <td><select name="filter_question_category" onchange="filter();">
                  <option value="*"></option>
                  <?php if ($filter_question_category) { ?>
                  <option value="1" selected="selected"><?php echo $text_question_product; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_question_product; ?></option>
                  <?php } ?>
                  <?php if (($filter_question_category !== null) && !$filter_question_category) { ?>
                  <option value="0" selected="selected"><?php echo $text_question_no_product; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_question_no_product; ?></option>
                  <?php } ?>
                </select></td>
              <td><select name="filter_status" onchange="filter();">
                  <option value="*"></option>
                  <?php if ($filter_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_answered; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_answered; ?></option>
                  <?php } ?>
                  <?php if (($filter_status !== null) && !$filter_status) { ?>
                  <option value="0" selected="selected"><?php echo $text_unanswered; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_unanswered; ?></option>
                  <?php } ?>
                </select></td>
              <td><input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" size="12" class="date" /></td>
              <td align="right"><a onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
            </tr>
			<?php if ($questions) { ?>
            <?php foreach ($questions as $question) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($question['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $question['question_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $question['question_id']; ?>" />
                <?php } ?></td>
              <td class="right"><?php echo $question['question_id']; ?></td>
			  <td class="left"><?php echo $question['questioner']; ?></td>
			  <td class="left"><?php echo $question['question_category']; ?></td>
              <td class="left"><?php echo $question['status']; ?></td>
              <td class="left"><?php echo $question['date_added']; ?> (<?php echo $question['time_added']; ?>)</td>
              <td class="right">
			    <?php foreach ($question['action'] as $action) { ?>
				  <?php if ($this->config->get('config_buttons_apply') == 0) { ?>
					[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
				  <?php } else { ?>
				    <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a>
				  <?php } ?>
                <?php } ?>
			  </td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="7"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=catalog/question&token=<?php echo $token; ?>';
	
	var filter_question_id = $('input[name=\'filter_question_id\']').attr('value');
	
	if (filter_question_id) {
		url += '&filter_question_id=' + encodeURIComponent(filter_question_id);
	}
	
	var filter_questioner = $('input[name=\'filter_questioner\']').attr('value');
	
	if (filter_questioner) {
		url += '&filter_questioner=' + encodeURIComponent(filter_questioner);
	}
	
	var filter_question_category = $('select[name=\'filter_question_category\']').attr('value');
	
	if (filter_question_category != '*') {
		url += '&filter_question_category=' + encodeURIComponent(filter_question_category);
	}
	
	var filter_status = $('select[name=\'filter_status\']').attr('value');
	
	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}
	
	var filter_date_added = $('input[name=\'filter_date_added\']').attr('value');
	
	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}
				
	location = url;
}
//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script>  
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_questioner\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/question/autocomplete&token=<?php echo $token; ?>&filter_questioner=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.questioner,
						value: item.question_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_questioner\']').val(ui.item.label);
						
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});
//--></script> 
<script type="text/javascript"><!--
$("span.tooltip").tooltip({
	track: true, 
    delay: 0, 
    showURL: false, 
    showBody: " - ", 
    fade: 250 
});
//--></script>
<?php echo $footer; ?>