</div>
<div id="footer">
<div id="container">
  <div class="column">
    <h3><?php echo $text_information; ?></h3>
    <ul>
      <li><noindex><a rel="nofollow" href="/about-us">О магазине</a></noindex></li>
	  <li><noindex><a rel="nofollow" href="/oplata-i-dostavka">Оплата и доставка</a></noindex></li>
	  <li><noindex><a rel="nofollow" href="/garantiynyy-i-negarantiynyy-remont-tsifrovoy-tehniki">Гарантия</a></noindex></li>
    </ul>
  </div>
  <div class="column">
    <h3>Поддержка</h3>
    <ul>
	<li><noindex><a rel="nofollow" href="affiliates/">Партнерская программа</a></noindex></li>
      <!--<li><noindex><a rel="nofollow" href="/servis">Сервис</a></noindex></li>-->
	  <link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/question.css" />
		<?php if ($this->config->get('config_question_faq')) { ?>
	      <li><a class="question" href="/question"><?php echo 'Вопросы (FAQ)' ?></a></noindex></li>
	    <?php } ?>
      <li><noindex><a rel="nofollow" href="<?php echo $return; ?>"><?php echo $text_return; ?></a></noindex></li>
      <li><noindex><a rel="nofollow" href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></noindex></li>
    </ul>
  </div>
  <div class="column">
    <h3>Дополнительно</h3>
    <ul>
		<li><noindex><a rel="nofollow" href="/publications"><?php echo 'Публикации'; ?></a></noindex></li>
      <li><noindex><a rel="nofollow" href="">Акции</a></noindex></li>
		<li><noindex><a rel="nofollow" href="/contact-us/">Обратная связь</a></noindex></li>
    </ul>
  </div>
  <div class="column">
  <noindex>
    <h3>Мы в соцсетях</h3>
		<div id="share42">
			<span class="share42-item" style="display:inline-block;margin:0 6px 6px 0;height:24px;">
				<a rel="nofollow" style="display:inline-block;width:24px;height:24px;margin:0;padding:0;outline:none;background:url(/share42/icons.png) -72px 0 no-repeat" href="/go.php?url=http://www.youtube.com/user/CTradeComUa" title="Мы на YouTube" target="_blank"></a></noindex>
			</span>
			<span class="share42-item" style="display:inline-block;margin:0 6px 6px 0;height:24px;">
				<a rel="nofollow" style="display:inline-block;width:24px;height:24px;margin:0;padding:0;outline:none;background:url(/share42/icons.png) -0px 0 no-repeat" href="/go.php?url=https://www.facebook.com/groups/Ctrade/" title="Мы на Facebook" target="_blank"></a></noindex>
			</span>
			<span class="share42-item" style="display:inline-block;margin:0 6px 6px 0;height:24px;">
				<a rel="nofollow" style="display:inline-block;width:24px;height:24px;margin:0;padding:0;outline:none;background:url(/share42/icons.png) -48px 0 no-repeat" href="/go.php?url=http://vk.com/ctrade" title="Мы Вконтакте" target="_blank"></a></noindex>
			</span>
			<span class="share42-item" style="display:inline-block;margin:0 6px 6px 0;height:24px;">
				<a rel="nofollow" style="display:inline-block;width:24px;height:24px;margin:0;padding:0;outline:none;background:url(/share42/icons.png) -24px 0 no-repeat" href="https://plus.google.com/103398432811087310490" title="Мы в Google+" target="_blank"></a></noindex>
			</span>
		</div>
  </noindex>
  </div>
</div>
</div>
<div id="powered">© 2008 - <?php echo date('Y') ?> C-Trade. Все права защищены.</div>

 <!-- Yandex.Metrika counter -->
<script type="text/javascript">
var yaParams = {/*Здесь параметры визита*/};
</script>

<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter12938119 = new Ya.Metrika({id:12938119,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,params:window.yaParams||{ }});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/12938119" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Google analytics -->
<script type="text/javascript">

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-29202883-1']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

</script>
<!-- /Google analytics -->
</div>
<a href="https://plus.google.com/u/0/113001556613542818556" rel="publisher"</a>

					
					<script src="jblgroup/js/easing.js" type="text/javascript"></script>
					<script src="jblgroup/js/jquery.ui.totop.min.js" type="text/javascript"></script>
					<script type="text/javascript">
						$(document).ready(function() {
							/*
							var defaults = {
								containerID: 'toTop', // fading element id
								containerHoverID: 'toTopHover', // fading element hover id
								scrollSpeed: 1200,
								easingType: 'linear' 
							};
							*/
							
							$().UItoTop({ easingType: 'easeOutQuart' });
							
						});
					</script>
				
			
<!-- star ipl -->

			<script type="text/javascript"><!--

				$(document).on('ready', function(){
					if (!Modernizr.csstransforms) {
						$('.cut_rotated span').removeClass('rotated');
						$('.cut_rotated span').addClass('horizontal');
					}

					if (!Modernizr.borderradius) {
						if (window.PIE) {
						    $('.round').each(function() {
						        PIE.attach(this);
						    });
						}
					}
				});

			//--></script>

			<!-- end ipl -->
</body></html>