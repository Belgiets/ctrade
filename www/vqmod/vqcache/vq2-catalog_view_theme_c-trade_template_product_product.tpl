<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div itemscope itemtype="http://schema.org/Product" id="content"><?php echo $content_top; ?>
  <div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><span typeof="v:Breadcrumb">
					<a href="<?php echo $breadcrumb['href']; ?>" rel="v:url" property="v:title"><?php echo $breadcrumb['text']; ?></a>
				</span>
    <?php } ?>
  </div>
  <div class="product-info">
  <h1 itemprop="name"><?php echo $heading_title; ?></h1>
				<meta itemprop="url" content="<?php echo $breadcrumb['href']; ?>" >
  <div class="rat-rew">
  	  <?php if ($review_status) { ?>
      <?php if($rating) { ?>
	  
				<div class="review">	  
			<?php } else { ?>
				<div class="review">
			<?php } ?>
        <div></div>
      </div>
      <?php } ?>
</div>
<div class="share">
		<!-- AddThis Button BEGIN -->
			<div class="share42init" data-image="<?php echo $thumb; ?>"></div>
			<script type="text/javascript" src="share42/share42.js"></script> 
		<!-- AddThis Button END --> 
        </div>
    <?php if ($thumb || $images) { ?>
    <div class="left">  
      <?php if ($thumb) { ?>
      <div class="image"><a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="colorbox"><img src="<?php echo $thumb; ?>" itemprop="image" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" /></a></div>
      <?php } ?>
      <?php if ($images) { ?>
      <div class="image-additional">
        <?php foreach ($images as $image) { ?>
        <a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="colorbox"><img src="<?php echo $image['thumb']; ?>" itemprop="image" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
        <?php } ?>
      </div>
      <?php } ?>
    </div>
    <?php } ?>
    <div class="center">
<div class="block-info">
<div class="description">
        <?php if ($manufacturer) { ?>
        <span><?php echo $text_manufacturer; ?></span> <a itemprop="manufacturer" href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a><br />
        <?php } ?>
        <!--<span><?php echo $text_model; ?></span> <nobr itemprop="model"><?php echo $model; ?></nobr><br />-->
        <?php if ($reward) { ?>
        <span><?php echo $text_reward; ?></span> <?php echo $reward; ?><br />
        <?php } ?>	
<?php echo $text_stock; ?><span class="<?php if ($stock=='Есть в наличии') { ?>stock-01<?php } elseif ($stock=='Нет в наличии'){ ?>stock-02<?php } elseif ($stock=='Ожидается поставка') { ?>stock-03<?php } elseif ($stock=='Под заказ с Китая') { ?>stock-04<?php } else{ ?>stock-05<?php } ?>"><?php echo $stock; ?></span>
<?php if ($stock=='Под заказ с Китая') { ?><br/><i>Доставка 3-4 недели. Обязательна предоплата!</i>	<?php } ?>	
</div>	

<?php if ($price) { ?>
      <div itemprop = "offers" itemscope itemtype = "http://schema.org/Offer" class="price">
				<meta itemprop="price" content="<?php echo rtrim(preg_replace("/[^0-9\.]/", "", ($special ? $special : $price)), '.'); ?>" />
				<meta itemprop="priceCurrency" content="<?php echo $md_currency; ?>" />
				<link itemprop = "availability" href = "http://schema.org/<?php echo ($md_availability ? "InStock" : "OutOfStock") ?>" />
	  <?php if ($stock!=='Снят с производства') { ?>
		<?php echo $text_price; ?>
        <?php if (!$special) { ?>
        <span id="formated_price" price="<?php echo $price_value; ?>"><?php echo $price; ?></span>
        <?php } else { ?>
        <span id="formated_price" class="price-old" price="<?php echo $price_value; ?>"><?php echo $price; /**/ ?></span> <span class="price-new"><span id="formated_special" price="<?php echo $special_value; ?>"><?php echo $special; /**/ ?></span></span>
        <?php } ?>
        <?php if ($tax) { ?>
        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $tax; ?></span><br />
        <?php } ?>
        <?php if ($points) { ?>
        <span class="reward"><small><?php echo $text_points; ?> <span id="formated_points" points="<?php echo $points; /**/ ?>"><?php echo $points; /**/ ?></span></small></span><br />
        <?php } ?>
        <?php if ($discounts) { ?>
        <div class="discount">
          <?php foreach ($discounts as $discount) { ?>
          <?php echo sprintf($text_discount, $discount['quantity'], $discount['price']); ?><br />
          <?php } ?>
        </div>
        <?php } ?>
		<?php } ?>
		<span class="links"><a onclick="addToWishList('<?php echo $product_id; ?>');"><span><?php echo $button_wishlist; ?></span></a><a onclick="addToCompare('<?php echo $product_id; ?>');"><span><?php echo $button_compare; ?></span></a></span>
      </div>
      <?php } ?>
	  
	<div class="cart">
        <?php if (($stock=='Есть в наличии') || ($stock=='Под заказ с Китая')) { ?>
          <input type="text" name="quantity" size="2" value="<?php echo $minimum; ?>" /> шт.
          <input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
		  
		<?php } ?>
		 <?php if ($stock!=='Снят с производства'){ ?>
		
			<input type="button" value="<?php echo $button_cart; ?>" id="button-cart" class="button" />
      <a id="fast_order" class="button" />Купить в 1 клик</a>
				<div style="display:none">
          <div id="fast_order_form">       
            <input id="product_name" type="hidden" value="<?php echo $heading_title; ?>">
            <input id="product_price" type="hidden" value="<?php echo ($special ? $special : $price); ?>">
            <div class="fast_order_center"><?php echo $heading_title; ?> — ваш заказ</div>
            <div class="fast_order_left">
              <p>Имя:</p>
              <p>Телефон:</p>
              <p>Комментарий:</p>
            </div>
            <div class="fast_order_right">
              <p><input type="text" id="customer_name"/></p>
              <p><input type="text" id="customer_phone"/></p>
              <p><input type="text" id="customer_message"/></p>
            </div>
            <div class="fast_order_center">
              <p id="fast_order_result">Пожалуйста, укажите ваше имя и телефон, чтобы мы могли связаться с вами</p>
              <button class="fast_order_button"><span>Быстрый заказ</span></button>
            </div>
          </div>
        </div>
  <link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/fast_order.css" />
  <script type="text/javascript" src="catalog/view/javascript/fast_order.js"></script>

		<?php } ?>
		<?php echo $content_top; ?>
        <?php if ($minimum > 1) { ?>
        <div class="minimum"><?php echo $text_minimum; ?></div>
        <?php } ?>
    </div>
	
</div>
  <?php if (($options) && (($stock=='Есть в наличии') || ($stock=='Под заказ с Китая'))){ ?>
      <div class="options" id="product_options">
        <h3>Дополнительные опции к товару :</h3>
        <?php foreach ($options as $option) { ?>
        <?php if ($option['type'] == 'select') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <span><b><?php echo $option['name']; ?>:</b></span>
          <select name="option[<?php echo $option['product_option_id']; ?>]" onchange="recalculateprice();">
              
<?php $opt_checked="checked"; ?>
            <?php foreach ($option['option_value'] as $option_value) { ?>
            <option value="<?php echo $option_value['product_option_value_id']; ?>"  points="<?php echo $option_value['points_value']; ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php echo $option_value['price_value']; ?>"><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            
        
          <?php
          if ($option_value['price_prefix'] == '*') {
            if ($option_value['price_value'] != 1.0)
              printf("(%+d%%)", round(($option_value['price_value'] * 100) - 100) );
          } else {
            echo "(".$option_value['price_prefix'].$option_value['price'].")"; 
          }
          ?>
        
      
            <?php } ?>
            </option>
            <?php } ?>
          </select>
        </div>
        <?php } ?>
        <?php if ($option['type'] == 'radio') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
<?php $opt_checked="checked"; ?>
          <?php foreach ($option['option_value'] as $option_value) { ?>
          <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]"  points="<?php echo $option_value['points_value']; ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php echo $option_value['price_value']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" <?php echo $opt_checked; $opt_checked=""; ?> onchange="recalculateprice();" />
          <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            
        
          <?php
          if ($option_value['price_prefix'] == '*') {
            if ($option_value['price_value'] != 1.0)
              printf("(%+d%%)", round(($option_value['price_value'] * 100) - 100) );
          } else {
            echo "(".$option_value['price_prefix'].$option_value['price'].")"; 
          }
          ?>
        
      
            <?php } ?>
          </label>
          <br />
          <?php } ?>
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'checkbox') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
<?php $opt_checked="checked"; ?>
          <?php foreach ($option['option_value'] as $option_value) { ?>
          <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]"  points="<?php echo $option_value['points_value']; ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php printf("%.5f", $option_value['price_value']); ?>" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" onchange="recalculateprice();" />
          <label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            
        
          <?php
          if ($option_value['price_prefix'] == '*') {
            if ($option_value['price_value'] != 1.0)
              printf("(%+d%%)", round(($option_value['price_value'] * 100) - 100) );
          } else {
            echo "(".$option_value['price_prefix'].$option_value['price'].")"; 
          }
          ?>
        
      
            <?php } ?>
          </label>
          <br />
          <?php } ?>
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'image') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <table class="option-image">
<?php $opt_checked="checked"; ?>
            <?php foreach ($option['option_value'] as $option_value) { ?>
            <tr>
              <td style="width: 1px;"><input type="radio" name="option[<?php echo $option['product_option_id']; ?>]"  points="<?php echo $option_value['points_value']; ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php echo $option_value['price_value']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" id="option-value-<?php echo $option_value['product_option_value_id']; ?>" <?php echo $opt_checked; $opt_checked=""; ?> onchange="recalculateprice();" /></td>
              <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" /></label></td>
              <td><label for="option-value-<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                  <?php if ($option_value['price']) { ?>
                  
        
          <?php
          if ($option_value['price_prefix'] == '*') {
            if ($option_value['price_value'] != 1.0)
              printf("(%+d%%)", round(($option_value['price_value'] * 100) - 100) );
          } else {
            echo "(".$option_value['price_prefix'].$option_value['price'].")"; 
          }
          ?>
        
      
                  <?php } ?>
                </label></td>
            </tr>
            <?php } ?>
          </table>
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'text') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" />
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'textarea') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <textarea name="option[<?php echo $option['product_option_id']; ?>]" cols="40" rows="5"><?php echo $option['option_value']; ?></textarea>
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'file') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <input type="button" value="<?php echo $button_upload; ?>" id="button-option-<?php echo $option['product_option_id']; ?>" class="button">
          <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" />
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'date') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="date" />
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'datetime') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="datetime" />
        </div>
        <br />
        <?php } ?>
        <?php if ($option['type'] == 'time') { ?>
        <div id="option-<?php echo $option['product_option_id']; ?>" class="option">
          <?php if ($option['required']) { ?>
          <span class="required">*</span>
          <?php } ?>
          <b><?php echo $option['name']; ?>:</b><br />
          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['option_value']; ?>" class="time" />
        </div>
        <br />
        <?php } ?>
        <?php } ?>
      </div>
      <?php } ?>
 </div>
	<div class="right">
	<!-- Выводим атрибут -->
  <?php if ($attribute_groups) { ?>
    <div class="attr">
		<?php foreach ($attribute_groups as $attribute_group) { ?>
		  <?php if ($attribute_group['name'] == 'Краткие характеристики') { ?>
			<?php foreach ($attribute_group['attribute'] as $attribute) { ?>
			  <div><b><?php echo $attribute['name']; ?>:</b> 
			  <span><?php echo html_entity_decode($attribute['text']); ?></span></div>
			<?php } ?>
		  <?php } ?>
		<?php } ?>
		<br/><a onclick="$('a[href=\'#tab-attribute\']').trigger('click');" href="<?php echo $_SERVER["REQUEST_URI"]; ?>#tab-attribute">Все характеристики >></a>
	</div>

  <?php } ?>
  <!-- Выводим атрибут Конец -->
  
<div class="inform">
	<ul>
		<b class="payment">Оплата</b>
			<li>Наложенный платеж (коммисия 2% + 20 грн)</li>
			<li>На карточный счет (коммисия 0,5%)</li>
			<li>Webmoney (коммисия 4%)</li>
			<li>Наличными в офисе</li>
	</ul>
	<ul>
		<b class="delivery">Доставка</b>
			<li>Новая почта (бесплатно*)<br/><i style="font-size:12px;">*Если сумма заказа превышает 800 грн.</i></li>
			<li>Самовывоз (на офисе магазина)</li>
	</ul>
	<ul>
		<b class="garantee">Гарантия</b>
			<li>12 месяцев от производителя</li>
			<li>от 1 до 12 месяцев от сервиса C-Trade</li>
			<li>14 дней для обмена/возврта</li>
	</ul>
</div>  	  
	</div>
	
  </div>
  <div id="tabs" class="htabs"><a href="#tab-description"><?php echo $tab_description; ?></a>
    <?php if ($attribute_groups) { ?>
    <a href="#tab-attribute"><?php echo $tab_attribute; ?></a>
    <?php } ?>
	
	<?php if ($question_status) { ?>
		<a href="#tab-question"><?php echo $tab_question; ?></a>
		<?php } ?>
	
    <?php if ($review_status) { ?>
    <a href="#tab-review"><?php echo $tab_review; ?></a>
    <?php } ?>
  </div>
  <div itemprop="description" id="tab-description" class="tab-content"><img src="http://c-trade.com.ua/transparent.png" class="transparent"><?php echo $description; ?></div>
  
  <?php if ($question_status) { ?>
		  <div id="tab-question" class="tab-content">
			<div id="question"></div>
			<h2 id="question-title"><?php echo $text_write_question; ?></h2>
			<?php if ($guest_question) { ?>
			<span class="required">*</span> <b><?php echo $entry_questioner; ?></b><br />
			<input type="text" name="questioner" value="<?php echo $customer_name; ?>" size="50" />
			<br />
			<br />
			<span style="display: none;">
			<input type="text" name="email" value="<?php echo $customer_email; ?>" size="50" />
			<input type="hidden" name="product" value="<?php echo $product_question; ?>" />
			</span>
			<span style="display:none;"><input type="text" name="question_category" value="1" size="50" /></span>
			<span class="required">*</span> <b><?php echo $entry_text; ?></b>
			<textarea name="text_question"  class="text-question"></textarea>
			<br />
			<br />
			<?php if ($captcha_product_display) { ?>
			  <span class="required">*</span> <b><?php echo $entry_captcha; ?></b><br />
			  <input type="text" name="q_captcha" value="" />
			  <br />
			  <img src="index.php?route=product/product/q_captcha" alt="" id="captcha" />
			  <br />
			  <br />
			<?php } ?>
			<div class="buttons">
			  <div class="right"><a id="button-question" class="button"><?php echo $button_continue; ?></a></div>
			</div>
			<?php } else { ?>
			  <?php echo $text_login_question; ?>
			<?php } ?>
		  </div>
		<?php } ?>
  
  <?php if ($attribute_groups) { ?>
  <div id="tab-attribute" class="tab-content">
    <table class="attribute">
      <?php foreach ($attribute_groups as $attribute_group) { ?>
	  <?php if ($attribute_group['name'] !== 'Краткие характеристики') { ?>
      <thead>
        <tr>
          <td colspan="2"><?php echo $attribute_group['name']; ?></td>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
        <tr>
          <td><?php echo $attribute['name']; ?></td>
          <td><?php echo $attribute['text']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
      <?php } ?><?php } ?>
    </table>
  </div>
  <?php } ?>
  <?php if ($review_status) { ?>
  <div id="tab-review" class="tab-content">
    <div id="review"></div>
    <h2 id="review-title"><?php echo $text_write; ?></h2>
    <b><?php echo $entry_name; ?></b><br />
    <input type="text" name="name" value="" />
    <br />
    <br />
    <b><?php echo $entry_review; ?></b>
    <textarea name="text" cols="40" rows="8" style="width: 98%;"></textarea>
    <span style="font-size: 11px;"><?php echo $text_note; ?></span><br />
    <br />
    <b><?php echo $entry_rating; ?></b> <span><?php echo $entry_bad; ?></span>&nbsp;
    <input type="radio" name="rating" value="1" />
    &nbsp;
    <input type="radio" name="rating" value="2" />
    &nbsp;
    <input type="radio" name="rating" value="3" />
    &nbsp;
    <input type="radio" name="rating" value="4" />
    &nbsp;
    <input type="radio" name="rating" value="5" />
    &nbsp;<span><?php echo $entry_good; ?></span><br />
    <br />
    <b><?php echo $entry_captcha; ?></b><br />
    <input type="text" name="captcha" value="" />
    <br />
    <img src="index.php?route=product/product/captcha" alt="" id="captcha" /><br />
    <br />
    <div class="buttons">
      <div class="right"><a id="button-review" class="button"><?php echo $button_continue; ?></a></div>
    </div>
  </div>
  <?php } ?>
  <?php if ($products) { ?>
  <div class="related-products">
  <div class="box"><div class="box-heading">Похожие товары</div></div>
    <div class="product-grid">
      <?php foreach ($products as $product) { ?>
      <div><div>
		<a href="<?php echo $product['href']; ?>">
<!-- star ipl --><?php echo $product['labels_ipl']; ?><!-- end ipl -->
		<div class="name"><?php echo $product['name']; ?></div>
        <?php if ($product['thumb']) { ?>
        <div class="image"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></div>
        <?php } ?></a>
        <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
        <div class="reviews-count"><?php echo $product['reviews']; ?></div>
		<div style="clear:both;"></div>
		<?php if ($product['price']) { ?>
        <div class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
          <?php } ?>
        </div>
		
        <?php } ?>
		<div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');"><i><?php echo $button_compare; ?></i></a></div>
		<div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');"><i><?php echo $button_wishlist; ?></i></a></div>
      <div style="clear:both;"></div>	
		<!-- Выводим атрибут -->
  <?php if($product['attribute_groups']) { ?>
  <div class="description">
		<?php foreach ($product['attribute_groups'] as $attribute_group) { ?>
		  <?php if ($attribute_group['name'] == 'Краткие характеристики') { ?>
			<?php foreach ($attribute_group['attribute'] as $attribute) { ?>
			  <b><?php echo $attribute['name']; ?>:</b> 
			  <span><?php echo html_entity_decode($attribute['text']); ?></span>
			<?php } ?>
		  <?php } ?>
		<?php } ?>
</div>
<?php } ?>
  <!-- Выводим атрибут Конец -->
    </div></div>
	<?php } ?>
  </div></div>
  <?php } ?>
  <?php if ($tags) { ?>
  <div class="tags"><b><?php echo $text_tags; ?></b>
    <?php for ($i = 0; $i < count($tags); $i++) { ?>
    <?php if ($i < (count($tags) - 1)) { ?>
    <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
    <?php } else { ?>
    <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
    <?php } ?>
    <?php } ?>
  </div>
  <?php } ?>
  <?php echo $content_bottom; ?></div>
<!-- star ipl -->

			<script type="text/javascript"><!--

				$(window).load(function(){
					$('.product-info .image').prepend('<?php echo $labels_ipl; ?>');
				});

			//--></script>

			<!-- end ipl -->
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.colorbox').colorbox({
		overlayClose: true,
		opacity: 0.5,
		rel: "colorbox"
	});
});
//--></script> 
<script type="text/javascript"><!--
$('#button-cart').bind('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, information, .error').remove();
			
			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						$('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
					}
				}
			} 
			
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
				$('.success').fadeIn('slow');
								
				setTimeout(
					function() {
						location.reload();
					}, 2000);
			}	
		}
	});
});
//--></script>
<?php if ($options) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
<?php foreach ($options as $option) { ?>
<?php if ($option['type'] == 'file') { ?>
<script type="text/javascript"><!--
new AjaxUpload('#button-option-<?php echo $option['product_option_id']; ?>', {
	action: 'index.php?route=product/product/upload',
	name: 'file',
	autoSubmit: true,
	responseType: 'json',
	onSubmit: function(file, extension) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', true);
	},
	onComplete: function(file, json) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', false);
		
		$('.error').remove();
		
		if (json['success']) {
			alert(json['success']);
			
			$('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json['file']);
		}
		
		if (json['error']) {
			$('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json['error'] + '</span>');
		}
		
		$('.loading').remove();	
	}
});
//--></script>
<?php } ?>
<?php } ?>
<?php } ?>
<script type="text/javascript"><!--
$('#review .pagination a').live('click', function() {
	$('#review').fadeOut('slow');
		
	$('#review').load(this.href);
	
	$('#review').fadeIn('slow');
	
	return false;
});			

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').bind('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-review').attr('disabled', true);
			$('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-review').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data['error']) {
				$('#review-title').after('<div class="warning">' + data['error'] + '</div>');
			}
			
			if (data['success']) {
				$('#review-title').after('<div class="success">' + data['success'] + '</div>');
								
				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').attr('checked', '');
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script> 
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
	if ($.browser.msie && $.browser.version == 6) {
		$('.date, .datetime, .time').bgIframe();
	}

	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
	$('.datetime').datetimepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: 'h:m'
	});
	$('.time').timepicker({timeFormat: 'h:m'});
});
//--></script> 
<script type="text/javascript" src="admin/view/javascript/ckeditor/ckeditor.js"></script> 
		<script type="text/javascript"><!--
		CKEDITOR.replaceClass = 'text-question';
		//--></script>
		<script type="text/javascript"><!--
		$('#question .pagination a').live('click', function() {
			$('#question').fadeOut('slow');
				
			$('#question').load(this.href);
			
			$('#question').fadeIn('slow');
			
			return false;
		});			

		$('#question').load('index.php?route=product/product/question&product_id=<?php echo $product_id; ?>');

		$('#button-question').bind('click', function() {
			for ( inst in CKEDITOR.instances ){
				CKEDITOR.instances[inst].updateElement();
			}
			$.ajax({

				url: 'index.php?route=product/product/submit_question&product_id=<?php echo $product_id; ?>',
				type: 'post',
				dataType: 'json',
				<?php if (($captcha_product == 'all') || ((!$this->customer->isLogged()) && ($captcha_product == 'guest')) || (($this->customer->isLogged()) && ($captcha_product == 'customer'))) { ?>
					data: 'questioner=' + encodeURIComponent($('input[name=\'questioner\']').val()) + '&email=' + encodeURIComponent($('input[name=\'email\']').val()) + '&product=' + encodeURIComponent($('input[name=\'product\']').val()) + '&question_category=' + encodeURIComponent($('input[name=\'question_category\']').val()) + '&text_question=' + encodeURIComponent($('textarea[name=\'text_question\']').val()) + '&q_captcha=' + encodeURIComponent($('input[name=\'q_captcha\']').val()),
				<?php } elseif (($captcha_product == 'no') || (($this->customer->isLogged()) && ($captcha_product == 'guest')) || ((!$this->customer->isLogged()) && ($captcha_product == 'customer'))) { ?>
					data: 'questioner=' + encodeURIComponent($('input[name=\'questioner\']').val()) + '&email=' + encodeURIComponent($('input[name=\'email\']').val()) + '&product=' + encodeURIComponent($('input[name=\'product\']').val()) + '&question_category=' + encodeURIComponent($('input[name=\'question_category\']').val()) + '&text_question=' + encodeURIComponent($('textarea[name=\'text_question\']').val()),
				<?php } ?>
				beforeSend: function() {
					$('.success, .warning').remove();
					$('#button-question').attr('disabled', true);
					$('#question-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
				},
				complete: function() {
					$('#button-question').attr('disabled', false);
					$('.attention').remove();
				},
				success: function(data) {
					if (data['error']) {
						$('#question-title').after('<div class="warning">' + data['error'] + '</div>');
					}
					
					if (data['success']) {
						$('#question-title').after('<div class="success">' + data['success'] + '</div>');
										
						$('input[name=\'questioner\']').val('');
						$('input[name=\'question_category\']').val('');
						$('input[name=\'email\']').val('');
						$('input[name=\'product\']').val('');
						$('textarea[name=\'text_questioner\']').val('');
						<?php if (($captcha_product == 'all') || ((!$this->customer->isLogged()) && ($captcha_product == 'guest')) || (($this->customer->isLogged()) && ($captcha_product == 'customer'))) { ?>
							$('input[name=\'q_captcha\']').val('');
						<?php } ?>
					}
				}
			});
		});
		//--></script>

        
<script type="text/javascript"><!--

function price_format(n)
{ 
    c = <?php echo (empty($currency['decimals']) ? "0" : $currency['decimals'] ); ?>;
    d = '<?php echo $currency['decimal_point']; ?>'; // decimal separator
    t = '<?php echo $currency['thousand_point']; ?>'; // thousands separator
    s_left = '<?php echo $currency['symbol_left']; ?>';
    s_right = '<?php echo $currency['symbol_right']; ?>';
      
    n = n * <?php echo $currency['value']; ?>;

    //sign = (n < 0) ? '-' : '';

    //extracting the absolute value of the integer part of the number and converting to string
    i = parseInt(n = Math.abs(n).toFixed(c)) + ''; 

    j = ((j = i.length) > 3) ? j % 3 : 0; 
    return s_left + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '') + s_right; 
}

function calculate_tax(price)
{
    <?php // Process Tax Rates
      if (isset($tax_rates) && $tax) {
         foreach ($tax_rates as $tax_rate) {
           if ($tax_rate['type'] == 'F') {
             echo 'price += '.$tax_rate['rate'].';';
           } elseif ($tax_rate['type'] == 'P') {
             echo 'price += (price * '.$tax_rate['rate'].') / 100.0;';
           }
         }
      }
    ?>
    return price;
}

function process_discounts(price, quantity)
{
    <?php
      foreach ($dicounts_unf as $discount) {
        echo 'if ((quantity >= '.$discount['quantity'].') && ('.$discount['price'].' < price)) price = '.$discount['price'].';'."\n";
      }
    ?>
    return price;
}


animate_delay = 20;

main_price_final = calculate_tax(Number($('#formated_price').attr('price')));
main_price_start = calculate_tax(Number($('#formated_price').attr('price')));
main_step = 0;
main_timeout_id = 0;

function animateMainPrice_callback() {
    main_price_start += main_step;
    
    if ((main_step > 0) && (main_price_start > main_price_final)){
        main_price_start = main_price_final;
    } else if ((main_step < 0) && (main_price_start < main_price_final)) {
        main_price_start = main_price_final;
    } else if (main_step == 0) {
        main_price_start = main_price_final;
    }
    
    $('#formated_price').html( price_format(main_price_start) );
    
    if (main_price_start != main_price_final) {
        main_timeout_id = setTimeout(animateMainPrice_callback, animate_delay);
    }
}

function animateMainPrice(price) {
    main_price_start = main_price_final;
    main_price_final = price;
    main_step = (main_price_final - main_price_start) / 10;
    
    clearTimeout(main_timeout_id);
    main_timeout_id = setTimeout(animateMainPrice_callback, animate_delay);
}


<?php if ($special) { ?>
special_price_final = calculate_tax(Number($('#formated_special').attr('price')));
special_price_start = calculate_tax(Number($('#formated_special').attr('price')));
special_step = 0;
special_timeout_id = 0;

function animateSpecialPrice_callback() {
    special_price_start += special_step;
    
    if ((special_step > 0) && (special_price_start > special_price_final)){
        special_price_start = special_price_final;
    } else if ((special_step < 0) && (special_price_start < special_price_final)) {
        special_price_start = special_price_final;
    } else if (special_step == 0) {
        special_price_start = special_price_final;
    }
    
    $('#formated_special').html( price_format(special_price_start) );
    
    if (special_price_start != special_price_final) {
        special_timeout_id = setTimeout(animateSpecialPrice_callback, animate_delay);
    }
}

function animateSpecialPrice(price) {
    special_price_start = special_price_final;
    special_price_final = price;
    special_step = (special_price_final - special_price_start) / 10;
    
    clearTimeout(special_timeout_id);
    special_timeout_id = setTimeout(animateSpecialPrice_callback, animate_delay);
}
<?php } ?>


function recalculateprice()
{
    var main_price = Number($('#formated_price').attr('price'));
    var input_quantity = Number($('input[name="quantity"]').attr('value'));
    var special = Number($('#formated_special').attr('price'));
    var tax = 0;
    
    if (isNaN(input_quantity)) input_quantity = 0;
    
    // Process Discounts.
    <?php if ($special) { ?>
        special = process_discounts(special, input_quantity);
    <?php } else { ?>
        main_price = process_discounts(main_price, input_quantity);
    <?php } ?>
    tax = process_discounts(tax, input_quantity);
    
    
   <?php if ($points) { ?>
     var points = Number($('#formated_points').attr('points'));
     $('#product_options input:checked').each(function() {
       points += Number($(this).attr('points'));
     });
     $('#product_options option:selected').each(function() {
       points += Number($(this).attr('points'));
     });
     $('#formated_points').html(points);
   <?php } ?>
    
    var option_price = 0;
    
    $('#product_options input:checked,option:selected').each(function() {
      if ($(this).attr('price_prefix') == '=') {
        option_price += Number($(this).attr('price'));
        main_price = 0;
        special = 0;
      }
    });
    
    $('#product_options input:checked,option:selected').each(function() {
      if ($(this).attr('price_prefix') == '+') {
        option_price += Number($(this).attr('price'));
      }
      if ($(this).attr('price_prefix') == '-') {
        option_price -= Number($(this).attr('price'));
        special -= Number($(this).attr('price'));
        tax -= Number($(this).attr('price'));
      }
      if ($(this).attr('price_prefix') == '*') {
        option_price *= Number($(this).attr('price'));
        main_price *= Number($(this).attr('price'));
        special *= Number($(this).attr('price'));
      }
    });
    
    special += option_price;
    main_price += option_price;

    <?php if ($special) { ?>
      tax = special;
    <?php } else { ?>
      tax = main_price;
    <?php } ?>
    
    // Process TAX.
    main_price = calculate_tax(main_price);
    special = calculate_tax(special);
    
    // Раскомментировать, если нужен вывод цены с умножением на количество
    //main_price *= input_quantity;
    //special *= input_quantity;
    //tax *= input_quantity;

    // Display Main Price
    //$('#formated_price').html( price_format(main_price) );
    animateMainPrice(main_price);
      
    <?php if ($special) { ?>
      //$('#formated_special').html( price_format(special) );
      animateSpecialPrice(special);
    <?php } ?>

    <?php if ($tax) { ?>
      $('#formated_tax').html( price_format(tax) );
    <?php } ?>
}

$(document).ready(function() {
  $('input[name="quantity"]').bind('input', function() { recalculateprice(); });
  recalculateprice();
});

//--></script>
        
      
<?php echo $footer; ?>