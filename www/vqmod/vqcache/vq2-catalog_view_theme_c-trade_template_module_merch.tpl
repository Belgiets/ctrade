
<div class="box">
<?php if($visible_header == 1) { ?>
  <div class="box-heading"><?php echo $heading ?></div>
<?php } ?> 
      <?php if(count($tabs) > 0) { ?>
      	<div id="tabs" class="htabs">
      		<?php $t_count=0; foreach($tabs as $tab) { ?>
      			<a href="#merch-tab-<?php echo $t_count ?>"><?php echo $tab['name'] ?></a>
      		<?php $t_count++; } ?>
      	</div>
      <?php } else { ?>
	  <div class="tab-content">
	    <div class="product-grid">
		<?php } ?>
      <?php $is_tab = false; $t_count=0; foreach ($products as $product) { ?>

	  <?php if(!isset($product['product_id'])) { ?>
	  	<?php if($is_tab === false) { ?>
	  		<div id="merch-tab-<?php echo $t_count ?>" class="tab-content"><div class="product-grid">
	  	<?php } else { ?>
	  		</div></div>
	  		<div id="merch-tab-<?php echo $t_count ?>" class="tab-content"><div class="product-grid">
	  	<?php $is_tab = false; } ?>
	  <?php 
	  	$t_count++; $is_tab = true; 
	  } else { ?>

      <div><div>
		<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>

			<!-- star ipl --><?php echo $product['labels_ipl']; ?><!-- end ipl -->
			
        <?php if ($product['thumb']) { ?>
        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>

			<!-- star ipl --><?php echo $product['labels_ipl']; ?><!-- end ipl -->
			
        <?php } ?>
        <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
        <div class="reviews-count"><?php echo $product['reviews']; ?></div>
		<div style="clear:both;"></div>
		<?php if ($product['price']) { ?>
        <div class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
          <?php } ?>
        </div>
		
        <?php } ?>
		<div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');"><i><?php echo $button_compare; ?></i></a></div>
		<div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');"><i><?php echo $button_wishlist; ?></i></a></div>
      <div style="clear:both;"></div>	
		<!-- Выводим атрибут -->
  <?php if($product['attribute_groups']) { ?>
  <div class="description">
		<?php foreach ($product['attribute_groups'] as $attribute_group) { ?>
		  <?php if ($attribute_group['name'] == 'Краткие характеристики') { ?>
			<?php foreach ($attribute_group['attribute'] as $attribute) { ?>
			  <b><?php echo $attribute['name']; ?>:</b> 
			  <span><?php echo html_entity_decode($attribute['text']); ?></span>
			<?php } ?>
		  <?php } ?>
		<?php } ?>
</div>
<?php } ?>
  <!-- Выводим атрибут Конец -->
    </div></div>
      
      <?php } ?>
      <?php } 
      if(count($tabs) > 0) {
      ?>
      </div></div>
      <?php } ?>
    </div>
<?php      if(count($tabs) == 0) { ?>
  </div>
</div>
<?php } ?>
<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script>