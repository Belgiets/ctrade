<?php
// Text
$_['text_faq'] 						= 'Вопросы (FAQ)';
$_['text_add_success_question']    	= 'Ваш вопрос поступил администратору. Вскоре вы получите на него ответ.';
$_['text_question_success'] 		= 'Ваш вопрос по данному товару поступил администратору. Вскоре вы получите на него ответ.'; 
$_['heading_title'] 				= 'Часто задаваемые вопросы (FAQ)';
$_['text_question_id']     			= '№ вопроса: ';
$_['text_questioner'] 				= 'Вопрос задал(а): ';
$_['text_question_category']  		= 'Категория вопросов: ';
$_['text_status']     				= 'Статус:';
$_['text_answered']     			= 'Закрыт';
$_['text_unanswered']    			= 'Открыт';
$_['text_subject'] 					= 'Тема: ';
$_['text_question_product']     	= 'Вопрос связан с товарами';
$_['text_question_no_product']  	= 'Вопрос не связан с товарами';
$_['text_admin_answer']  			= 'Ответ администратора';
$_['text_empty']  					= 'Вопросов нет.';
$_['text_no_questions']  			= 'Вопросов по данному товару нет.';
$_['text_add_question'] 			= 'Новый вопрос';
$_['text_write_question']  			= 'Задать вопрос по данному товару'; 
$_['text_login_question']  			= 'Чтобы задать вопрос по товару вы должны <a href="%s">авторизироваться</a> или <a href="%s">зарегистрироваться</a>.';

// Entry
$_['entry_questioner']  			= 'Ваши ФИО:'; 
$_['entry_email']  					= 'Ваш E-Mail:<br /><span class="help">Укажите для получения уведомления об ответе.</span>';
$_['entry_category']  				= 'Категория вопроса:';
$_['entry_product']  				= 'Товар:<br /><span class="help">(Для автозаполнения введите знак %)</span>';
$_['entry_subject']  				= 'Тема:';
$_['entry_text']  					= 'Ваш вопрос:';
$_['entry_captcha']     			= 'Введите код с картинки:';

// Column
$_['column_question_id'] 			= '№ вопроса';
$_['column_questioner'] 			= 'Автор вопроса';
$_['column_question_category']  	= 'Категория вопросов';
$_['column_question_product']  		= 'Товар';
$_['column_status']     			= 'Статус';
$_['column_date_added'] 			= 'Дата добавления';
$_['column_action']     			= 'Фильтр';

// Button
$_['button_add_question']     		= 'Задать новый вопрос';
$_['button_filter']     			= 'Фильтр';

// Tabs
$_['tab_question']      			= 'Вопросы (%s)';

// Errors
$_['error_questioner']      		= 'ФИО должны быть от 3 до 64 символов!';
$_['error_email']      				= 'Укажите E-Mail!';
$_['error_product']         		= 'Необходимо выбрать товар!';
$_['error_subject']         		= 'Тема вопроса должна быть от 3 до 64 символов!';
$_['error_text_question']   		= 'Текст вопроса должен быть от 10 до 5000 символов!';
$_['error_q_captcha'] 				= 'Код с картинки, введен неверно!';
$_['error_captcha']     			= 'Код с картинки, введен неверно!';
?>