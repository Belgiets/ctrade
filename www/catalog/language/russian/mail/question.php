<?php
// Text
$_['text_subject']				= '%s - Новый вопрос';
$_['text_waiting']				= 'Вам поступил новый вопрос.<br />';
$_['text_category']				= 'Категория вопросов: %s<br />';
$_['text_product']				= 'Товар: <b>%s</b><br />';
$_['text_question_subject']		= 'Тема вопроса: <b>%s</b><br />';
$_['text_questioner']			= 'Вопрос задал(а): <b>%s</b><br />';
$_['text_question']				= '<b>Текст вопроса</b>:<br />';
$_['text_question_product']		= '<b>Вопрос связан с товарами</b>';
$_['text_question_no_product'] 	= '<b>Вопрос не связан с товарами</b>';

// Delete
$_['text_subject_delete']		= '%s - Удаление вопроса';
$_['text_waiting_delete']		= 'Покупатель удалил свой вопрос.';
?>