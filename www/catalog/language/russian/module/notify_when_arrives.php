<?php

$_['heading_title'] = 'Out of stock!';

#text---------------------------------------------------------------------------

$_['text_description']  = 'Введите ваш email, чтобы быть в курсе, когда этот товар появится в наличии:';

$_['text_description_2']  = 'Заполните эту форму, и мы уведомим Вас, когда этот товар появится в наличии:';

$_['text_success'] =  'E-mail Зарегистрирован!<br/>вы будете проинформированы, когда этот товар появится в наличии. Спасибо!';

$_['mail_admin_subject'] =  'Уведомить о наличии - {product_name}';

$_['mail_admin_body'] =  '
<b>Детали Запроса</b>
<br/><br/>
Имя заказчика: {customer_name}
<br/><br/>
E-mail заказчика : {customer_email}
<br/><br/>
Телефон : {customer_phone}
<br/><br/>
{custom_field} {customer_custom}
<br/><br/>
Товар: {product_name}
';

$_['text_error_mail']  = 'Этот e-mail не действителен!';
$_['text_error_name']  = 'Это имя не является допустимым!';
$_['text_error_phone']  = 'Этот телефон не действителен!';
$_['text_error_custom']  = '{custom_name} данные не действительны!';

$_['text_error_data'] = 'Неверные данные формы.';
#entry
$_['nwa_entry_name'] = 'Имя';
$_['nwa_entry_phone'] = 'Телефон';
$_['nwa_entry_mail'] = 'E-mail';
#buttons---------------------------------------------------------------------------

$_['button_register']  = 'Отправить';
$_['button_category'] = 'Уведомлять меня!';
$_['button_close'] = 'Закрыть';

?>