<?php
// Heading 
$_['heading_title'] 		= 'Успей купить';
$_['header_hit_tovar']    	= 'Товар дня';

$_['ostalos']        			= 'Уже продано';
$_['schetcik_rasprod']        	= 'До конца акции осталось:';
$_['schetcik_end_rasprod']      = 'Акция закончилась';
$_['count_rasprod']        		= 'Штук';

$_['days']        				= 'Дней';
$_['hours']        				= 'Час';
$_['minutes']        			= 'Мин';
$_['sec']        				= 'Сек';

$_['quick']        				= 'Быстрый просмотр';

// Text
$_['text_reviews']  		= 'На основании %s отзывов.'; 
?>