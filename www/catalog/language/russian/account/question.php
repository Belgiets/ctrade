<?php
// Heading 
$_['heading_title']         		= 'История вопросов';

// Text
$_['text_account']          		= 'Личный Кабинет';
$_['text_add_success_question']    	= 'Ваш вопрос поступил администратору. Вскоре вы получите на него ответ.';
$_['text_success_delete_question']	= 'Ваш вопрос успешно удален.';
$_['text_question']         		= 'Вопрос';
$_['text_add_question']     		= 'Новый вопрос';
$_['text_status']           		= 'Статус:';
$_['text_question_id']    			= '№ вопроса';
$_['text_date_added']     			= 'Дата вопроса';
$_['text_date_modified']  			= 'Дата ответа';
$_['text_question_type']  			= 'Категория вопросов';
$_['text_question_product'] 		= 'Вопрос связан с товарами';
$_['text_question_no_product'] 		= 'Вопрос не связан с товарами';
$_['text_action'] 					= 'Действие';
$_['text_no_answer'] 				= 'Ответ не поступил';
$_['text_product'] 					= 'Товар:';
$_['text_empty']            		= 'Вы не задавали вопросов!';
$_['text_error']            		= 'Запрошенный вопрос не найден!';
$_['text_answered']         		= 'Закрыт';
$_['text_unanswered']       		= 'Открыт';
$_['text_question_detail']  		= 'Детали вопроса';
$_['text_info_question_delete']  	= 'Администратор запретил вам удаление своих вопросов из данной категории.';
$_['text_my_question']  			= 'Мои вопросы';
$_['text_history_question']  		= 'История вопросов';

// Entry
$_['entry_questioner']  			= 'ФИО:';
$_['entry_category']  				= 'Категория вопроса:';
$_['entry_product']  				= 'Товар:<br /><span class="help">(Для автозаполнения введите знак %)</span>';
$_['entry_subject']  				= 'Тема:';
$_['entry_text']  					= 'Ваш вопрос:';
$_['entry_captcha']     			= 'Введите код с картинки:';

// Column
$_['column_question_id']    		= '№';
$_['column_date_added']     		= 'Дата вопроса';
$_['column_date_modified']  		= 'Дата ответа';
$_['column_question_type']  		= 'Категория вопросов';
$_['column_status']  				= 'Статус';
$_['column_name']           		= 'Название товара';
$_['column_image']          		= 'Изображение';
$_['column_question']       		= 'Ваш вопрос';
$_['column_answer']       			= 'Ответ администратора';

// Button
$_['button_add_question']   		= 'Задать новый вопрос';

// Errors
$_['error_questioner']      		= 'ФИО должны быть от 3 до 64 символов!';
$_['error_product']         		= 'Необходимо выбрать товар!';
$_['error_subject']         		= 'Тема вопроса должна быть от 3 до 64 символов!';
$_['error_text_question']   		= 'Текст вопроса должен быть от 10 до 5000 символов!';
$_['error_captcha']     			= 'Код с картинки, введен неверно!';
?>
