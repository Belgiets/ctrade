<?php
// Text
$_['text_home']           = 'Главная';
$_['text_wishlist']       = 'Закладки (%s)';
$_['text_shopping_cart']  = 'Корзина покупок';
$_['text_search']         = 'Поиск';
$_['text_welcome']        = '<noindex><a rel="nofollow" href="%s">Войти</a> или <noindex><a rel="nofollow" href="%s">зарегистрироваться</a><noindex>';
$_['text_logged']         = 'Вы вошли как <noindex><a rel="nofollow" href="%s">%s</a> <b>(</b> <a href="%s">Выйти</a> <b>)</b><noindex>';
$_['text_account']        = 'Постоянный покупатель';
$_['text_checkout']       = 'Оформление заказа';
$_['text_page']           = 'страница';
?>