<?php
// Text
$_['text_faq'] 						= 'Questions (FAQ)';
$_['text_add_success_question']    	= 'Your question came administrator. Soon you will get the answer.';
$_['text_question_success'] 		= 'Your question on this product entered the administrator. Soon you will get the answer.'; 
$_['heading_title'] 				= 'Frequently Asked Questions (FAQ)';
$_['text_question_id']     			= 'Question ID: ';
$_['text_questioner'] 				= 'Asked Question: ';
$_['text_question_category']  		= 'Category Questions: ';
$_['text_status']     				= 'Status:';
$_['text_answered']     			= 'Closed';
$_['text_unanswered']    			= 'Opened';
$_['text_subject'] 					= 'Theme: ';
$_['text_question_product']     	= 'Question associated with the products';
$_['text_question_no_product']  	= 'Question is not associated with the products';
$_['text_admin_answer']  			= 'Answer administrator';
$_['text_empty']  					= 'There are no questions.';
$_['text_no_questions']  			= 'Questions on this product no.';
$_['text_add_question'] 			= 'New Question';
$_['text_write_question']  			= 'Ask a question on this product'; 
$_['text_login_question']  			= 'To ask a question on the product you must be <a href="%s">logged</a> in or <a href="%s">register</a>.';

// Entry
$_['entry_questioner']  			= 'First Name, Last Name:'; 
$_['entry_email']  					= 'Your E-Mail:<br /><span class="help">Specify whether the receipt of the notification about the answer.</span>';
$_['entry_category']  				= 'Category Questions:';
$_['entry_product']  				= 'Product:<br/><span class="help">(Autocomplete %)</span>';
$_['entry_subject']  				= 'Theme:';
$_['entry_text']  					= 'Your question:';
$_['entry_captcha']     			= 'Captcha:';

// Column
$_['column_question_id'] 			= 'Question ID';
$_['column_questioner'] 			= 'Author';
$_['column_question_category']  	= 'Category Questions';
$_['column_question_product']  		= 'Product';
$_['column_status']     			= 'Status';
$_['column_date_added'] 			= 'Date added';
$_['column_action']     			= 'Filter';

// Button
$_['button_add_question']     		= 'Ask New Question';
$_['button_filter']     			= 'Filter';

// Tabs
$_['tab_question']      			= 'Questions (%s)';

// Errors
$_['error_questioner']      		= 'First Name, Last Name should be from 3 up to 64 characters!';
$_['error_email']      				= 'E-Mail Address does not appear to be valid!';
$_['error_product']         		= 'Should choose the product!';
$_['error_subject']         		= 'Theme question should be from 3 up to 64 characters!';
$_['error_text_question']   		= 'Question text should be from 10 up to 5000 characters!';
$_['error_q_captcha'] 				= 'Picture code incorrect!';
$_['error_captcha']     			= 'Picture code incorrect!';
?>