<?php
// Heading 
$_['heading_title']         		= 'History Questions';

// Text
$_['text_account']          		= 'Account';
$_['text_add_success_question']    	= 'Your question came administrator. Soon you will get the answer.';
$_['text_success_delete_question']	= 'Your question has been deleted.';
$_['text_question']         		= 'Question';
$_['text_add_question']     		= 'New Question';
$_['text_status']           		= 'Status:';
$_['text_question_id']    			= 'Question ID';
$_['text_date_added']     			= 'Date added';
$_['text_date_modified']  			= 'Date of answer';
$_['text_question_type']  			= 'Category Questions';
$_['text_question_product'] 		= 'Question associated with the products';
$_['text_question_no_product'] 		= 'Question is not associated with the products';
$_['text_action'] 					= 'Action';
$_['text_no_answer'] 				= 'Do not get a response';
$_['text_product'] 					= 'Product:';
$_['text_empty']            		= 'You not asked questions!';
$_['text_error']            		= 'Requested the question is not found!';
$_['text_answered']         		= 'Closed';
$_['text_unanswered']       		= 'Opened';
$_['text_question_detail']  		= 'Details';
$_['text_info_question_delete']  	= 'The administrator has forbidden you to remove your questions from this category.';
$_['text_my_question']  			= 'My Questions';
$_['text_history_question']  		= 'History questions';

// Entry
$_['entry_questioner']  			= 'First Name, Last Name:';
$_['entry_category']  				= 'Category Questions:';
$_['entry_product']  				= 'Product:<br/><span class="help">(Autocomplete %)</span>';
$_['entry_subject']  				= 'Theme:';
$_['entry_text']  					= 'Your question:';
$_['entry_captcha']     			= 'Type the code from picture:';

// Column
$_['column_question_id']    		= 'ID';
$_['column_date_added']     		= 'Date added';
$_['column_date_modified']  		= 'Date of answer';
$_['column_question_type']  		= 'Category Questions';
$_['column_status']  				= 'Status';
$_['column_name']           		= 'Product name';
$_['column_image']          		= 'Image';
$_['column_question']       		= 'Your question';
$_['column_answer']       			= 'Answer administrator';

// Button
$_['button_add_question']   		= 'Ask New Question';

// Errors
$_['error_questioner']      		= 'First Name, Last Name should be from 3 up to 64 characters!';
$_['error_product']         		= 'Should choose the product!';
$_['error_subject']         		= 'Theme question should be from 3 up to 64 characters!';
$_['error_text_question']   		= 'Question text should be from 10 up to 5000 characters!';
$_['error_captcha']     			= 'Picture code incorrect!';
?>
