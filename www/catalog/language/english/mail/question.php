<?php
// Text
$_['text_subject']				= '%s - New Question';
$_['text_waiting']				= 'You entered a new question.<br />';
$_['text_category']				= 'Category: %s<br />';
$_['text_product']				= 'Product: <b>%s</b><br />';
$_['text_question_subject']		= 'Subject: <b>%s</b><br />';
$_['text_questioner']			= 'Author: <b>%s</b><br />';
$_['text_question']				= '<b>Question Qext</b>:<br />';
$_['text_question_product']		= '<b>Question associated with the products</b>';
$_['text_question_no_product'] 	= '<b>Question is not associated with the products</b>';

// Delete
$_['text_subject_delete']		= '%s - Удаление Вопросов';
$_['text_waiting_delete']		= 'The buyer has deleted his question.';
?>