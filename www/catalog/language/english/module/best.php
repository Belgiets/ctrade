<?php
// Heading 
$_['heading_title'] 		= 'Uspey buy';
$_['header_hit_tovar']    	= 'Product of the day';

$_['ostalos']        			= 'already sold';
$_['schetcik_rasprod']        	= 'Time left to buy:';
$_['schetcik_end_rasprod']      = 'The action ended';
$_['count_rasprod']        		= 'pcs';

$_['days']        				= 'days';
$_['hours']        				= 'hour';
$_['minutes']        			= 'min';
$_['sec']        				= 'sec';

$_['quick']        				= 'Quick View';

// Text
$_['text_reviews']  = 'Based on %s reviews.'; 
?>