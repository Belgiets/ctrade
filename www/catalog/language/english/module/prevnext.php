<?php
/* PprMkr module PrevNext

       |-|-|
         |   PprMkr
         O
           
 Author:    R.Dijk
 Version:   1.0.0
 Feel free to donate: https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=HC6BRSDD65RQA&lc=NL&item_name=R%2e%20Dijk&item_number=via_pprmkr&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted
*/
// Heading
$_['heading_title'] = 'PrevNext product';
// text
$_['text_volgende'] = 'Next product';
$_['text_vorige'] = 'Previous product';
$_['text_eerste'] = 'First product';
$_['text_laatste'] = 'Last product';
?>