<?php 
class ControllerAccountQuestion extends Controller {
	private $error = array();
		
	public function index() {
		if ($this->config->get('question_installed')) {
			if (!$this->customer->isLogged()) {
				$this->session->data['redirect'] = $this->url->link('account/question', '', 'SSL');

				$this->redirect($this->url->link('account/login', '', 'SSL'));
			}
			
			$this->language->load('account/question');
			
			$this->load->model('catalog/question');

			$this->document->setTitle($this->language->get('heading_title'));

			$this->data['breadcrumbs'] = array();

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home'),        	
				'separator' => false
			); 

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_account'),
				'href'      => $this->url->link('account/account', '', 'SSL'),        	
				'separator' => $this->language->get('text_separator')
			);
			
			$url = '';
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
					
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('account/question', $url, 'SSL'),        	
				'separator' => $this->language->get('text_separator')
			);

			$this->data['heading_title'] = $this->language->get('heading_title');

			$this->data['text_question_id'] = $this->language->get('text_question_id');
			$this->data['text_status'] = $this->language->get('text_status');
			$this->data['text_date_added'] = $this->language->get('text_date_added');
			$this->data['text_date_modified'] = $this->language->get('text_date_modified');
			$this->data['text_customer'] = $this->language->get('text_customer');
			$this->data['text_question_type'] = $this->language->get('text_question_type');
			$this->data['text_question_product'] = $this->language->get('text_question_product');
			$this->data['text_question_no_product'] = $this->language->get('text_question_no_product');
			$this->data['text_no_answer'] = $this->language->get('text_no_answer');
			$this->data['text_action'] = $this->language->get('text_action');
			$this->data['text_total'] = $this->language->get('text_total');
			$this->data['text_empty'] = $this->language->get('text_empty');
			$this->data['text_info_question_delete'] = $this->language->get('text_info_question_delete');

			$this->data['button_view'] = $this->language->get('button_view');
			$this->data['button_add_question'] = $this->language->get('button_add_question');
			$this->data['button_delete'] = $this->language->get('button_delete');
			$this->data['button_back'] = $this->language->get('button_back');
			
			if (isset($this->session->data['success'])) {
				$this->data['success'] = $this->session->data['success'];
			
				unset($this->session->data['success']);
			} else {
				$this->data['success'] = '';
			}
			
			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 1;
			}
			
			$this->data['questions'] = array();
			
			$question_total = $this->model_catalog_question->getTotalQuestionsAllCategories();
			
			$results = $this->model_catalog_question->getQuestionsByCustomerId(($page - 1) * $this->config->get('config_limit_question_account'), $this->config->get('config_limit_question_account'));
			
			foreach ($results as $result) {
				$this->data['questions'][] = array(
					'question_id' 	   	=> $result['question_id'],
					'product_id' 	   	=> $result['product_id'],
					'answer_status'     => $result['status'] && $result['text_answer'],
					'question_category' => $result['question_category'],
					'status'     	   	=> ($result['status'] ? '<span style="color:green;">'.$this->language->get('text_answered').'</span>' : '<span style="color:red;">'.$this->language->get('text_unanswered').'</span>'),
					'date_added' 		=> date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'time_added' 		=> date($this->language->get('time_format'), strtotime($result['date_added'])),
					'date_modified' 	=> date($this->language->get('date_format_short'), strtotime($result['date_modified'])),
					'time_modified' 	=> date($this->language->get('time_format'), strtotime($result['date_modified'])),
					'selected'   		=> isset($this->request->post['selected']) && in_array($result['question_id'], $this->request->post['selected']),
					'href'       		=> $this->url->link('account/question/info', 'question_id=' . $result['question_id'], 'SSL')
				);
			}
			
			$pagination = new Pagination();
			$pagination->total = $question_total;
			$pagination->page = $page;
			$pagination->limit = $this->config->get('config_limit_question_account');
			$pagination->text = $this->language->get('text_pagination');
			$pagination->url = $this->url->link('account/question', 'page={page}', 'SSL');
			
			$this->data['pagination'] = $pagination->render();
			
			$this->data['continue'] = $this->url->link('account/account', '', 'SSL');
			$this->data['add_question'] = $this->url->link('account/question/insert', '', 'SSL');
			$this->data['delete'] = $this->url->link('account/question/delete', '', 'SSL');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/question_list.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/account/question_list.tpl';
			} else {
				$this->template = 'default/template/account/question_list.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'	
			);
							
			$this->response->setOutput($this->render());				
		}
	}
	
	public function delete() { 
		if ($this->config->get('question_installed')) {
			$this->language->load('account/question');
			
			$this->load->model('catalog/question');
			
			$this->document->setTitle($this->language->get('heading_title'));

			if (isset($this->request->post['selected'])) {
				foreach ($this->request->post['selected'] as $question_id) {
					$this->model_catalog_question->deleteQuestion($question_id);
				}

				$this->session->data['success'] = $this->language->get('text_success_delete_question');
							
				$this->redirect($this->url->link('account/question', '', 'SSL'));
			}

			$this->index();
		}
	}
	
	public function info() { 
		if ($this->config->get('question_installed')) {
			$this->language->load('account/question');
			
			if (isset($this->request->get['question_id'])) {
				$question_id = $this->request->get['question_id'];
			} else {
				$question_id = 0;
			}	

			if (!$this->customer->isLogged()) {
				$this->session->data['redirect'] = $this->url->link('account/question/info', 'question_id=' . $question_id, 'SSL');
				
				$this->redirect($this->url->link('account/login', '', 'SSL'));
			}
							
			$this->load->model('catalog/question');
				
			$question_info = $this->model_catalog_question->getQuestion($question_id);
			
			if ($question_info) {
				$this->document->setTitle($this->language->get('text_question'));
				
				$this->data['breadcrumbs'] = array();
			
				$this->data['breadcrumbs'][] = array(
					'text'      => $this->language->get('text_home'),
					'href'      => $this->url->link('common/home'),        	
					'separator' => false
				); 
			
				$this->data['breadcrumbs'][] = array(
					'text'      => $this->language->get('text_account'),
					'href'      => $this->url->link('account/account', '', 'SSL'),        	
					'separator' => $this->language->get('text_separator')
				);
				
				$url = '';
				
				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}
							
				$this->data['breadcrumbs'][] = array(
					'text'      => $this->language->get('heading_title'),
					'href'      => $this->url->link('account/question', $url, 'SSL'),      	
					'separator' => $this->language->get('text_separator')
				);
				
				$this->data['breadcrumbs'][] = array(
					'text'      => $this->language->get('text_question'),
					'href'      => $this->url->link('account/question/info', 'question_id=' . $this->request->get['question_id'] . $url, 'SSL'),
					'separator' => $this->language->get('text_separator')
				);
						
				$this->data['heading_title'] = $this->language->get('text_question');
				
				$this->data['text_question_detail'] = $this->language->get('text_question_detail');
				$this->data['text_question_product'] = $this->language->get('text_question_product');
				$this->data['text_question_no_product'] = $this->language->get('text_question_no_product');
				
				$this->data['column_question_id'] = $this->language->get('column_question_id');
				$this->data['column_date_added'] = $this->language->get('column_date_added');
				$this->data['column_date_modified'] = $this->language->get('column_date_modified');
				$this->data['column_question_type'] = $this->language->get('column_question_type');
				$this->data['column_status'] = $this->language->get('column_status');
				$this->data['column_name'] = $this->language->get('column_name');
				$this->data['column_image'] = $this->language->get('column_image');
				$this->data['column_question'] = $this->language->get('column_question');
				$this->data['column_answer'] = $this->language->get('column_answer');
				
				$this->data['button_add_question'] = $this->language->get('button_add_question');
				$this->data['button_back'] = $this->language->get('button_back');
				$this->data['button_continue'] = $this->language->get('button_continue');
				
				$this->data['question_id'] = $this->request->get['question_id'];
				$this->data['date_added'] = date($this->language->get('date_format_short'), strtotime($question_info['date_added']));
				$this->data['time_added'] = date($this->language->get('time_format'), strtotime($question_info['date_added']));
				$this->data['question_category'] = $question_info['question_category'];
				$this->data['text_question'] = html_entity_decode($question_info['text_question'], ENT_QUOTES, 'UTF-8');
				$this->data['text_answer'] = html_entity_decode($question_info['text_answer'], ENT_QUOTES, 'UTF-8');
				$this->data['answer_status'] = $question_info['status'];
				$this->data['date_modified'] = date($this->language->get('date_format_short'), strtotime($question_info['date_modified']));
				$this->data['time_modified'] = date($this->language->get('time_format'), strtotime($question_info['date_modified']));
				$this->data['status'] = ($question_info['status'] ? '<span style="color:green;">'.$this->language->get('text_answered').'</span>' : '<span style="color:red;">'.$this->language->get('text_unanswered').'</span>');

				$this->load->model('catalog/product');
			
				$product_info = $this->model_catalog_product->getProduct($question_info['product_id']);
				
				$this->data['product_name'] = $product_info['name'];
				$this->data['product_href'] = $this->url->link('product/product', 'product_id=' . $question_info['product_id']);
				
				$this->load->model('tool/image');
				
				$this->data['product_image'] = $this->model_tool_image->resize($product_info['image'], 50, 50);		

				$this->data['continue'] = $this->url->link('account/question', '', 'SSL');
				$this->data['add_question'] = $this->url->link('account/question/insert', '', 'SSL');
			
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/question_info.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/account/question_info.tpl';
				} else {
					$this->template = 'default/template/account/question_info.tpl';
				}
				
				$this->children = array(
					'common/column_left',
					'common/column_right',
					'common/content_top',
					'common/content_bottom',
					'common/footer',
					'common/header'	
				);
									
				$this->response->setOutput($this->render());		
			} else {
				$this->document->setTitle($this->language->get('text_question'));
				
				$this->data['heading_title'] = $this->language->get('text_question');

				$this->data['text_error'] = $this->language->get('text_error');

				$this->data['button_continue'] = $this->language->get('button_continue');
				
				$this->data['breadcrumbs'] = array();

				$this->data['breadcrumbs'][] = array(
					'text'      => $this->language->get('text_home'),
					'href'      => $this->url->link('common/home'),
					'separator' => false
				);
				
				$this->data['breadcrumbs'][] = array(
					'text'      => $this->language->get('text_account'),
					'href'      => $this->url->link('account/account', '', 'SSL'),
					'separator' => $this->language->get('text_separator')
				);

				$this->data['breadcrumbs'][] = array(
					'text'      => $this->language->get('heading_title'),
					'href'      => $this->url->link('account/question', '', 'SSL'),
					'separator' => $this->language->get('text_separator')
				);
				
				$this->data['breadcrumbs'][] = array(
					'text'      => $this->language->get('text_question'),
					'href'      => $this->url->link('account/question/info', 'question_id=' . $question_id, 'SSL'),
					'separator' => $this->language->get('text_separator')
				);
													
				$this->data['continue'] = $this->url->link('account/question', '', 'SSL');
							
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
				} else {
					$this->template = 'default/template/error/not_found.tpl';
				}
				
				$this->children = array(
					'common/column_left',
					'common/column_right',
					'common/content_top',
					'common/content_bottom',
					'common/footer',
					'common/header'	
				);
									
				$this->response->setOutput($this->render());				
			}
		}
  	}
	
	public function insert() {
		if ($this->config->get('question_installed')) {
			$this->language->load('account/question');
			
			$this->load->model('catalog/question');
			
			$this->document->setTitle($this->language->get('text_add_question'));
			
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
				$this->model_catalog_question->addQuestionByCustomerId($this->request->post);
				
				$this->session->data['success'] = $this->language->get('text_add_success_question');
							
				$this->redirect($this->url->link('account/question', '', 'SSL'));
			}
			
			$this->data['breadcrumbs'] = array();
			
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home'),        	
				'separator' => false
			); 
			
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_account'),
				'href'      => $this->url->link('account/account', '', 'SSL'),        	
				'separator' => $this->language->get('text_separator')
			);
							
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('account/question', '', 'SSL'),      	
				'separator' => $this->language->get('text_separator')
			);
				
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_add_question'),
				'href'      => $this->url->link('account/question/insert', '', 'SSL'), 
				'separator' => $this->language->get('text_separator')
			);

			$this->data['heading_title'] = $this->language->get('text_add_question');
			
			$this->data['text_question_product'] = $this->language->get('text_question_product');
			$this->data['text_question_no_product'] = $this->language->get('text_question_no_product');
			$this->data['text_none'] = $this->language->get('text_none');
			$this->data['text_select'] = $this->language->get('text_select');

			$this->data['entry_category'] = $this->language->get('entry_category');
			$this->data['entry_product'] = $this->language->get('entry_product');
			$this->data['entry_subject'] = $this->language->get('entry_subject');
			$this->data['entry_questioner'] = $this->language->get('entry_questioner');
			$this->data['entry_text'] = $this->language->get('entry_text');
			$this->data['entry_captcha'] = $this->language->get('entry_captcha');

			$this->data['button_back'] = $this->language->get('button_back');
			$this->data['button_continue'] = $this->language->get('button_continue');
			
			$this->data['captcha_account_display'] = $this->config->get('config_captcha_account_display');

			if (isset($this->error['warning'])) {
				$this->data['error_warning'] = $this->error['warning'];
			} else {
				$this->data['error_warning'] = '';
			}
			
			if (isset($this->error['questioner'])) {
				$this->data['error_questioner'] = $this->error['questioner'];
			} else {
				$this->data['error_questioner'] = '';
			}
			
			if (isset($this->error['product'])) {
				$this->data['error_product'] = $this->error['product'];
			} else {
				$this->data['error_product'] = '';
			}
			
			if (isset($this->error['subject'])) {
				$this->data['error_subject'] = $this->error['subject'];
			} else {
				$this->data['error_subject'] = '';
			}
			
			if (isset($this->error['text_question'])) {
				$this->data['error_text_question'] = $this->error['text_question'];
			} else {
				$this->data['error_text_question'] = '';
			}
			
			if (isset($this->error['captcha'])) {
				$this->data['error_captcha'] = $this->error['captcha'];
			} else {
				$this->data['error_captcha'] = '';
			}
			
			$this->data['action'] = $this->url->link('account/question/insert', '', 'SSL');
			
			if ($this->customer->isLogged()) {
				$this->data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
			} else {
				$this->data['customer_name'] = '';
			}
			
			if ($this->customer->isLogged()) {
				$this->data['customer_email'] = $this->customer->getEmail();
			} else {
				$this->data['customer_email'] = '';
			}
			
			$this->load->model('catalog/product');
			
			if (isset($this->request->get['product_id'])) {
				$product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);
			}
			
			if (isset($this->request->post['product_id'])) {
				$this->data['product_id'] = $this->request->post['product_id'];
			} elseif (!empty($question_info)) {
				$this->data['product_id'] = $question_info['product_id'];
			} else {
				$this->data['product_id'] = '';
			}
			
			if (isset($this->request->post['product'])) {
				$this->data['product'] = $this->request->post['product'];
			} elseif (!empty($product_info)) {
				$this->data['product'] = $product_info['name'];				
			} else {
				$this->data['product'] = '';
			}
			
			$this->load->model('catalog/question');
			
			if (isset($this->request->get['question_id'])) {		
				$question_info = $this->model_catalog_question->getQuestion($this->request->get['question_id']);
			}
			
			if (isset($this->request->post['subject'])) {
				$this->data['subject'] = $this->request->post['subject'];
			} elseif (!empty($question_info)) {
				$this->data['subject'] = $question_info['subject'];
			} else {
				$this->data['subject'] = '';
			}
					
			if (isset($this->request->post['questioner'])) {
				$this->data['questioner'] = $this->request->post['questioner'];
			} elseif (!empty($question_info)) {
				$this->data['questioner'] = $question_info['questioner'];
			} else {
				$this->data['questioner'] = '';
			}
			
			if (isset($this->request->post['email'])) {
				$this->data['email'] = $this->request->post['email'];
			} elseif (!empty($question_info)) {
				$this->data['email'] = $question_info['email'];
			} else {
				$this->data['email'] = '';
			}

			if (isset($this->request->post['text_question'])) {
				$this->data['text_question'] = $this->request->post['text_question'];
			} elseif (!empty($question_info)) {
				$this->data['text_question'] = $question_info['text_question'];
			} else {
				$this->data['text_question'] = '';
			}
			
			if (isset($this->request->post['question_category'])) {
				$this->data['question_category'] = $this->request->post['question_category'];
			} elseif (!empty($question_info)) {
				$this->data['question_category'] = $question_info['question_category'];
			} else {
				$this->data['question_category'] = 1;
			}
			
			if (isset($this->request->post['captcha'])) {
				$this->data['captcha'] = $this->request->post['captcha'];
			} else {
				$this->data['captcha'] = '';
			}
			
			$this->data['back'] = $this->url->link('account/question', '', 'SSL');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/question_form.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/account/question_form.tpl';
				} else {
					$this->template = 'default/template/account/question_form.tpl';
				}
				
				$this->children = array(
					'common/column_left',
					'common/column_right',
					'common/content_top',
					'common/content_bottom',
					'common/footer',
					'common/header'	
				);
					
			$this->response->setOutput($this->render());
		}
	}
	
	protected function validate() {
		if ($this->config->get('question_installed')) {
			if ((utf8_strlen($this->request->post['questioner']) < 3) || (utf8_strlen($this->request->post['questioner']) > 64)) {
				$this->error['questioner'] = $this->language->get('error_questioner');
			}

			if ($this->request->post['question_category']) {
				if (!$this->request->post['product_id']) {
					$this->error['product'] = $this->language->get('error_product');
				}		
			}
			
			if (!$this->request->post['question_category']) {
				if ((utf8_strlen($this->request->post['subject']) < 3) || (utf8_strlen($this->request->post['subject']) > 64)) {
					$this->error['subject'] = $this->language->get('error_subject');
				}
			}

			if ((utf8_strlen($this->request->post['text_question']) < 30) || (utf8_strlen($this->request->post['text_question']) > 6000)) {
				$this->error['text_question'] = $this->language->get('error_text_question');
			}
			
			$this->data['captcha_account_display'] = $this->config->get('config_captcha_account_display');
			
			if ($this->data['captcha_account_display']) {
				if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
					$this->error['captcha'] = $this->language->get('error_captcha');
				}
			}

			if (!$this->error) {
				return true;
			} else {
				return false;
			}
		}
  	}
	
	public function captcha() {
		$this->load->library('captcha');
		
		$captcha = new Captcha();
		
		$this->session->data['captcha'] = $captcha->getCode();
		
		$captcha->showImage();
	}
	
	public function autocomplete() {
		$json = array();
		
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/product');
			 
			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);
			
			$results = $this->model_catalog_product->getProducts($data);
			
			foreach ($results as $result) {
				$json[] = array(
					'product_id' => $result['product_id'],
					'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))			
				);	
			}
		}

		$this->response->setOutput(json_encode($json));
	}
}
?>