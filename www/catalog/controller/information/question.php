<?php 
class ControllerInformationQuestion extends Controller { 	
	private $error = array();
	
	public function index() { 
    	if ($this->config->get('question_installed')) {
			$this->language->load('information/question');
			
			$this->load->model('catalog/question');
			
			if (isset($this->request->get['filter_question_id'])) {
				$filter_question_id = $this->request->get['filter_question_id'];
			} else {
				$filter_question_id = null;
			}

			if (isset($this->request->get['filter_questioner'])) {
				$filter_questioner = $this->request->get['filter_questioner'];
			} else {
				$filter_questioner = null;
			}
			
			if (isset($this->request->get['filter_question_category'])) {
				$filter_question_category = $this->request->get['filter_question_category'];
			} else {
				$filter_question_category = null;
			}
			
			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = null;
			}
			
			if (isset($this->request->get['filter_status'])) {
				$filter_status = $this->request->get['filter_status'];
			} else {
				$filter_status = null;
			}
			
			if (isset($this->request->get['filter_date_added'])) {
				$filter_date_added = $this->request->get['filter_date_added'];
			} else {
				$filter_date_added = null;
			}
			
			if (isset($this->request->get['sort'])) {
				$sort = $this->request->get['sort'];
			} else {
				$sort = 'date_added';
			}

			if (isset($this->request->get['order'])) {
				$order = $this->request->get['order'];
			} else {
				$order = 'DESC';
			}
				 
			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 1;
			}
			
			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			} else {
				$limit = $this->config->get('config_limit_question_faq');
			}
							
			$this->document->setTitle($this->language->get('heading_title'));
			$this->document->addScript('catalog/view/javascript/jquery/jquery.total-storage.min.js');
			$this->document->addScript('catalog/view/javascript/jquery/tabs.js');
			$this->document->addScript('catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/colorbox/colorbox.css');
			$this->document->addStyle('catalog/view/theme/default/stylesheet/question.css');

			$this->data['breadcrumbs'] = array();

			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home'),
				'separator' => false
			);

			$url = '';
			
			if (isset($this->request->get['filter_question_id'])) {
				$url .= '&filter_question_id=' . $this->request->get['filter_question_id'];
			}
				
			if (isset($this->request->get['filter_questioner'])) {
				$url .= '&filter_questioner=' . urlencode(html_entity_decode($this->request->get['filter_questioner'], ENT_QUOTES, 'UTF-8'));
			}
				
			if (isset($this->request->get['filter_question_category'])) {
				$url .= '&filter_question_category=' . $this->request->get['filter_question_category'];
			}
			
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}
				
			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}
				
			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
					
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}	
			
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
						
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('information/question', $url),
				'separator' => $this->language->get('text_separator')
			);
			
			$this->data['heading_title'] = $this->language->get('heading_title');
	   
			$this->data['text_empty'] = $this->language->get('text_empty');
			$this->data['text_question_id'] = $this->language->get('text_question_id');
			$this->data['text_questioner'] = $this->language->get('text_questioner');
			$this->data['text_question_category'] = $this->language->get('text_question_category');
			$this->data['text_status'] = $this->language->get('text_status');
			$this->data['text_date_added'] = $this->language->get('text_date_added');
			$this->data['text_question_product'] = $this->language->get('text_question_product');
			$this->data['text_question_no_product'] = $this->language->get('text_question_no_product');
			$this->data['text_subject'] = $this->language->get('text_subject');
			$this->data['text_answered'] = $this->language->get('text_answered');
			$this->data['text_unanswered'] = $this->language->get('text_unanswered');
			$this->data['text_admin_answer'] = $this->language->get('text_admin_answer');
			$this->data['text_limit'] = $this->language->get('text_limit');

			$this->data['column_question_id'] = $this->language->get('column_question_id');
			$this->data['column_questioner'] = $this->language->get('column_questioner');
			$this->data['column_question_category'] = $this->language->get('column_question_category');
			$this->data['column_question_product'] = $this->language->get('column_question_product');
			$this->data['column_status'] = $this->language->get('column_status');
			$this->data['column_date_added'] = $this->language->get('column_date_added');
			$this->data['column_action'] = $this->language->get('column_action');		
			
			$this->data['button_filter'] = $this->language->get('button_filter');		
			$this->data['button_add_question'] = $this->language->get('button_add_question');
			
			if (isset($this->session->data['success'])) {
				$this->data['success'] = $this->session->data['success'];
			
				unset($this->session->data['success']);
			} else {
				$this->data['success'] = '';
			}
			
			$this->data['question_faq'] = $this->config->get('config_question_faq');
			$allowed_faq = $this->config->get('config_allowed_faq');
			
			if (($allowed_faq == 'all') || (!$this->customer->isLogged() && $allowed_faq == 'guest') || ($this->customer->isLogged() && $allowed_faq == 'customer')) {
				$this->data['allowed_faq'] = true;
			} elseif (($allowed_faq == 'no') || ($this->customer->isLogged() && $allowed_faq == 'guest') || (!$this->customer->isLogged() && $allowed_faq == 'customer')) {
				$this->data['allowed_faq'] = false;
			}
			
			$this->load->model('tool/image');
			
			$this->load->model('catalog/product');
			
			$this->data['questions'] = array();

			$data = array(
				'filter_question_id' 		=> $filter_question_id,
				'filter_questioner' 		=> $filter_questioner,
				'filter_question_category'	=> $filter_question_category,
				'filter_name'	  			=> $filter_name, 
				'filter_status'   			=> $filter_status,
				'filter_date_added'     	=> $filter_date_added,
				'sort'  					=> $sort,
				'order' 					=> $order,
				'start' 					=> ($page - 1) * $limit,
				'limit' 					=> $limit
			);
				
			$question_total = $this->model_catalog_question->getTotalQuestionsFaq($data);
				
			$results = $this->model_catalog_question->getQuestionsFaq($data);
			
			foreach ($results as $result) {					
				$questioner_symbols = 30;
				$questioner_plaintext = strip_tags(html_entity_decode($result['questioner'], ENT_QUOTES, 'UTF-8'));
				if( mb_strlen($questioner_plaintext, 'UTF-8') > $questioner_symbols ) {
					$questioner_plaintext = mb_substr($questioner_plaintext, 0, $questioner_symbols, 'UTF-8') . '&nbsp;&hellip;';
				}
			
				$question_product_info = $this->model_catalog_product->getProduct($result['product_id']);
				
				if ($question_product_info['image']) {
					$product_image = $this->model_tool_image->resize($question_product_info['image'], 40, 40);
				} else {
					$product_image = $this->model_tool_image->resize('no_image.jpg', 40, 40);
				}

				$this->data['questions'][] = array(
					'question_id' 	   	=> $result['question_id'],
					'product_id' 	   	=> $result['product_id'],
					'questioner' 	   	=> $questioner_plaintext,
					'category' 			=> $result['question_category'],
					'question_category' => ($result['question_category'] ? $this->language->get('text_question_product') : $this->language->get('text_question_no_product')),
					'status'     	   	=> ($result['status'] ? '<span style="color:green;">'.$this->language->get('text_answered').'</span>' : '<span style="color:red;">'.$this->language->get('text_unanswered').'</span>'),
					'date_added' 		=> date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'time_added' 		=> date($this->language->get('time_format'), strtotime($result['date_added'])),
					'date_modified' 	=> date($this->language->get('date_format_short'), strtotime($result['date_modified'])),
					'time_modified' 	=> date($this->language->get('time_format'), strtotime($result['date_modified'])),
					'subject' 	   		=> $result['subject'],
					'text_question'     => html_entity_decode($result['text_question'], ENT_QUOTES, 'UTF-8'),
					'text_answer'       => html_entity_decode($result['text_answer'], ENT_QUOTES, 'UTF-8'),
					'product_image'		=> $product_image,
					'product_name'		=> $question_product_info['name'],
					'product_href'		=> $this->url->link('product/product', 'product_id=' . $question_product_info['product_id'])
				);
			}

			$url = '';
			
			if (isset($this->request->get['filter_question_id'])) {
				$url .= '&filter_question_id=' . $this->request->get['filter_question_id'];
			}
			
			if (isset($this->request->get['filter_questioner'])) {
				$url .= '&filter_questioner=' . urlencode(html_entity_decode($this->request->get['filter_questioner'], ENT_QUOTES, 'UTF-8'));
			}
			
			if (isset($this->request->get['filter_question_category'])) {
				$url .= '&filter_question_category=' . $this->request->get['filter_question_category'];
			}
			
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}
			
			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}
			
			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
				
			$url = '';
			
			if (isset($this->request->get['filter_question_id'])) {
				$url .= '&filter_question_id=' . $this->request->get['filter_question_id'];
			}
			
			if (isset($this->request->get['filter_questioner'])) {
				$url .= '&filter_questioner=' . urlencode(html_entity_decode($this->request->get['filter_questioner'], ENT_QUOTES, 'UTF-8'));
			}
			
			if (isset($this->request->get['filter_question_category'])) {
				$url .= '&filter_question_category=' . $this->request->get['filter_question_category'];
			}
			
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}
			
			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}
			
			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
							
			$pagination = new Pagination();
			$pagination->total = $question_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->text = $this->language->get('text_pagination');
			$pagination->url = $this->url->link('information/question', $url . '&page={page}');
				
			$this->data['pagination'] = $pagination->render();
			
			$this->data['filter_question_id'] = $filter_question_id;
			$this->data['filter_questioner'] = $filter_questioner;
			$this->data['filter_question_category'] = $filter_question_category;
			$this->data['filter_name'] = $filter_name;
			$this->data['filter_status'] = $filter_status;
			$this->data['filter_date_added'] = $filter_date_added;
			
			$this->data['sort'] = $sort;
			$this->data['order'] = $order;
			$this->data['limit'] = $limit;
			
			$this->data['add_question'] = $this->url->link('information/question/insert', '', 'SSL');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/question.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/information/question.tpl';
			} else {
				$this->template = 'default/template/information/question.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);
		
			$this->response->setOutput($this->render());
		}
  	}
	
	public function insert() {
		if ($this->config->get('question_installed')) {
			$this->language->load('information/question');
			
			$this->load->model('catalog/question');
			
			$this->document->setTitle($this->language->get('text_add_question'));
			
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
				$this->model_catalog_question->addQuestionByCustomerId($this->request->post);
				
				$this->session->data['success'] = $this->language->get('text_add_success_question');
							
				$this->redirect($this->url->link('information/question', '', 'SSL'));
			}
			
			$this->data['breadcrumbs'] = array();
			
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home'),        	
				'separator' => false
			); 
			
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('information/question', '', 'SSL'),        	
				'separator' => $this->language->get('text_separator')
			);
				
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_add_question'),
				'href'      => $this->url->link('information/question/insert', '', 'SSL'), 
				'separator' => $this->language->get('text_separator')
			);

			$this->data['heading_title'] = $this->language->get('text_add_question');
			
			$this->data['text_question_product'] = $this->language->get('text_question_product');
			$this->data['text_question_no_product'] = $this->language->get('text_question_no_product');
			$this->data['text_none'] = $this->language->get('text_none');
			$this->data['text_select'] = $this->language->get('text_select');

			$this->data['entry_questioner'] = $this->language->get('entry_questioner');
			$this->data['entry_email'] = $this->language->get('entry_email');
			$this->data['entry_category'] = $this->language->get('entry_category');
			$this->data['entry_product'] = $this->language->get('entry_product');
			$this->data['entry_subject'] = $this->language->get('entry_subject');
			$this->data['entry_text'] = $this->language->get('entry_text');
			$this->data['entry_captcha'] = $this->language->get('entry_captcha');

			$this->data['button_back'] = $this->language->get('button_back');
			$this->data['button_continue'] = $this->language->get('button_continue');
			
			$captcha_faq = $this->config->get('config_captcha_faq_display');
			
			if (($captcha_faq == 'all') || (!$this->customer->isLogged() && $captcha_faq == 'guest') || ($this->customer->isLogged() && $captcha_faq == 'customer')) {
				$this->data['captcha_faq'] = true;
			} elseif (($captcha_faq == 'no') || ($this->customer->isLogged() && $captcha_faq == 'guest') || (!$this->customer->isLogged() && $captcha_faq == 'customer')) {
				$this->data['captcha_faq'] = false;
			}

			if (isset($this->error['warning'])) {
				$this->data['error_warning'] = $this->error['warning'];
			} else {
				$this->data['error_warning'] = '';
			}
			
			if (isset($this->error['questioner'])) {
				$this->data['error_questioner'] = $this->error['questioner'];
			} else {
				$this->data['error_questioner'] = '';
			}
			
			if (isset($this->error['email'])) {
				$this->data['error_email'] = $this->error['email'];
			} else {
				$this->data['error_email'] = '';
			}
			
			if (isset($this->error['product'])) {
				$this->data['error_product'] = $this->error['product'];
			} else {
				$this->data['error_product'] = '';
			}
			
			if (isset($this->error['subject'])) {
				$this->data['error_subject'] = $this->error['subject'];
			} else {
				$this->data['error_subject'] = '';
			}
			
			if (isset($this->error['text_question'])) {
				$this->data['error_text_question'] = $this->error['text_question'];
			} else {
				$this->data['error_text_question'] = '';
			}
			
			if (isset($this->error['captcha'])) {
				$this->data['error_captcha'] = $this->error['captcha'];
			} else {
				$this->data['error_captcha'] = '';
			}
			
			$this->data['action'] = $this->url->link('information/question/insert', '', 'SSL');
			
			if ($this->customer->isLogged()) {
				$this->data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
			} else {
				$this->data['customer_name'] = '';
			}
			
			if ($this->customer->isLogged()) {
				$this->data['customer_email'] = $this->customer->getEmail();
			} else {
				$this->data['customer_email'] = '';
			}
			
			$this->load->model('catalog/product');
			
			if (isset($this->request->get['product_id'])) {
				$product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);
			}
			
			if (isset($this->request->post['product_id'])) {
				$this->data['product_id'] = $this->request->post['product_id'];
			} elseif (!empty($question_info)) {
				$this->data['product_id'] = $question_info['product_id'];
			} else {
				$this->data['product_id'] = '';
			}
			
			if (isset($this->request->post['product'])) {
				$this->data['product'] = $this->request->post['product'];
			} elseif (!empty($product_info)) {
				$this->data['product'] = $product_info['name'];				
			} else {
				$this->data['product'] = '';
			}
			
			$this->load->model('catalog/question');
			
			if (isset($this->request->get['question_id'])) {		
				$question_info = $this->model_catalog_question->getQuestion($this->request->get['question_id']);
			}
			
			if (isset($this->request->post['subject'])) {
				$this->data['subject'] = $this->request->post['subject'];
			} elseif (!empty($question_info)) {
				$this->data['subject'] = $question_info['subject'];
			} else {
				$this->data['subject'] = '';
			}
					
			if (isset($this->request->post['questioner'])) {
				$this->data['questioner'] = $this->request->post['questioner'];
			} elseif (!empty($question_info)) {
				$this->data['questioner'] = $question_info['questioner'];
			} else {
				$this->data['questioner'] = '';
			}
			
			if (isset($this->request->post['email'])) {
				$this->data['email'] = $this->request->post['email'];
			} elseif (!empty($question_info)) {
				$this->data['email'] = $question_info['email'];
			} else {
				$this->data['email'] = '';
			}

			if (isset($this->request->post['text_question'])) {
				$this->data['text_question'] = $this->request->post['text_question'];
			} elseif (!empty($question_info)) {
				$this->data['text_question'] = $question_info['text_question'];
			} else {
				$this->data['text_question'] = '';
			}
			
			if (isset($this->request->post['question_category'])) {
				$this->data['question_category'] = $this->request->post['question_category'];
			} elseif (!empty($question_info)) {
				$this->data['question_category'] = $question_info['question_category'];
			} else {
				$this->data['question_category'] = 1;
			}
			
			if (isset($this->request->post['captcha'])) {
				$this->data['captcha'] = $this->request->post['captcha'];
			} else {
				$this->data['captcha'] = '';
			}
			
			$this->data['back'] = $this->url->link('information/question', '', 'SSL');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/question_form.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/information/question_form.tpl';
				} else {
					$this->template = 'default/template/information/question_form.tpl';
				}
				
				$this->children = array(
					'common/column_left',
					'common/column_right',
					'common/content_top',
					'common/content_bottom',
					'common/footer',
					'common/header'	
				);
					
			$this->response->setOutput($this->render());
		}
	}
	
	protected function validate() {
		if ($this->config->get('question_installed')) {
			if ((utf8_strlen($this->request->post['questioner']) < 3) || (utf8_strlen($this->request->post['questioner']) > 64)) {
				$this->error['questioner'] = $this->language->get('error_questioner');
			}
			
			if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
				$this->error['email'] = $this->language->get('error_email');
			}

			if ($this->request->post['question_category']) {
				if (!$this->request->post['product_id']) {
					$this->error['product'] = $this->language->get('error_product');
				}		
			}
			
			if (!$this->request->post['question_category']) {
				if ((utf8_strlen($this->request->post['subject']) < 3) || (utf8_strlen($this->request->post['subject']) > 64)) {
					$this->error['subject'] = $this->language->get('error_subject');
				}
			}

			if ((utf8_strlen($this->request->post['text_question']) < 30) || (utf8_strlen($this->request->post['text_question']) > 6000)) {
				$this->error['text_question'] = $this->language->get('error_text_question');
			}
			
			$captcha_faq = $this->config->get('config_captcha_faq_display');
			
			if (($captcha_faq == 'all') || (!$this->customer->isLogged() && $captcha_faq == 'guest') || ($this->customer->isLogged() && $captcha_faq == 'customer')) {
				if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
					$this->error['captcha'] = $this->language->get('error_captcha');
				}
			} elseif (($captcha_faq == 'no') || ($this->customer->isLogged() && $captcha_faq == 'guest') || (!$this->customer->isLogged() && $captcha_faq == 'customer')) {
				$this->session->data['captcha'] = false;
			}

			if (!$this->error) {
				return true;
			} else {
				return false;
			}
		}
  	}
	
	public function captcha() {
		$this->load->library('captcha');
		
		$captcha = new Captcha();
		
		$this->session->data['captcha'] = $captcha->getCode();
		
		$captcha->showImage();
	}
	
	public function autocomplete() {
		$json = array();
		
		if (isset($this->request->get['filter_questioner'])) {
			$this->load->model('catalog/question');
			
			if (isset($this->request->get['filter_questioner'])) {
				$filter_questioner = $this->request->get['filter_questioner'];
			} else {
				$filter_questioner = '';
			}			
			
			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];	
			} else {
				$limit = 20;	
			}			
						
			$data = array(
				'filter_questioner' => $filter_questioner,
				'start'          	=> 0,
				'limit'          	=> $limit
			);
			
			$results = $this->model_catalog_question->getQuestionsFaq($data);
			
			foreach ($results as $result) {
					
				$json[] = array(
					'question_id' => $result['question_id'],
					'questioner'  => strip_tags(html_entity_decode($result['questioner'], ENT_QUOTES, 'UTF-8'))	
				);	
			}
		}

		$this->response->setOutput(json_encode($json));
	}
	
	public function autocomplete_product() {
		$json = array();
		
		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/product');
			
			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}	
			 
			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);
			$results = $this->model_catalog_product->getProducts($data);
			
			foreach ($results as $result) {				
				$json[] = array(
					'product_id' => $result['product_id'],
					'name' 		 => html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')
				);	
			}
			

		}

		$this->response->setOutput(json_encode($json));
	}
}
?>