<?php  
class ControllerModuleVote extends Controller {
	protected function index($setting) {
	
		$this->language->load('module/vote');
		$this->load->model('catalog/product');
		$this->load->model('catalog/vote');
		$this->load->model('catalog/review');
		
		$product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);
		
    	$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['textvotes'] = $this->language->get('textvotes');
		$this->data['product_id'] = $this->request->get['product_id'];
		$this->data['display'] = "";
		$this->data['rating'] = (int)$product_info['rating'];
		$this->data['votes'] = $this->model_catalog_vote->getTotalVotesByProductId($this->request->get['product_id']);
		$this->data['tab_review'] = sprintf($this->language->get('tab_review'), $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']));
		
		
		if ($setting['position']=='content_top') {
		$this->document->addScript('catalog/view/javascript/jquery/vote/vote-pos.js');
		$this->document->addScript('catalog/view/javascript/jquery/vote/lib/jquery.raty.js');
		$this->data['display'] = "display:none";
		} else {
		$this->document->addScript('catalog/view/javascript/jquery/vote/lib/jquery.raty.js');
		$this->document->addScript('catalog/view/javascript/jquery/vote/vote-pos-side.js');
		
		}


		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/vote.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/vote.tpl';
		} else {
			$this->template = 'default/template/module/vote.tpl';
		}
		
		$this->render();
  	
	}
	
	
		public function write() {
		
		$this->language->load('module/vote');
		$this->load->model('catalog/vote');
				
			$vote = array(
					'product_id' => (int)$this->request->get['product_id'],
					'rating'   	 => (int)$this->request->post['rating'],
					'name'    	 => ('vote'),
					'text_vote'  => ('vote'),
			
				);	
		
		
		$json = array();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
				
		$vote_id = "v".$this->request->get['product_id'];
			
		if (!empty($this->session->data[$vote_id])) {
			
			if ($this->session->data[$vote_id] == $this->request->get['product_id']) {
				$json['error'] = $this->language->get('error_rating');
			}
		}
							
			if (!isset($json['error'])) {
				$this->model_catalog_vote->addVote($vote);
				
				$json['success'] = $this->language->get('text_success');
				$json['votes']   = $this->model_catalog_vote->getTotalVotesByProductId($this->request->get['product_id']);
				$this->session->data[$vote_id]= $this->request->get['product_id'];	
							
			}
				
		}
						
		$this->response->setOutput(json_encode($json));
		
		}
}
?>