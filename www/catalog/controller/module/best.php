<?php
class ControllerModulebest extends Controller {
	protected function index($setting) {
		$this->language->load('module/best'); 

      	$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['button_cart'] = $this->language->get('button_cart');
		
		$this->load->model('catalog/product'); 
		
		$this->load->model('tool/image');
		
		$this->data['products_day'] = array();

		$products_day = explode(',', $this->config->get('best_product_day'));
		
		$products_uspey = explode(',', $this->config->get('best_product_uspey'));

		if (empty($setting['limit'])) {
			$setting['limit'] = 5;
		}
		
		$this->data['position'] = $setting['position'];
		$this->data['image_width'] = $setting['image_width'];
		$this->data['image_height'] = $setting['image_height'];
		$this->data['image_addit_width'] = $setting['image_addit_width'];
		$this->data['image_addit_height'] = $setting['image_addit_height'];
		
		$this->data['modules_setting'] = array();
		
		if (isset($this->request->post['best_module_setting'])) {
			$modules_setting = $this->request->post['best_module_setting'];
		} elseif ($this->config->get('best_module_setting')) { 
			$modules_setting = $this->config->get('best_module_setting');
		}
		
		if ($this->config->get('best_module_setting')) {
			$this->data['products_day_on'] = "1";
		}
		
		foreach ($modules_setting as $module_setting) {
		if (isset($module_setting['pervon_kol_vo'])) {$this->data['pervon_kol_vo'] = $module_setting['pervon_kol_vo'];} else {$this->data['pervon_kol_vo'] = "200";}
		if (isset($module_setting['color_best_day'])) {$this->data['color_best_day'] = $module_setting['color_best_day'];} else {$this->data['color_best_day'] = "#2E85BC";}
		if (isset($module_setting['hea_color_best_day'])) {$this->data['hea_color_best_day'] = $module_setting['hea_color_best_day'];} else {$this->data['hea_color_best_day'] = "#FFFFFF";}
		if (isset($module_setting['hea_color_best_uspey'])) {$this->data['hea_color_best_uspey'] = $module_setting['hea_color_best_uspey'];} else {$this->data['hea_color_best_uspey'] = "#333333";}
		if (isset($module_setting['off_product_day'])) {$this->data['off_product_day'] = $module_setting['off_product_day'];} else {$this->data['off_product_day'] = "1";}
		if (isset($module_setting['off_addit'])) {$this->data['off_addit'] = $module_setting['off_addit'];} else {$this->data['off_addit'] = "1";}
		if (isset($module_setting['schetchik'])) {$this->data['schetchik'] = $module_setting['schetchik'];} else {$this->data['schetchik'] = "1";}
		if (isset($module_setting['rating_and_prodano'])) {$this->data['rating_and_prodano'] = $module_setting['rating_and_prodano'];} else {$this->data['rating_and_prodano'] = "1";}
		if (isset($module_setting['responsive_setting'])) {$this->data['responsive_setting'] = $module_setting['responsive_setting'];} else {$this->data['responsive_setting'] = "0";}
		if (isset($module_setting['kol_vo_responsive_setting'])) {$this->data['kol_vo_responsive_setting'] = $module_setting['kol_vo_responsive_setting'];} else {$this->data['kol_vo_responsive_setting'] = "3";}
		}
		
		$products_day = array_slice($products_day, 0, (int)$setting['limit']);
		
		foreach ($products_day as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);
			
			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $setting['image_width'], $setting['image_height']);
				} else {
					$image = false;
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}
						
				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
				
				if ($this->config->get('config_review_status')) {
					$rating = $product_info['rating'];
				} else {
					$rating = false;
				}
					
				$this->data['products_day'][] = array(
					'product_id' => $product_info['product_id'],
					'thumb'   	 => $image,
					'name'    	 => $product_info['name'],
					'price'   	 => $price,
					'special' 	 => $special,
					'rating'     => $rating,
					'reviews'    => sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']),
					'href'    	 => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
				);
			}
		}
		
		$products_uspey = array_slice($products_uspey, 0, (int)$setting['limit']);
		
		foreach ($products_uspey as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);
			
			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $setting['image_width'], $setting['image_height']);
				} else {
					$image = false;
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}
						
				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
				
				if ($this->config->get('config_review_status')) {
					$rating = $product_info['rating'];
				} else {
					$rating = false;
				}
					
				$this->data['products_uspey'][] = array(
					'product_id' => $product_info['product_id'],
					'thumb'   	 => $image,
					'name'    	 => $product_info['name'],
					'price'   	 => $price,
					'special' 	 => $special,
					'rating'     => $rating,
					'reviews'    => sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']),
					'href'    	 => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
				);
			}
		}
		
		$data = array(
			'sort'  => 'pd.name',
			'order' => 'ASC',
			'start' => 0,
			'limit' => $setting['limit']
		);
		
		

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/best.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/best.tpl';
		} else {
			$this->template = 'c-trade/template/module/best.tpl';
		}

		$this->render();
	}
}
?>