<?php echo $header; ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/question.css" />
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <div class="border">
  <table class="list">
    <thead>
      <tr>
        <td class="left"><?php echo $text_question_detail; ?></td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="left">
		  <table class="list">
			<thead>
			  <tr>
				<td class="center" width="5%"><?php echo $column_question_id; ?></td>
				<td class="center" width="20%"><?php echo $column_date_added; ?></td>
				<?php if ($text_answer) { ?>
				  <td class="center" width="20%"><?php echo $column_date_modified; ?></td>
				<?php } ?>
				<td class="left"><?php echo $column_question_type; ?></td>
				<td class="center" width="13%"><?php echo $column_status; ?></td>
			  </tr>
			</thead>
			<tbody>
			  <tr>
				<td class="center"><?php echo $question_id; ?></td>
				<td class="center"><?php echo $date_added; ?> (<?php echo $time_added; ?>)</td>
				<?php if ($text_answer) { ?>
				  <td class="center"><?php echo $date_modified; ?> (<?php echo $time_modified; ?>)</td>
				<?php } ?>
				<?php if ($question_category) { ?>
				  <td class="left"><?php echo $text_question_product; ?></td>
				<?php } else { ?>
				  <td class="left"><?php echo $text_question_no_product; ?></td>
				<?php } ?>
				<td class="center"><?php echo $status; ?></td>
			  </tr>
			<tbody>
		  </table>
		  <?php if ($question_category) { ?>
			<table class="list">
			  <thead>
				<tr>
				  <td class="center" width="60"><?php echo $column_image; ?></td>
				  <td class="left"><?php echo $column_name; ?></td>
				</tr>
			  </thead>
			  <tbody>
				<tr>
				  <td class="center"><img src="<?php echo $product_image; ?>" alt="<?php echo $product_name; ?>"></td>
				  <td class="left"><a href="<?php echo $product_href; ?>"><?php echo $product_name; ?></a></td>
				</tr>
			  <tbody>
			</table>
		  <?php } ?>
		  <table class="list">
			<thead>
			  <tr>
				<td class="left"><?php echo $column_question; ?></td>
			  </tr>
			</thead>
			<tbody>
			  <tr>
				<td class="left"><?php echo $text_question; ?></td>
			  </tr>
			<tbody>
		  </table>
		  <?php if ($text_answer) { ?>
		    <table class="list">
			  <thead>
			    <tr>
				  <td class="left"><?php echo $column_answer; ?></td>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
				  <td class="left"><?php echo $text_answer; ?></td>
			    </tr>
			  <tbody>
		    </table>
		  <?php } ?>
	    </td>
      </tr>
    </tbody>
  </table>
  <div class="buttons">
    <div class="left"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_back; ?></a></div>
	<div class="right"><a href="<?php echo $add_question; ?>" class="button"><?php echo $button_add_question; ?></a></div>
  </div>
  </div>
  <div class="bottom"></div>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?> 