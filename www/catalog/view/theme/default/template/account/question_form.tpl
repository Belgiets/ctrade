<?php echo $header; ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/question.css" />
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <div class="border">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	  <table id="question" class="form">
        <tr>
          <td><span class="required">*</span> <?php echo $entry_questioner; ?></td>
		  <td><input type="text" name="questioner" value="<?php echo $customer_name; ?>" size="50" />
		    <?php if ($error_questioner) { ?>
              <span class="error"><?php echo $error_questioner; ?></span>
            <?php } ?></td>
        </tr>
		<tr style="display:none;">
          <td><input type="text" name="email" value="<?php echo $customer_email; ?>" /></td>
        </tr>
		<tr>
          <td><?php echo $entry_category; ?></td>
          <td><select name="question_category">
            <?php if ($question_category) { ?>
            <option value="1" selected="selected"><?php echo $text_question_product; ?></option>
            <option value="0"><?php echo $text_question_no_product; ?></option>
            <?php } else { ?>
            <option value="1"><?php echo $text_question_product; ?></option>
            <option value="0" selected="selected"><?php echo $text_question_no_product; ?></option>
            <?php } ?>
            </select></td>
        </tr>
		<tbody id="question-1" class="question">
		  <tr>
            <td><span class="required">*</span> <?php echo $entry_product; ?></td>
            <td><input type="text" name="product" value="<?php echo $product; ?>" size="50" />
              <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
              <?php if ($error_product) { ?>
              <span class="error"><?php echo $error_product; ?></span>
              <?php } ?></td>
		  </tr>
        </tbody>
		<tbody id="question-0" class="question">
		  <tr>
            <td><span class="required">*</span> <?php echo $entry_subject; ?></td>
            <td><input type="text" name="subject" value="<?php echo $subject; ?>" size="50" />
              <?php if ($error_subject) { ?>
              <span class="error"><?php echo $error_subject; ?></span>
              <?php } ?></td>
		  </tr>
        </tbody>
        <tr>
          <td colspan="2"><span class="required">*</span> <b><?php echo $entry_text; ?></b><br />
			<textarea name="text_question" id="text-question"><?php echo $text_question; ?></textarea>
            <?php if ($error_text_question) { ?>
            <span class="error"><?php echo $error_text_question; ?></span>
            <?php } ?></td>
        </tr>
		<?php if ($captcha_account_display) { ?>
		  <tr>
            <td><?php echo $entry_captcha; ?></td>
		    <td><input type="text" name="captcha" value="" /><br />
		      <img src="index.php?route=account/question/captcha" alt="" />
		      <?php if ($error_captcha) { ?>
              <span class="error"><?php echo $error_captcha; ?></span>
              <?php } ?></td>
		  </tr>
		<?php } ?>
      </table>
	  <div class="buttons">
        <div class="left"><a href="<?php echo $back; ?>" class="button"><?php echo $button_back; ?></a></div>
        <div class="right">
          <input type="submit" value="<?php echo $button_continue; ?>" class="button" />
      </div>
    </div>
    </form>
  </div>
  <div class="bottom"></div>
  <?php echo $content_bottom; ?></div>
<script type="text/javascript" src="admin/view/javascript/ckeditor/ckeditor.js"></script> 
<script type="text/javascript"><!--
CKEDITOR.replace('text-question');
//--></script> 
<script type="text/javascript"><!--
CKEDITOR.replace('text-answer');
//--></script> 
<script type="text/javascript"><!--	
$('select[name=\'question_category\']').bind('change', function() {
	$('#question .question').hide();
	
	$('#question #question-' + $(this).attr('value').replace('_', '-')).show();
});

$('select[name=\'question_category\']').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('input[name=\'product\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=account/question/autocomplete&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id
					}
				}));
			}
		});
	},
	select: function(event, ui) {
		$('input[name=\'product\']').val(ui.item.label);
		$('input[name=\'product_id\']').val(ui.item.value);
		
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});
//--></script> 
<?php echo $footer; ?> 