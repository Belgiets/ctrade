<?php echo $header; ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/question.css" />
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-tooltip/jquery.tooltip.min.js"></script>
<link type="text/css" href="catalog/view/javascript/jquery/jquery-tooltip/jquery.tooltip.css" rel="stylesheet" />
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <div class="border">
    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
	   <?php if ($questions) { ?>
		<table class="list">
          <thead>
            <tr>
			  <?php if (($this->config->get('config_question_customer_delete') == 'all') || ($this->config->get('config_question_customer_delete') == 'product') || ($this->config->get('config_question_customer_delete') == 'no_product')) { ?>
			    <td width="1" class="center"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
			  <?php } ?>
			  <td class="center"><?php echo $text_question_id; ?></td>
			  <td class="center"><?php echo $text_date_added; ?></td>
			  <td class="center"><?php echo $text_date_modified; ?></td>
			  <td class="left"><?php echo $text_question_type; ?></td>
			  <td class="center"><?php echo $text_status; ?></td>
			  <td class="center"><?php echo $text_action; ?></td>
			</tr>
		  </thead>
		  <?php foreach ($questions as $question) { ?>
			<tbody>
			  <tr>
			    <?php if (($this->config->get('config_question_customer_delete') == 'all') || ($this->config->get('config_question_customer_delete') == 'product' && $question['question_category'] == '1')  || ($this->config->get('config_question_customer_delete') == 'no_product' && $question['question_category'] == '0')) { ?>
				  <td style="text-align: center;"><?php if ($question['selected']) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $question['question_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $question['question_id']; ?>" />
                    <?php } ?></td>
				<?php } elseif (($this->config->get('config_question_customer_delete') == 'product' && $question['question_category'] == '0')  || ($this->config->get('config_question_customer_delete') == 'no_product' && $question['question_category'] == '1')) { ?>
				<td><div class="info-question-delete tooltip" title="<?php echo $text_info_question_delete; ?>"></div></td>
				<?php } ?>
				<td class="center"><?php echo $question['question_id']; ?></td>
				<td class="center"><?php echo $question['date_added']; ?> (<?php echo $question['time_added']; ?>)</td>
				<?php if ($question['answer_status']) { ?>
				  <td class="center"><?php echo $question['date_modified']; ?> (<?php echo $question['time_modified']; ?>)</td>
				  <?php } else { ?>
				  <td class="center"><?php echo $text_no_answer; ?></td>
				<?php } ?>
				<?php if ($question['question_category']) { ?>
				  <td class="left"><?php echo $text_question_product; ?></td>
				  <?php } else { ?>
				  <td class="left"><?php echo $text_question_no_product; ?></td>
				<?php } ?>
				<td class="center"><?php echo $question['status']; ?></td>
				<td class="center"><a href="<?php echo $question['href']; ?>" class="button"><?php echo $button_view; ?></a></td>
			  </tr>
			</tbody>
		  <?php } ?>
		</table>
	    <div class="pagination"><?php echo $pagination; ?></div>
	    <?php } else { ?>
	    <div class="content"><?php echo $text_empty; ?></div>
	  <?php } ?>
	</form>
    <div class="buttons">
	  <div class="left"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_back; ?></a></div>
	  <div class="right">
	    <a href="<?php echo $add_question; ?>" class="button"><?php echo $button_add_question; ?></a>
	    <?php if ($questions) { ?>
		  <?php if ($this->config->get('config_question_customer_delete') == 'all' || $this->config->get('config_question_customer_delete') == 'product' || $this->config->get('config_question_customer_delete') == 'no_product') { ?>
	        <a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a>
		  <?php } ?>
	    <?php } ?>
	  </div>
    </div>
  </div>
  <div class="bottom"></div>
  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
$("div.tooltip").tooltip({
	track: true, 
    delay: 0, 
    showURL: false, 
    showBody: " - ", 
    fade: 250 
});
//--></script>
<?php echo $footer; ?>