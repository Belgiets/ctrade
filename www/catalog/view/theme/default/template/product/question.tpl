<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/question.css" />
<?php if ($questions) { ?>
  <?php foreach ($questions as $question) { ?>
	<div class="question-list">
      <div class="questioner"><b><?php echo $text_questioner; ?></b> - <?php echo $question['questioner']; ?> <span><?php echo $question['date_added']; ?> (<?php echo $question['time_added']; ?>)</span></div>
	  <div class="text-question"><?php echo $question['text_question']; ?></div>
	  <?php if ($question['text_answer']) { ?>
	    <div class="admin-answer"><b><?php echo $text_admin_answer; ?></b> <span><?php echo $question['date_modified']; ?> (<?php echo $question['time_modified']; ?>)</span></div>
	    <div class="text-answer"><?php echo $question['text_answer']; ?></div>
	  <?php } ?>
	</div>
  <?php } ?>
  <div class="pagination"><?php echo $pagination; ?></div>
<?php } else { ?>
  <div class="content"><?php echo $text_no_questions; ?></div>
<?php } ?>
