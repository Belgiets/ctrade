<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<h1><?php echo $heading_title; ?></h1>
  <div class="border">
    <?php if ($questions) { ?>
	<table class="list" id="form">
      <thead>
	    <tr>
		  <td class="right"><?php echo $column_question_id; ?></a></td>
		  <td class="left"><?php echo $column_questioner; ?></td>
		  <td class="left"><?php echo $column_question_category; ?></td>
		  <td class="left"><?php echo $column_question_product; ?></td>
          <td class="center"><?php echo $column_status; ?></td>
          <td class="center"><?php echo $column_date_added; ?></td>
          <td class="center"><?php echo $column_action; ?></td>
        </tr>		  
	  </thead>
	  <tbody>
        <tr class="question-filter">
          <td align="right"><input type="text" name="filter_question_id" value="<?php echo $filter_question_id; ?>" size="4" style="text-align: right;" /></td>
          <td><input type="text" name="filter_questioner" value="<?php echo $filter_questioner; ?>" /></td>
          <td><select name="filter_question_category" onchange="filter();">
            <option value="*"></option>
            <?php if ($filter_question_category) { ?>
            <option value="1" selected="selected"><?php echo $text_question_product; ?></option>
            <?php } else { ?>
            <option value="1"><?php echo $text_question_product; ?></option>
            <?php } ?>
            <?php if (($filter_question_category !== null) && !$filter_question_category) { ?>
            <option value="0" selected="selected"><?php echo $text_question_no_product; ?></option>
            <?php } else { ?>
            <option value="0"><?php echo $text_question_no_product; ?></option>
            <?php } ?>
            </select></td>
		  <td><input type="text" name="filter_name" value="<?php echo $filter_name; ?>" /></td>
          <td align="center"><select name="filter_status" onchange="filter();">
            <option value="*"></option>
            <?php if ($filter_status) { ?>
            <option value="1" selected="selected"><?php echo $text_answered; ?></option>
            <?php } else { ?>
            <option value="1"><?php echo $text_answered; ?></option>
            <?php } ?>
            <?php if (($filter_status !== null) && !$filter_status) { ?>
            <option value="0" selected="selected"><?php echo $text_unanswered; ?></option>
            <?php } else { ?>
            <option value="0"><?php echo $text_unanswered; ?></option>
            <?php } ?>
            </select></td>
          <td align="center"><input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" size="12" class="date" /></td>
          <td align="center"><a onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
		</tr>
        <?php foreach ($questions as $question) { ?>
		  <tr>
			<td class="left" colspan="7">
			  <div class="question-list">
			    <div class="questioner"><?php echo $text_question_id; ?><b><?php echo $question['question_id']; ?></b> &bull;&bull;&bull; <?php echo $text_question_category; ?><b><?php echo $question['question_category']; ?></b> <span class="text-questioner"><?php echo $text_questioner; ?><b><?php echo $question['questioner']; ?></b> - <?php echo $question['date_added']; ?> (<?php echo $question['time_added']; ?>) &bull;&bull;&bull; <?php echo $text_status; ?><b>&nbsp;<?php echo $question['status']; ?></b></span></div>
				<div class="text-question">
				  <?php if ($question['category']) { ?>
				    <div class="question-product">
					  <a href="<?php echo $question['product_href']; ?>"><img align="absmiddle" src="<?php echo $question['product_image']; ?>" alt="<?php echo $question['product_name']; ?>"><?php echo $question['product_name']; ?></a>
					</div>
					<?php echo $question['text_question']; ?>
				    <?php } else { ?>
					<div class="subject"><?php echo $text_subject; ?><b><?php echo $question['subject']; ?></b></div>
				    <?php echo $question['text_question']; ?>
				  <?php } ?>
				</div>
				  <?php if ($question['text_answer']) { ?>
				    <div class="admin-answer"><b><?php echo $text_admin_answer; ?></b> <span><?php echo $question['date_modified']; ?> (<?php echo $question['time_modified']; ?>)</span></div>
					<div class="text-answer"><?php echo $question['text_answer']; ?></div>
				  <?php } ?>
				</div>
			  </div>
			</td>
          </tr>
        <?php } ?>
	  </tbody>
	</table>
	<div class="pagination"><?php echo $pagination; ?></div>
	<?php } else { ?>
      <div class="content"><?php echo $text_empty; ?></div>
    <?php } ?>
	<?php if ($question_faq) { ?>
	  <?php if ($allowed_faq) { ?>
        <div class="buttons">
	      <div class="right"><a href="<?php echo $add_question; ?>" class="button"><?php echo $button_add_question; ?></a></div>
        </div>
	  <?php } ?>
	<?php } ?>
  </div>
<div class="bottom"></div>
<?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=information/question';
	
	var filter_question_id = $('input[name=\'filter_question_id\']').attr('value');
	
	if (filter_question_id) {
		url += '&filter_question_id=' + encodeURIComponent(filter_question_id);
	}
	
	var filter_questioner = $('input[name=\'filter_questioner\']').attr('value');
	
	if (filter_questioner) {
		url += '&filter_questioner=' + encodeURIComponent(filter_questioner);
	}
	
	var filter_name = $('input[name=\'filter_name\']').attr('value');
	
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}
	
	var filter_question_category = $('select[name=\'filter_question_category\']').attr('value');
	
	if (filter_question_category != '*') {
		url += '&filter_question_category=' + encodeURIComponent(filter_question_category);
	}
	
	var filter_status = $('select[name=\'filter_status\']').attr('value');
	
	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}
	
	var filter_date_added = $('input[name=\'filter_date_added\']').attr('value');
	
	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}
				
	location = url;
}
//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script>  
<script type="text/javascript"><!--
$('input[name=\'filter_questioner\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=information/question/autocomplete&filter_questioner=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.questioner,
						value: item.question_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_questioner\']').val(ui.item.label);
						
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=information/question/autocomplete_product&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_name\']').val(ui.item.label);
						
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});
//--></script> 
<?php echo $footer; ?>