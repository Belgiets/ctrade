<?php ?>
<div id="votes" style="<?php echo $display; ?>">

<div class="box">
  <div class="box-heading"><span><?php echo $heading_title; ?></span></div>
  <div class="box-content">
 		<div id="vote0"> 
			<div id="vote" style="vertical-align: middle;">
				<div id="star" style="display:inline-block; vertical-align: text-top;"></div>
				<div class="vote-text" style="display: inline-block;"> <?php echo $textvotes; ?>: <span><?php echo $votes; ?></span> </div>

				<div id="p-review"><?php echo $tab_review; ?></div>
  			
			</div>
	  	</div>
  </div>
</div>

</div>

<script type="text/javascript"><!-- 
 
$(document).ready(function() {
	
$.fn.raty.defaults.path = 'catalog/view/javascript/jquery/vote/lib/img/';
$.fn.raty.defaults.size =	'24';
$.fn.raty.defaults.hints =	'12345';
$('#star').raty({
	score: <?php echo $rating; ?>,
		
	click: function(score) {
        $.ajax({
		url: 'index.php?route=module/vote/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'rating=' + score,

			success: function(data) {
				if (data['error']) {
				$('#vote').after('<div class="warning">' + data['error'] + '</div>');
				$('.warning').fadeIn('slow').delay(2000).fadeOut(3000,function() {
				$('.warning').remove();
				$('#star').raty({ score: <?php echo $rating; ?> });
				});		
				}
			
				if (data['success']) {
				$('#vote').after('<div class="success">' + data['success'] + '</div>');
				$('.success').fadeIn('slow').delay(2000).fadeOut(3000);					
				$('.vote-text span').text(data['votes']);
				
				}
			}
		});
	   
	}
	
});

});

//--></script>  