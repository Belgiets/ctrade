<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/c-trade/stylesheet/stylesheet.css" />
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/tabs.js"></script>
<script>
var cm_style = "blackred";
</script>
<script type="text/javascript" src="/callme/js/callme.js"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
<!--[if IE 7]> 
<link rel="stylesheet" type="text/css" href="catalog/view/theme/c-trade/stylesheet/ie7.css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/c-trade/stylesheet/ie6.css" />
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->
<?php if ($stores) { ?>
<script type="text/javascript"><!--
$(document).ready(function() {
<?php foreach ($stores as $store) { ?>
$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
<?php } ?>
});
//--></script>
<?php } ?>
<?php echo $google_analytics; ?>
</head>
<body oncopy="return false;" <?php if (!isset($this->request->get['route'])) {$this->request->get['route']='common/home';}if ($this->request->get['route']=='common/home') {echo 'class="main"';}?>>
<div class="blackout"></div>
<div id="header">
<div id="container">
<div id="top">
	<div class="city">
		<span>город : </span> Черкассы
	</div>
	<noindex>
	<div class="links">
		<a rel="nofollow" href="/about-us">О магазине</a>
		<a rel="nofollow" href="/oplata-i-dostavka"><?php echo 'Оплата и доставка'; ?></a>
		<a rel="nofollow" href="/garantiynyy-i-negarantiynyy-remont-tsifrovoy-tehniki"><?php echo 'Гарантия'; ?></a>
		<a rel="nofollow" href="/servis"><?php echo 'Сервис'; ?></a>
		<a rel="nofollow" href="<?php echo $wishlist; ?>" id="wishlist-total"><?php echo $text_wishlist; ?></a>
	</div>
	</noindex>
<div id="welcome">
    <?php if (!$logged) { ?>
    <?php echo $text_welcome; ?>
    <?php } else { ?>
    <?php echo $text_logged; ?>
    <?php } ?>
</div>
 </div>
  <?php if ($logo) { ?>
  <div id="logo">
  <?php if ($home == $og_url) { ?>
  <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" />
  <?php } else { ?>
  <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
  <?php } ?>
  </div>
  <?php } ?>
  <div class="phones">
	<div class="mts">(099) 290-61-41</div>
	<div class="kyivstar">(097) 009-19-21</div>
	<div class="life">(063) 203-85-12</div>
	<div class="service">(063) 694-12-40</div>
  </div>
  <noindex><a rel="nofollow" href="#" class="callme_viewform">Обратный звонок</a></noindex>
  <noindex><a rel="nofollow" class="ask" onclick="o=window.open;o('http://www.c-trade.com.ua/go.php?url=https://siteheart.com/webconsultation/369349?', 'siteheart_sitewindow_369349', 'width=550,height=400,top=30,left=30,resizable=yes'); return false;" href="http://www.c-trade.com.ua/go.php?url=https://siteheart.com/webconsultation/369349?" target="siteheart_sitewindow_369349">Online консультант</a><noindex>
  <div class="worktime"><span>Call-центр:</span>Пн-Пт: &nbsp;&nbsp;9:00-20:00<br/>Сб-Вс: 10:00-18:00</div>
  <!--<?php echo $language; ?>
  <?php echo $currency; ?>-->
<div id="menu">
	<ul>
		<li>
			<div id="search">
				<div class="button-search"></div>
				<input type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />
			  </div>
		</li>
	</ul>
</div>
<?php echo $cart; ?>
</div>
</div>
<div class="consultant"><noindex><a rel="nofollow" onclick="o=window.open;o('http://www.c-trade.com.ua/go.php?url=https://siteheart.com/webconsultation/369349?', 'siteheart_sitewindow_369349', 'width=550,height=400,top=30,left=30,resizable=yes'); return false;" href="http://www.c-trade.com.ua/go.php?url=https://siteheart.com/webconsultation/369349?" target="siteheart_sitewindow_369349"><img src="http://webindicator.siteheart.com/webindicator/image/1361524551?ent=369349&amp;company=369349" alt="SiteHeart"> </a></noindex></div>
<div id="container">

<div id="notification"></div>
