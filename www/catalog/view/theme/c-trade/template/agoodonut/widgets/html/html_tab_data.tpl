<div id="html_<?php echo $prefix;?>">
<div id="tab-html-<?php echo $prefix; ?>" class="tab-content">
<div class="box" style="display: block">
	<div class="box-content bordernone">

<?php echo($html); ?>

 </div>
</div>
</div>
</div>
<script>

<?php if (isset($settings_widget['anchor']) && $settings_widget['anchor']!='') { ?>

$(document).ready(function(){
	$('#tabs').append('<a href="#tab-html-<?php echo $prefix; ?>"><?php echo $heading_title; ?></a>');
	var data = $('#html_<?php echo $prefix;?>').html();
	var prefix = '<?php echo $prefix;?>';
	$('#html_<?php echo $prefix;?>').remove();

	<?php echo $settings_widget['anchor']; ?>

	$('#tabs a').tabs();

});
<?php  } ?>
</script>

