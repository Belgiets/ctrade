    <?php foreach ($products as $product) { ?>
    <div> <div>
	<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
      <?php if ($product['thumb']) { ?>
      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
      <?php } ?>   
     <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
	 <div class="reviews-count"><?php echo $product['reviews']; ?></div>
	 <div style="clear:both;"></div>
	 
	 <?php if ($product['price']) { ?>
      <div class="price">
        <?php if (!$product['special']) { ?>
        <?php echo $product['price']; ?>
        <?php } else { ?>
        <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
        <?php } ?>
        <?php if ($product['tax']) { ?>
        <br />
        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
        <?php } ?>
      </div>
      <?php } ?>
	 
      <div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');"><i><?php echo $button_wishlist; ?></i></a></div>
      <div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');"><i><?php echo $button_compare; ?></i></a></div>
	<div style="clear:both;"></div>
   <!-- Выводим атрибут -->
<?php if($product['attribute_groups']) { ?>
  <div class="description">
		<?php foreach ($product['attribute_groups'] as $attribute_group) { ?>
		  <?php if ($attribute_group['name'] == 'Краткие характеристики') { ?>
			<?php foreach ($attribute_group['attribute'] as $attribute) { ?>
			  <b><?php echo $attribute['name']; ?>:</b> 
			  <span><?php echo html_entity_decode($attribute['text']); ?></span>
			<?php } ?>
		  <?php } ?>
		<?php } ?>
</div>
<?php } ?>
<!-- Выводим атрибут Конец -->	
	</div></div>
    <?php } ?>