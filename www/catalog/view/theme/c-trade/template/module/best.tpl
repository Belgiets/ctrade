<?php 
$this->load->model('catalog/product');
$this->load->model('tool/image');
$image_additional_hover_module_setting="1";
$this->document->addScript('catalog/view/theme/c-trade/js/jquery/owl-carousel/owl.carousel.js');
$this->document->addScript('catalog/view/theme/c-trade/js/jquery/timer/jquery.countdown.js');
$this->document->addStyle('catalog/view/theme/c-trade/stylesheet/best.css');
$this->document->addStyle('catalog/view/theme/c-trade/stylesheet/best-responsive.css');
$this->document->addStyle('catalog/view/theme/c-trade/js/jquery/timer/jquery.countdown.css');
$button_compare = $this->language->get('button_compare');
$button_wishlist = $this->language->get('button_wishlist');
$no_image = $this->model_tool_image->resize('no_image.jpg', $image_width, $image_height);
$gen_carousel = rand(10000, 50000);;
?>
<?php if ($position == 'content_top' or $position == 'content_bottom') { ?>
		<div class="relative box daybox none">
		<?php if ($off_product_day =="1") { ?>
			<?php if ($products_day) { ?>
		      <div class="tovar_day">
				<?php foreach ($products_day as $product_day) {
				$hit_day = $this->model_catalog_product->getProduct($product_day['product_id']);
				$product_id = $product_day['product_id'];
				$href = $product_day['href'];
				$name = $product_day['name'];
				$thumb0 = $product_day['thumb'];
				$price = $product_day['price'];
				$special = $product_day['special'];
				$rating = $product_day['rating'];
				$reviews = $product_day['reviews'];
				$product_id = $product_day['product_id'];
				$quantity = $hit_day['quantity'];
				$imageaddit0 = $this->model_tool_image->resize($hit_day['image'], $image_addit_width, $image_addit_height);
				}
				if (empty($product_id)) {$product_id = "";}
				$imgs0 = $this->model_catalog_product->getProductImages($product_id);
				$imgt0 = array();
				foreach ($imgs0 as $imgi0) {
						$imgt0[] = array(
							'popup' => $this->model_tool_image->resize($imgi0['image'], $image_width, $image_height),
							'thumb' => $this->model_tool_image->resize($imgi0['image'], $image_addit_width, $image_addit_height),
							
						);
				}
				?>
				<?php if (!empty($product_id)) { ?>
				<div class="box-heading"><?php echo $this->language->get('header_hit_tovar'); ?></div>
					<div>
						<div class="name"><a href="<?php echo $href; ?>"><?php echo $name; ?></a></div>					
						<?php if ($thumb0) { ?>
						<div class="image relative"><a href="<?php echo $href; ?>"><img src="<?php echo $thumb0; ?>" alt="<?php echo $name; ?>" id="images<?php echo $gen_carousel; ?>1" /></a></div>
						<?php } ?>
						<?php if ($off_addit =="1") { ?>
							<?php if ($image_additional_hover_module_setting=="1") { ?>
								<?php if ($imgt0) { ?>
									<div class="images-add<?php echo $gen_carousel; ?>1 images-add">
										<a clickimage="<?php echo $thumb0; ?>" title="<?php echo $thumb0; ?>"><img src="<?php echo $imageaddit0; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $name; ?>" /></a>
										<?php $d = 2000; ?>
										<?php foreach ($imgt0 as $image0) { ?>
											<?php $d = $d + 1; ?>
											<?php if ($d <=2002 ) { ?>
												<a clickimage="<?php echo $image0['popup']; ?>"><img src="<?php if ($image0['thumb']) {echo $image0['thumb'];} else {echo $no_image_addit;} ?>" title="<?php echo $name; ?>" alt="<?php echo $heading_title; ?>" /></a>
											<?php } ?>
										<?php } ?>
									</div>
								<?php } ?>
							<?php } ?>
						<?php } ?>
						<?php if ($rating_and_prodano == "1") { ?>
						<div class="rating"><img src="catalog/view/theme/c-trade/image/stars-5.png" alt="<?php echo $reviews; ?>" /></div>
						<?php $width_vn = "100"; ?>
						<?php $width_vn = ($pervon_kol_vo-$quantity)/$pervon_kol_vo*100; ?>
						<div class="product-block">
						<div class="product-zoom-hit"><div class="product-zoom-hit-vnutr" style="width: <?php echo $width_vn; ?>%;"></div><span class="procent"><?php echo round($width_vn, 0)."%"; ?></span></div>
						<div class="ostalos"><?php echo $this->language->get('ostalos'); ?></div>
						</div>
						<?php } ?>
						<?php if ($price) { ?>
						<div class="price">
						  <?php if (!$special) { ?>
						  <?php echo $price; ?>
						  <?php } else { ?>
						  <span class="price-old"><?php echo $price; ?></span> <span class="price-new"><?php echo $special; ?></span>
						  <?php } ?>
						</div>
						<?php } ?>
						<div class="cart"><input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product_id; ?>');" class="button" />
						</div>
						<div class="wishlist"><a onclick="addToWishList('<?php echo $product_id; ?>');" title="<?php echo $button_wishlist; ?>"><i class="icon-wishlist icon"></i></a></div>
						<div class="compare"><a onclick="addToCompare('<?php echo $product_id; ?>');" title="<?php echo $button_compare; ?>"><i class="icon-compare icon"></i></a></div>
				    </div>
					<script type="text/javascript"><!--
						$(document).ready(function() {
							var img = $('#images<?php echo $gen_carousel; ?>1').attr('src');
							if (img != undefined) {
								var imgWidth = img.substring(img.lastIndexOf('-') + 1, img.lastIndexOf('x'));
								var imgHeight = img.substring(img.lastIndexOf('x') + 1, img.lastIndexOf('.'));
							}
							$('.images-add<?php echo $gen_carousel; ?>1 img').click(function() {
								var newsrc = $(this).parent().attr('clickimage');
								$('#images<?php echo $gen_carousel; ?>1').attr({
									src: newsrc,
									title: $(this).attr('title'),
									alt: $(this).attr('alt'),
									width: imgWidth,
									height: imgHeight
								});
								$('#images<?php echo $gen_carousel; ?>1').parent().attr('clickimage', newsrc);
							});
						});
					//--></script>
					<?php } ?>
			  </div>
			  <?php } ?>
			  <?php } ?>
			  <?php if (isset($products_uspey)) { ?>
			  <div class="day <?php if ($off_product_day == "0" or !$products_day) { ?>nonetovar_day<?php } ?>">
			  <div class="box-heading none"><?php echo $heading_title; ?></div>
			  <div id="owl-example<?php echo $gen_carousel; ?>" class="owl-carousel">
				<?php $n = $gen_carousel.'1'; ?>
				<?php foreach ($products_uspey as $product) { ?>
				<?php $product_id = $product['product_id']; ?>
				<?php $n = $n + 1; ?>
					  <div>
					  <?php 
					  $spec = $this->model_catalog_product->getProduct($product['product_id']);
					  $str1 = substr($spec['date_end'],0,strpos($spec['date_end'],"-")); 
					  $str2 = substr(str_replace("-","", substr($spec['date_end'],strpos($spec['date_end'],"-"))),0,2);
					  $str3 = substr(str_replace("-","", substr($spec['date_end'],strpos($spec['date_end'],"-"))),2);
					  ?>
						<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
						<div class="image relative">
							<?php if ($product['thumb']) { ?>
								<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="lazyOwl" data-src="path-to-your-image" id="images<?php echo $n; ?>" /></a>
							<?php } else { ?>
								<a href="<?php echo $product['href']; ?>"><img src="<?php echo $no_image; ?>" alt="<?php echo $product['name']; ?>" /></a>
							<?php } ?>  
						</div>
						<?php
						$resul = $this->model_catalog_product->getProduct($product['product_id']);
						if ($resul['image']) {$imageaddit = $this->model_tool_image->resize($resul['image'], $image_addit_width, $image_addit_height);} else {$imageaddit = $this->model_tool_image->resize('no_image.jpg', $image_addit_width, $image_addit_height);}
						$imgs = $this->model_catalog_product->getProductImages($product['product_id']);
						$imgt = array();
						foreach ($imgs as $imgi) {
								$imgt[] = array(
									'popup' => $this->model_tool_image->resize($imgi['image'], $image_width, $image_height),
									'thumb' => $this->model_tool_image->resize($imgi['image'], $image_addit_width, $image_addit_height),
								);
						}
						?>
						<?php $m = $gen_carousel.'1'; ?>
						<?php $md = $gen_carousel.'1'; ?>
						<?php if ($off_addit =="1") { ?>
							<?php if ($imgt) { ?>
								<div class="images-add<?php echo $n; ?> images-add">
									<a clickimage="<?php echo $product['thumb']; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $imageaddit; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $product['name']; ?>" /></a>
									<?php foreach ($imgt as $image) { ?>
										<?php $m = $m + 1; ?>
										<?php $mj = $md + 2; ?>
										<?php if ($m <= $mj ) { ?>
											<a clickimage="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"><img src="<?php if ($image['thumb']) {echo $image['thumb'];} else {echo $no_image_addit;} ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $heading_title; ?>" /></a>
										<?php } ?>
									<?php } ?>
								</div>
							<?php } ?>
						<?php } ?>
						<?php if ($product['price']) { ?>
						<div class="price">
						  <?php if (!$product['special']) { ?>
						  <?php echo $product['price']; ?>
						  <?php } else { ?>
						  <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
						  <?php } ?>
						</div>
						<?php } ?>
						<?php if ($schetchik == "1") { ?>
						<div class="action-tovar">
							<?php if ($spec['date_end']!="") { ?>
								<?php echo $this->language->get('schetcik_rasprod'); ?>
							<?php } else { ?>
								<?php echo $this->language->get('schetcik_end_rasprod'); ?>
							<?php } ?>
						</div>
						<?php if ($spec['date_end']!="") { ?>
						<div id="countdown<?php echo $n; ?>"></div>
						<?php } else { ?>
						<div class="countdownHolder"><span class="countDays"><span class="position"><span class="digit static" style="top: 0px; opacity: 1;">0</span></span><span class="position"><span class="digit static" style="top: 0px; opacity: 1;">0</span></span><span class="time"><?php echo $this->language->get('days'); ?></span></span><span class="countDiv countDiv0"></span><span class="countHours"><span class="position"><span class="digit static" style="top: 0px; opacity: 1;">0</span></span><span class="position"><span class="digit static" style="top: 0px; opacity: 1;">0</span></span><span class="time"><?php echo $this->language->get('hours'); ?></span></span><span class="countDiv countDiv1"></span><span class="countMinutes"><span class="position"><span class="digit static" style="top: 0px; opacity: 1;">0</span></span><span class="position"><span class="digit static" style="top: 0px; opacity: 1;">0</span></span><span class="time"><?php echo $this->language->get('minutes'); ?></span></span><span class="countDiv countDiv2"></span><span class="countSec"><span class="position"><span class="digit static" style="top: 0px; opacity: 1;">0</span></span><span class="position"><span class="digit static" style="top: 0px; opacity: 1;">0</span></span><span class="time"><?php echo $this->language->get('sec'); ?></span></span><span class="countDiv countDiv3"></span></div>
						<?php } ?>
						<div class="action-count">
						<span><?php echo $spec['quantity']; ?></span>
						<div class="qya"><?php echo $this->language->get('count_rasprod'); ?></div>
						</div>
						<?php } ?>
						<div class="cart-carousel">
						<div class="cart"><input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product_id; ?>');" class="button" /></div>
						<div class="wishlist"><a onclick="addToWishList('<?php echo $product_id; ?>');" title="<?php echo $button_wishlist; ?>"><i class="icon-wishlist icon"></i></a></div>
						<div class="compare"><a onclick="addToCompare('<?php echo $product_id; ?>');" title="<?php echo $button_compare; ?>"><i class="icon-compare icon"></i></a></div>
						</div>
					  </div>
				  <?php } ?>
			  </div>
		  </div>
		  <script type="text/javascript">
			$(document).ready(function() {
				$("#owl-example<?php echo $gen_carousel; ?>").owlCarousel({
				  navigation : true,
				  slideSpeed : 200,
				  paginationSpeed : 400,
				  items: <?php echo $kol_vo_responsive_setting; ?>,
				  itemsDesktop: [1488,3],
				  itemsDesktopSmall : [1139,2],
				  itemsTablet: [919,1],
				  itemsTabletSmall: false,
				  itemsMobile : [479,1],
				  singleItem : false,
				  itemsScaleUp : false,
				  lazyLoad: true,
				  paginationNumbers: true,
				  <?php if ($responsive_setting =="0") { ?>responsive: false,<?php } ?>
				});
				
			});
			</script>
		  <?php } ?>
			<?php if (isset($products_uspey)) { ?>
			<?php $v = $gen_carousel.'1'; ?>
			<?php foreach ($products_uspey as $product) { ?>
			<?php $v = $v + 1; ?>
			<script type="text/javascript"><!--
					$(document).ready(function() {
						var img = $('#images<?php echo $v; ?>').attr('src');
						if (img != undefined) {
							var imgWidth = img.substring(img.lastIndexOf('-') + 1, img.lastIndexOf('x'));
							var imgHeight = img.substring(img.lastIndexOf('x') + 1, img.lastIndexOf('.'));
						}
						$('.images-add<?php echo $v; ?> img').click(function() {
							var newsrc = $(this).parent().attr('clickimage');
							$('#images<?php echo $v; ?>').attr({
								src: newsrc,
								title: $(this).attr('title'),
								alt: $(this).attr('alt'),
								width: imgWidth,
								height: imgHeight
							});
							$('#images<?php echo $v; ?>').parent().attr('clickimage', newsrc);
						});
					});
			//--></script>
			<?php } ?>
			<?php } ?>
			<?php if (isset($products_uspey)) { ?>
			<?php $n = $gen_carousel.'1'; ?>
			<?php foreach ($products_uspey as $product) { ?>
			<?php $n = $n + 1; ?>
			<?php $spec = $this->model_catalog_product->getProduct($product['product_id']); ?>
			<?php
			$str1 = substr($spec['date_end'],0,strpos($spec['date_end'],"-")); 
			$str2 = substr(str_replace("-","", substr($spec['date_end'],strpos($spec['date_end'],"-"))),0,2) - 1;
			$str3 = substr(str_replace("-","", substr($spec['date_end'],strpos($spec['date_end'],"-"))),2);
			?>
			<script type="text/javascript"><!--
			$(function(){
			var note = $('#note'),
				ts = new Date(<?php echo $str1; ?>, <?php echo $str2; ?>, <?php echo $str3; ?>),
				newYear = true;
			if((new Date()) > ts){
				ts = (new Date()).getTime() + 10*24*60*60*1000;
				newYear = false;
			}
			$('#countdown<?php echo $n; ?>').countdown({
				timestamp	: ts,
				callback	: function(days, hours, minutes, seconds){
					
				var message = "";
				
				message += "����: " + days +", ";
				message += "�����: " + hours + ", ";
				message += "�����: " + minutes + " � ";
				message += "������: " + seconds + " <br />";
		
				note.html(message);
				}
			});
			});
			//--></script>      
			<?php } ?>
			<?php } ?>
<?php } ?>
</div>
<script type="text/javascript"><!--
function init(elem, options){
		elem.addClass('countdownHolder');
		$.each(['Days','Hours','Minutes','Sec'],function(i){
		var clas;
		if (this=='Days') {clas="<?php echo $this->language->get('days'); ?>";}
		if (this=='Hours') {clas="<?php echo $this->language->get('hours'); ?>";}
		if (this=='Minutes') {clas="<?php echo $this->language->get('minutes'); ?>";}
		if (this=='Sec') {clas="<?php echo $this->language->get('sec'); ?>";}
		
			$('<span class="count'+this+'">' +
				'<span class="position">' +
					'<span class="digit static">0</span>' +	'</span>' +
				'<span class="position">' +	'<span class="digit static">0</span>' +	'</span><span class="time">'+clas+'</span></span>').appendTo(elem);
			
			if(this!="Seconds"){
				elem.append('<span class="countDiv countDiv'+i+'"></span>');
			}
		});
}
//--></script>
<style type="text/css">
		#content .tovar_day .box-heading {
			background: <?php echo $color_best_day; ?>;
            border-left: 2px solid <?php echo $color_best_day; ?>;
            border-right: 2px solid <?php echo $color_best_day; ?>;
		}
        .tovar_day > div {
            border-left: 2px solid <?php echo $color_best_day; ?>;
            border-right: 2px solid <?php echo $color_best_day; ?>;
            border-bottom: 2px solid <?php echo $color_best_day; ?>;
        }
		#content .tovar_day .box-heading {
			color: <?php echo $hea_color_best_day; ?>;
		}
		#content .day .box-heading.none {
			color: <?php echo $hea_color_best_uspey; ?>;
		}
        #content .day .owl-carousel {
            border: 0 none;
        }
</style>