<?php
class ModelCatalogVote extends Model {		
	public function addVote($vote) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "review SET author = '" . $vote['name'] . "', customer_id = '9999', product_id = '" . $vote['product_id'] . "', text = '" . $vote['text_vote'] . "', rating = '" . $vote['rating'] . "', date_added = NOW(), status='1'");
	}
	
	public function getTotalVotesByProductId($product_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r LEFT JOIN " . DB_PREFIX . "product p ON (r.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND p.date_available <= NOW() AND p.status = '1' AND r.status = '1' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		
		return $query->row['total'];
	}

}
?>