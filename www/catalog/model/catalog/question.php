<?php
class ModelCatalogQuestion extends Model {		
	public function addQuestion($product_id, $data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "question SET questioner = '" . $this->db->escape($data['questioner']) . "', email = '" . $this->db->escape($data['email']) . "', customer_id = '" . (int)$this->customer->getId() . "', product_id = '" . (int)$product_id . "', product = '" . $this->db->escape($data['product']) . "', text_question = '" . $this->db->escape($data['text_question']) . "', question_category = '" . (int)$data['question_category'] . "', date_added = NOW()");

		if ($this->config->get('config_question_mail_admin')) {

			$this->language->load('mail/question');
			$this->load->model('catalog/product');
			$product_info = $this->model_catalog_product->getProduct($product_id);
			
			$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));

			$message  = $this->language->get('text_waiting') . "\n";
			$message .= sprintf($this->language->get('text_category'), $this->language->get('text_question_product')) . "\n";
			$message .= sprintf($this->language->get('text_product'), $this->db->escape(strip_tags($product_info['name']))) . "\n";
			$message .= sprintf($this->language->get('text_questioner'), $this->db->escape(strip_tags($data['questioner']))) . "\n";
			$message .= $this->language->get('text_question') . "\n";
			$message .= html_entity_decode($data['text_question'], ENT_QUOTES, 'UTF-8');

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');	
			$mail->setTo(array($this->config->get('config_email')));
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			$mail->setHtml($message);
			$mail->send();

			$emails = explode(',', $this->config->get('config_alert_emails'));
			
			foreach ($emails as $email) {
				if ($email && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}
	}
	
	public function addQuestionByCustomerId($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "question SET questioner = '" . $this->db->escape($data['questioner']) . "', email = '" . $this->db->escape($data['email']) . "', customer_id = '" . (int)$this->customer->getId() . "', subject = '" . $this->db->escape($data['subject']) . "', product_id = '" . $this->db->escape($data['product_id']) . "', product = '" . $this->db->escape($data['product']) . "', text_question = '" . $this->db->escape($data['text_question']) . "', question_category = '" . (int)$data['question_category'] . "', date_added = NOW()");
		
		if ($this->config->get('config_question_mail_admin_account') || $this->config->get('config_question_mail_admin_faq')) {
			$this->language->load('mail/question');
			
			$this->load->model('catalog/product');
			$product_info = $this->model_catalog_product->getProduct($this->db->escape($data['product_id']));
			
			$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));
			
			$this->load->model('catalog/question');
			
			if ($this->db->escape($data['question_category']) == 1) {
				$question_category = $this->language->get('text_question_product');
			} else {
				$question_category = $this->language->get('text_question_no_product');
			}

			$message  = $this->language->get('text_waiting') . "\n";
			if ($this->db->escape($data['question_category']) == 1) {
			  $message .= sprintf($this->language->get('text_category'), $question_category) . "\n";
			  $message .= sprintf($this->language->get('text_product'), $this->db->escape(strip_tags($product_info['name']))) . "\n";
			} else {
				$message .= sprintf($this->language->get('text_category'), $question_category) . "\n";
				$message .= sprintf($this->language->get('text_question_subject'), $this->db->escape(strip_tags($data['subject']))) . "\n";
			}
			$message .= sprintf($this->language->get('text_questioner'), $this->db->escape(strip_tags($data['questioner']))) . "\n";
			$message .= $this->language->get('text_question') . "\n";
			$message .= html_entity_decode($data['text_question'], ENT_QUOTES, 'UTF-8');

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');	
			$mail->setTo(array($this->config->get('config_email')));
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			$mail->setHtml($message);
			$mail->send();

			$emails = explode(',', $this->config->get('config_alert_emails'));
			
			foreach ($emails as $email) {
				if ($email && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}
		
		$this->cache->delete('product');
	}
	
	public function deleteQuestion($question_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "question WHERE question_id = '" . (int)$question_id . "'");
		
		if ($this->config->get('config_question_mail_admin_delete')) {
			$this->language->load('mail/question');
			
			$subject = sprintf($this->language->get('text_subject_delete'), $this->config->get('config_name'));

			$message  = $this->language->get('text_waiting_delete') . "\n";

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');	
			$mail->setTo(array($this->config->get('config_email')));
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			$mail->setHtml($message);
			$mail->send();

			$emails = explode(',', $this->config->get('config_alert_emails'));
			
			foreach ($emails as $email) {
				if ($email && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}
		
		$this->cache->delete('product');
	}
	
	public function getTotalQuestions($data = array()) {
        $query = $this->db->query("SELECT count(*) AS total FROM `" . DB_PREFIX . "question` q WHERE p.product_id = '" . (int)$product_id . "' AND p.date_available <= NOW() AND p.status = '1' AND q.status = '1' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		
        return $query->row['total'];
    }
	
	public function getTotalQuestionsAllCategories($data = array()) {
        $query = $this->db->query("SELECT count(*) AS total FROM " . DB_PREFIX . "question");
		
        return $query->row['total'];
    }
		
	public function getQuestionsByProductId($product_id, $start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}
		
		if ($limit < 1) {
			$limit = 20;
		}		
		
		$query = $this->db->query("SELECT q.question_id, q.questioner, q.text_question, q.text_answer, p.product_id, pd.name, p.price, p.image, q.date_added, q.date_modified FROM " . DB_PREFIX . "question q LEFT JOIN " . DB_PREFIX . "product p ON (q.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND p.date_available <= NOW() AND p.status = '1' AND q.status = '1' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY q.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);
			
		return $query->rows;
	}
	
	public function getQuestionsByCustomerId($start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}
		
		if ($limit < 1) {
			$limit = 20;
		}	
				
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "question`WHERE customer_id = '" . $this->customer->getId() . "' ORDER BY question_id DESC LIMIT " . (int)$start . "," . (int)$limit);
		
		return $query->rows;
	}

	public function getTotalQuestionsByProductId($product_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "question q LEFT JOIN " . DB_PREFIX . "product p ON (q.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND p.date_available <= NOW() AND p.status = '1' AND q.status = '1' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		
		return $query->row['total'];
	}
	
	public function getQuestion($question_id) {
		$question_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "question` WHERE question_id = '" . (int)$question_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");
		
		if ($question_query->num_rows) {
			return array(
				'question_id'        => $question_query->row['question_id'],
				'product_id'         => $question_query->row['product_id'],
				'customer_id'        => $question_query->row['customer_id'],
				'questioner'         => $question_query->row['questioner'],
				'question_category'  => $question_query->row['question_category'],
				'text_question'      => $question_query->row['text_question'],
				'text_answer'        => $question_query->row['text_answer'],				
				'status'             => $question_query->row['status'],
				'date_added'         => $question_query->row['date_added'],
				'date_modified'      => $question_query->row['date_modified']
			);
		} else {
			return false;	
		}
	}
	
	public function getQuestionsFaq($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "question` WHERE question_id";
		
		if (!empty($data['filter_question_id'])) {
			$sql .= " AND question_id = '" . (int)$data['filter_question_id'] . "'";
		}

		if (!empty($data['filter_questioner'])) {
			$sql .= " AND questioner LIKE '%" . $this->db->escape($data['filter_questioner']) . "%'";
		}
		
		if (isset($data['filter_question_category']) && $data['filter_question_category'] !== null) {
			$sql .= " AND question_category = '" . (int)$data['filter_question_category'] . "'";
		}
		
		if (!empty($data['filter_name'])) {
			$sql .= " AND product LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		
		if (isset($data['filter_status']) && $data['filter_status'] !== null) {
			$sql .= " AND status = '" . (int)$data['filter_status'] . "'";
		}
		
		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}
		
		$sql .= " GROUP BY question_id";
		
		$sort_data = array(
			'question_id',
			'questioner',
			'question_category',
			'status',
			'date_added'
		);	
			
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY date_added";	
		}
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}																																							  
																																							  
		$query = $this->db->query($sql);																																				
		
		return $query->rows;
	}
	
	public function getTotalQuestionsFaq($data = array()) {
		$sql = "SELECT count(*) AS total FROM `" . DB_PREFIX . "question` q WHERE q.question_id";
		
		if (!empty($data['filter_question_id'])) {
			$sql .= " AND question_id = '" . (int)$data['filter_question_id'] . "'";
		}

		if (!empty($data['filter_questioner'])) {
			$sql .= " AND questioner LIKE '%" . $this->db->escape($data['filter_questioner']) . "%'";
		}
		
		if (isset($data['filter_question_category']) && $data['filter_question_category'] !== null) {
			$sql .= " AND question_category = '" . (int)$data['filter_question_category'] . "'";
		}
		
		if (!empty($data['filter_name'])) {
			$sql .= " AND product LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		
		if (isset($data['filter_status']) && $data['filter_status'] !== null) {
			$sql .= " AND status = '" . (int)$data['filter_status'] . "'";
		}
		
		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
}
?>